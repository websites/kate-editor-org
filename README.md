# kate-editor.org

[![Build Status](https://binary-factory.kde.org/buildStatus/icon?job=Website_kate-editor-org)](https://binary-factory.kde.org/job/Website_kate-editor-org/)

This repository contains the full [kate-editor.org website](https://kate-editor.org).

On push the https://binary-factory.kde.org/job/Website_kate-editor-org/ job will trigger an update of the web server.

# Live preview

You can start a local Hugo powered web server via:

```bash
./server.sh
```

The command will print the URL to use for local previewing.

## Update the theme

- If you want to always get and use the latest version of the theme, uncomment the `hugo mod get ...` line in `server.sh` before running it;
- If you want to update the theme once, run:
  ```bash
  hugo mod get invent.kde.org/websites/hugo-kde && hugo mod tidy
  ```
  Or if you use [just](https://just.systems/):
  ```bash
  just mod-up
  ```
- `go.mod` and `go.sum` files should be committed to Git.

# Update the syntax-highlighting framework update sites

Check out the README.md in the syntax-highlighting.git on invent.kde.org.

There is a build target to generate the needed stuff after a successful compile of the framework.

# Update auto-generated pages like "team" or "merge requests"

Just run:

```bash
./regenerate.sh
```

and review the results before you commit them.

# Development

Details about the shared theme: [hugo-kde wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/)

## I18n

If you want to build the site in other languages, run:

```bash
python3 scripts/custom_generation.py
hugo server
```

The Python script
- generates files in other languages, using [hugo-i18n](https://invent.kde.org/websites/hugo-i18n);
- downloads translation catalogues of [Syntax Highlighting framework](https://invent.kde.org/frameworks/syntax-highlighting/) from SVN into a `pos` folder, to use for /syntax and /themes pages. If such a folder already exists, it will skip this step.
