#!/usr/bin/env perl

# be strict with us ;)
use strict;
use warnings;

# generate data for the team page

# the repos we are interested in
my %repos = (
    "." => ".",
    "kate" => "git\@invent.kde.org:utilities/kate.git",
    "ktexteditor" => "git\@invent.kde.org:frameworks/ktexteditor.git",
    "syntax-highlighting" => "git\@invent.kde.org:frameworks/syntax-highlighting.git",
);

# get the repos
for my $repo (sort keys %repos) {
    # clone from invent.kde.org, update if already around, if not . aka this website!
    if ($repo ne ".") {
        if (-d $repo) {
            print "Updating $repo clone...\n";
            system("git", "-C", $repo, "pull", "-q") == 0 || die "Failed to pull $repo!\n";
        } else {
            print "Creating $repo clone...\n";
            system("git", "clone", "-q", $repos{$repo}) == 0 || die "Failed to clone $repo from $repos{$repo}!\n";
        }
    }
}

# mapping of some contributor names, either to a canonical one or the void
my %contributorMapping = (
    # ignore some accounts like scripts
    "Script Kiddy" => "",
    "l10n daemon script" => "",
    "KDE Sysadmin" => "",

    # canonical names for some people
    "jonathan poelen" => "Jonathan Poelen",
    "mentasti" => "Marco Mentasti",
);

# function to compute contributors (all or since some time)
sub computeContributors
{
    my $contributors = shift;
    my $limit = shift;
    if (!defined($limit)) {
        $limit = "";
    }

    # get stats per repo
    for my $repo (sort keys %repos) {
        # get short stats with commits => names
        for (`git -C $repo shortlog $limit -s -n`) {
            if ((my $commits, my $contributor) = /\s*([0-9]+)\s+(.*)/) {
                # remap, if needed, might map to empty string for triggering of filter below
                if (defined($contributorMapping{$contributor})) {
                    $contributor = $contributorMapping{$contributor};
                }

                # skip bad contributors
                next if ($contributor =~ /^\s*$/);

                # remember number of commits, accumulated over all repositories
                if (!defined($contributors->{$contributor})) {
                    $contributors->{$contributor} = 0;
                }
                $contributors->{$contributor} += int($commits);
            }
        }
        if ($? != 0) {
            die "Failed to retrieve contributors for $repo!\n";
        }
    }

    # use "SPDX-FileCopyrightText:" for stuff not tracked by version control
    # only do this, if no date limit given
    if ($limit eq "") {
        for my $repo (sort keys %repos) {
            for (`git -C $repo grep "SPDX-FileCopyrightText:"`) {
                if ((my $contributor) = /SPDX-FileCopyrightText:\s+(?:[0-9]+)\s+(.*)\s+</) {
                    # remap, if needed, might map to empty string for triggering of filter below
                    if (defined($contributorMapping{$contributor})) {
                        $contributor = $contributorMapping{$contributor};
                    }

                    # skip bad contributors
                    next if ($contributor =~ /^\s*$/);

                    # we have here no commits, we just add people that are not known by commits (with one dummy commit)
                    if (!defined($contributors->{$contributor})) {
                        $contributors->{$contributor} = 1;
                    }
                }
            }
            if ($? != 0) {
                die "Failed to retrieve contributors for $repo via SPDX-FileCopyrightText grep!\n";
            }
        }
    }
}

print "Generating team data in data/team.yaml...\n";
my $team_path = "data/team.yaml";
open (my $team_handle, ">$team_path");

# helper to print contributors for given contributors hash
sub printContributors
{
    my $mapName = shift;
    my $contributors = shift;
    my $oldContributors = shift;

    # we now have all contributors
    print $team_handle "$mapName:\n";
    for my $contributor (sort { int($contributors->{$b}) <=> int($contributors->{$a}) } sort keys %{$contributors}) {
        print $team_handle
            "- name: $contributor\n".
            "  ctrb: $contributors->{$contributor}\n";
        # highlight new contributors?
        if (defined($oldContributors) && !defined($oldContributors->{$contributor})) {
            print $team_handle "  new: true\n";
        }
    }
}

# fill contributor of last year => commits map
my %contributorsOfLastYear = ();
computeContributors(\%contributorsOfLastYear, "--since=1.years");

# fill contributor before last year => commits map
my %contributorsBeforeLastYear = ();
computeContributors(\%contributorsBeforeLastYear, "--until=1.years");

# fill total contributor => commits map
my %contributors = ();
computeContributors(\%contributors);

# contributors of the last year
printContributors("lastYear", \%contributorsOfLastYear, \%contributorsBeforeLastYear);

# all contributors ever
printContributors("allTime", \%contributors);

# we are done with the page => ensure close before git add happens
close($team_handle);
