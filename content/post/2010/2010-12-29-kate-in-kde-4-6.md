---
title: Kate in KDE 4.6
author: Dominik Haumann

date: 2010-12-29T10:47:20+00:00
url: /2010/12/29/kate-in-kde-4-6/
categories:
  - Users
tags:
  - planet

---
Some days ago KDE 4.6 was branched. So what will Kate in KDE 4.6 bring?

  * The <a title="Kate Tree View Plugin" href="/2010/09/10/tree-view-plugin-introduction/" target="_blank">new tree view</a> as &#8220;Documents&#8221; sidebar completely replaces the old file list. It supports the simple list mode as well as a pseudo-intelligent tree view to group files. The &#8220;Documents&#8221; tab is now implemented as plugin (always loaded), meaning that it brought a code cleanup as well. We hope you like it! :)
  * <a title="Kate Swap Files" href="/2010/07/12/gsoc-swap-files-for-kate/" target="_self">Swap</a> <a title="Kate Swap Files" href="/2010/07/27/gsoc-view-differences-for-kates-swap-files/" target="_self">file</a> support, meaning that you can recover all your unsaved data for local files on the next Kate startup.
  * Ability to always load plugins.
  * New <a title="Kate SQL Plugin" href="../2010/07/29/katesql-a-new-plugin-for-kate/" target="_self">SQL Query plugin</a>. The day we blogged about it had the most visits in the entire kate-editor.org history. So it seems the the SQL plugin is a really nice addon for web developers! :)
  * New <a title="Kate GDB Plugin" href="/2010/10/06/introducing-kate-gdb-plugin/" target="_self">GNU Debugger (GDB) Plugin</a>!
  * New <a title="Highlight Selection" href="/2010/11/14/highlight-selected-text/" target="_self">Highlight Selected Text Plugin</a>.
  * Ability to <a title="Scripted Actions" href="/2010/07/09/kate-scripted-actions/" target="_self">add scripts to the menu and bind shortcuts</a>.
  * Kate got a new homepage on <a title="Kate Homepage" href="http://www.kate-editor.org" target="_self">http://www.kate-editor.org</a> featuring WordPress.
  * <a title="Resolved Issues in Kate" href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&product=kate&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&emailassigned_to1=1&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2010-08-10&chfieldto=2010-12-31&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&field0-0-0=noop&type0-0-0=noop&value0-0-0=" target="_blank">Lots of bugs fixed</a> since the KDE 4.5.0 release.
  * <a title="Kate Smart Classes Removed" href="/2010/08/13/smartranges-goodbye/" target="_self">SmartCursors and SmartRanges</a> are gone, resulting in more stable and clean code. MovingCursors and MovingRanges replace the old classes and work reliable. For instance, KDevelop is already ported leading to less crashes.
  * Christoph found the <a title="Kate History" href="/2010/08/15/kate-history/" target="_self">emails of the early days of Kate</a> again. A funny read! :)
  * <a title="Kate Unit Tests" href="/2010/08/23/kate-night-make/" target="_self">Daily unit test runs</a>. We have more and more unit tests, securing the correct behavior of e.g. search&replace, the text buffer, <a title="Encoding Detection" href="/2010/08/26/encoding-detection-revised/" target="_blank">encoding detection</a> or indenters.
  * Kate development mostly moved to <a title="Kate on Git" href="/2010/11/16/kate-on-git-kde-org/" target="_self">KDE&#8217;s git infrastructure</a>, resulting in very easy <a title="Building Kate" href="/get-it/" target="_self">build instructions</a>.
  * The default settings of a session are now stored in the katerc file again, rendering the default.katesession obsolete. This means you will have to configure your default session again (sorry!). On the other hand, this <a title="Resolved Issues in Kate" href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&product=kate&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&emailassigned_to1=1&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2010-08-10&chfieldto=2010-12-31&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&field0-0-0=noop&type0-0-0=noop&value0-0-0=" target="_blank">fixed quite some bugs</a> with regard to the session handling.

**Credit where credit is due!**

There were lots of developers involved who helped to improve Kate. Thanks a lot to every one of you! Without your contribution, Kate would not be what it is :-) Keep it up!