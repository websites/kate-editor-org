---
title: Kate History ;)
author: Christoph Cullmann

date: 2010-08-15T16:16:46+00:00
url: /2010/08/15/kate-history/
categories:
  - Developers
  - KDE
  - Users
tags:
  - planet

---
While setting up the new server for the Kate homepage, I actually found again old stuff ;)  
Amazing that mails nearly ten years old can still be somewhere on the filesystem.  
Perhaps a little hint, to post the beginnings of what today is Kate/KatePart/KWrite and KTextEditor.

Ten years ago, I asked the original author of KWrite, if he is interested in a MDI version of it (sorry, german, original mail):

<pre>From: Cullmann Christoph &lt;crossfire@babylon2k.de&gt;
To: digisnap@cs.tu-berlin.de
Subject: KWrite - Verbesserungsvorschläge
Date: Thu, 14 Dec 2000 18:38:42 +0100

Hallo
Ich benutze KWrite regelmässig um Quellcode zu bearbeiten und das
Syntaxhighlighting ist sehr praktisch.
Es wäre jedoch schön wenn KWrite eine MDI-Oberfläche hätte.
Ich baue gerade eine und falls jemand Interesse hat können sie sich ja melden.

Danke und Tschö
Christoph Cullmann</pre>

I actually never got any reaction from the author Jochen Wilhelmy. Guess the mail address was already abandoned at that time.  
Later I tried my luck with kde-devel:

<pre>From: Cullmann Christoph &lt;crossfire@babylon2k.de&gt;
To: kde-devel@max.tat.physik.uni-tuebingen.de
Subject: Need help - KWrite
Date: Thu, 4 Jan 2001 00:21:35 +0100

Hi,
i am building a mdi texteditor using the kwrite-widget.
I want to use most of the extended features of the kwrite class, like search
dialog, kspell, ....

Is there any way to do this using the KParts system or must i use the kwrite
include files and compile the kwrite widget into my program ?

cu and thanks for any answer
C. Cullmann</pre>

Not much reactions, thought, but I kept to be persistent ;)

<pre>From: Cullmann Christoph &lt;crossfire@babylon2k.de&gt;
To: kde-devel@max.tat.physik.uni-tuebingen.de
Subject: Re: Looking for kwrite developers.
Date: Thu, 11 Jan 2001 17:30:19 +0100

Hi all,
I have build up a editor using the KWrite Widget and a QTabWidget to provide
a multidocument interface :-)
It has some bugs at the moment (I think QTabWidget is the problem) but works
real nice.

Anybody interested in this ?

cu
C. cullmann</pre>

Shortly after this mail, one of the developers which stayed around for years joined, Anders Lund (more at [the team page][1]).  
I named the starting project &#8220;KCEdit&#8221; and put it up on sourceforge.net:

<pre>From: Cullmann Christoph &lt;crossfire@babylon2k.de&gt;
To: kde-devel@max.tat.physik.uni-tuebingen.de
Subject: MDI TextEditor - KCEdit
Date: Sat, 13 Jan 2001 12:04:33 +0100

Hi all,
I have build up a small mdi texteditor using the kwrite widget :-)
If someone is interested in helping to improve it or only wants to
test it a bit, i have set a sourceforge.net project up.

url : http://sourceforge.net/projects/kcedit

It would be nice if someone wants to take part in the development.

cu
C.Cullmann</pre>

After that, the next nice guy joined: Michael Bartl.  
We searched for a new name for the editor, as KCEdit was not that nice, as very similar to KEdit and no longer only &#8220;Cullmann&#8221;&#8216;s pet project.  
What did we choose? Here you see:

<pre>From: Cullmann Christoph &lt;crossfire@babylon2k.de&gt;
To: Michael Bartl &lt;michael.bartl1@chello.at&gt;
Subject: Kant is born ;-)
Date: Fri, 19 Jan 2001 20:55:59 +0100

Here it is ;-)
kant-0.1.0.tar.gz</pre>

As sourceforge.net failed to be a nice hosting, we moved the project to http://www.openave.net (and later back again, lol).  
Because of family problems Michael dropped out of the time after sometime, still BIG THANK YOU.

Later, I tried to get my changes back in KDE, as I didn&#8217;t want to do a permanent fork:

<pre>From: Cullmann Christoph &lt;crossfire@babylon2k.de&gt;
To: kde-devel@max.tat.physik.uni-tuebingen.de
Subject: How can I participate in the KWrite project ?
Date: Tue, 20 Feb 2001 20:25:15 +0100
Cc: kde-core-devel@max.tat.physik.uni-tuebingen.de

Hi,
I want to help as a developer in the kwrite project. I and some other people
are working on Kant (http://www.sourceforge.net/projects/kant), a MDI
texteditor for kde &gt;=2.0 and we often find bugs in the kwrite code or missing
features we would need. It would be great if I could help to develop kwrite
because only sending bug reports and hoping that new features in kwrite will
come up sometime is really annoying.
How can I join the KWrite team and get CVS read/write access (perhaps ;-) ?
To have an overview about my skills please look at the Kant sourcecode or
simply download and test Kant out of the CVS at sourceforge.net.

cu and thx for you interest
Christoph Cullmann

P.S.
Sorry for the bad English ;-)</pre>

Without much problems, I got a CVS account on the KDE server and was allowed to add my code and the code of the others to the KWrite codebase.  
All other contributors which were still active got accounts later, too. Worked all like a charm thanks to Waldo Bastian.  
Still Kant itself was not in KDE, therefor next try:

<pre>From: Cullmann Christoph &lt;crossfire@babylon2k.de&gt;
To: kde-devel@max.tat.physik.uni-tuebingen.de
Subject: Could Kant replace or extend KWrite in KDE ?
Date: Sat, 24 Feb 2001 17:28:58 +0100

Hi,
I am the projectmanager of Kant, a MDI texteditor which uses the KWrite
widget for displaying text (no MDI like you know it from windows, MDI like
you know it from Emacs or Konqueror :).

Kant has come to a level of stability which would it allow to put it into the
kde cvs i hope. I have talked with Carsten Pfeiffer (he likes Kant :) and he
told me to send a message to this list to start a discussion if and where
Kant could be integrated.

I just released a new Kant version (kant-0.2.0-prerelease) on sourceforge.net
for testing the app that you have an overview about its features.

Kant Homepage:
http://sourceforge.net/projects/kant/

newest Kant version to download:
http://download.sourceforge.net/kant/kant-0.2.0-prerelease.tar.gz

nice screenshot of Kant:
http://sourceforge.net/dbimage.php?id=1590

If you want to look at the unstable development code just look into the Kant
CVS at sourceforge.net, you find the exact description to checkout at the
Kant Homepage under "CVS" (cvs-modulename: kant).

Kant links dynamic to kwritepart and konsolepart. This must be considered if
you want to put Kant into kde cvs.

I hope you all like Kant, I think it would be a nice replacement for KWrite.

cu and thx
Christoph

P.S.
Sorry for my poor English and the big tar.gz file (something isn't right with
make dist in kant, must fix it :)</pre>

After some changes to the code, we were allowed to move the development completly to KDE CVS.  
Joseph Wenninger joined the development, too.

Btw., my nice old e-mail footer:

<pre>| |  / /   - get an edge in editing -
| | / /    »»»» GET KANT ««««
| |/ /     a fast and capable multiple document,
|    \     multiple view text editor for KDE
| |\  \
| | \  \   http://devel-home.kde.org/~kant</pre>

Whereas Kant was just a fine name for us, it had some pronunciation in english which was not that political correct ;)  
Therefor we searched a new name:

<pre>From: Cullmann Christoph &lt;crossfire@babylon2k.de&gt;
To: David Faure &lt;david@mandrakesoft.com&gt;
Subject: Hi, is Kate a good name ?
Date: Sat, 31 Mar 2001 15:15:47 +0200

Hi David,
would be Kate a political correct name for Kant ?

Kate - KDE Advanced Text Editor

cu
Christoph</pre>

We even asked the developers of Katy, an other text editor, if they would have problems with that name change ;)  
Kate was born ;)

 [1]: /the-team/