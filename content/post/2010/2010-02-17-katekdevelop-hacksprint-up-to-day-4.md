---
title: Kate/KDevelop HackSprint – Up To Day 4
author: Milian Wolff

date: 2010-02-17T17:43:38+00:00
excerpt: |
  Woha, quite a few days flew by without me blogging about anything. Thankfully the others started to write so I don&#8217;t have to repeat it all ;-) Instead I&#8217;ll concentrate on stuff I did or learned.
  
  GHNS for Snippets
  
  Well, first I think an ex...
url: /2010/02/17/katekdevelop-hacksprint-up-to-day-4/
syndication_source:
  - 'Milian Wolff - kate'
syndication_source_uri:
  - http://milianw.de/taxonomy/term/170/0
syndication_source_id:
  - http://milianw.de/tag/kate/feed
"rss:comments":
  - 'http://milianw.de/blog/katekdevelop-hacksprint-up-to-day-4#comments'
"wfw:commentRSS":
  - http://milianw.de/crss/node/122
syndication_feed:
  - http://milianw.de/tag/kate/feed
syndication_feed_id:
  - "8"
syndication_permalink:
  - http://milianw.de/blog/katekdevelop-hacksprint-up-to-day-4
syndication_item_hash:
  - 0aae84bbf7aa85040771956d50d64823
  - 2957e563da0f0d3b665be1300231e267
categories:
  - KDE

---
Woha, quite a few days flew by without me blogging about anything. Thankfully the others started to write so I don&#8217;t have to repeat it all ;-) Instead I&#8217;ll concentrate on stuff I did or learned.

##### GHNS for Snippets

Well, first I think an excuse is in oder: There is a GHNS button for Kate Snippets in 4.4.0 but it&#8217;s broken, neither me nor Joseph had time to acutally use and fix it&#8230; But anyways, I fixed it now for 4.4.1. For 4.5 we&#8217;ll also have an Upload Dialog.

I also added **both** now to KDevelop, you can now upload and download snippets from it. I added a few dump examples but will probably improve it steadily.

##### Kate Performance

On Saturday and Sunday I started to profile Kate highlighting for a large MySQL dump and managed to greatly improve the speed. Actually the funny thing is that I could improve RegExp based highlighting (you still should try to prevent using it, it will always be slower than simple char/string based highlighting). And the knowledge for this optimization I had from my time as an active contributor for [GeSHi][1]. I feel like it was ages ago, he funny :)

So if anybody has a big file that takes ages to load (but only if you use KDE 4.5 trunk or higher), tell me and give me the file. I might find some more ways to optimize different languages.

##### Other Stuff

Other than that I also managed to fix a few more bugs in Kate and KDevelop and had a good time with the other guys here in Berlin. Yesterday I showed some others how I spent quite a few nights: Partying in Berlin is nice :)

Oh and Bernhard showed me a little gem of his: [Kate Standalone][2] which you can use to build _only_ KatePart from kdelibs trunk. This actually works very well (up until some new API from KDE trunk is used).

 [1]: http://qbnz.com/highlighter/
 [2]: http://github.com/shentok/kate-standalone