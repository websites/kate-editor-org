---
title: Kate/KDevelop HackSprint Day 1
author: Milian Wolff

date: 2010-02-13T23:58:53+00:00
excerpt: |
  So, first day of the Kate/KDevelop hacksprint.
  
  We just talked and hacked at the rented flat,got to know each other and had a fun time. Everybody made it more or less in time, even last minute attendee Adymo from Ukraine, nice! Hacking-wise the product...
url: /2010/02/14/katekdevelop-hacksprint-day-1/
syndication_source:
  - 'Milian Wolff - kate'
syndication_source_uri:
  - http://milianw.de/taxonomy/term/170/0
syndication_source_id:
  - http://milianw.de/tag/kate/feed
"rss:comments":
  - 'http://milianw.de/blog/katekdevelop-hacksprint-day-1#comments'
"wfw:commentRSS":
  - http://milianw.de/crss/node/121
syndication_feed:
  - http://milianw.de/tag/kate/feed
syndication_feed_id:
  - "8"
syndication_permalink:
  - http://milianw.de/blog/katekdevelop-hacksprint-day-1
syndication_item_hash:
  - d73de4257946ea0f39d3f14e5b64bdcb
  - 206fb777d9bf01213d0dd51a1c4cb091
categories:
  - KDE

---
So, first day of the Kate/KDevelop hacksprint.

We just talked and hacked at the rented flat,got to know each other and had a fun time. Everybody made it more or less in time, even last minute attendee Adymo from Ukraine, nice! Hacking-wise the productivity wasn&#8217;t that high, esp. for me, but a few patches got committed here and there.

Right now I&#8217;m working on a little speedup for Kate, esp. for big MySQL files - lets see how it turns out. Cullmann showed me a few things I could do so maybe it works out, lets see.

Over the next week I plan to push in user configurable include paths for the PHP plugin and do some more Snippets & Scripting work in Kate, lets see how it turns out. I&#8217;ll go home now, kinda sucks that I don&#8217;t stay with the others here at the flat but have to take a 1h ride into the city&#8230; Berlin is definitely too big :D