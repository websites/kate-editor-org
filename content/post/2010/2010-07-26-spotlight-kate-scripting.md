---
title: 'Spotlight: Kate Scripting'
author: Milian Wolff

date: 2010-07-26T22:51:25+00:00
excerpt: |
  Hey ho everyone.
  
  Dominik asked me to blog about a feature in Kate that is still (sadly!) pretty unknown and seldom used: Kate Scripting. As you should know you can script KatePart completely via JavaScript. As those articles explain, it&#8217;s rather...
url: /2010/07/26/spotlight-kate-scripting/
syndication_source:
  - 'Milian Wolff - kate'
syndication_source_uri:
  - http://milianw.de/taxonomy/term/170/0
syndication_source_id:
  - http://milianw.de/tag/kate/feed
"rss:comments":
  - 'http://milianw.de/blog/spotlight-kate-scripting#comments'
"wfw:commentRSS":
  - http://milianw.de/crss/node/140
syndication_feed:
  - http://milianw.de/tag/kate/feed
syndication_feed_id:
  - "8"
syndication_permalink:
  - http://milianw.de/blog/spotlight-kate-scripting
syndication_item_hash:
  - 62f55c38c16ec92c043aa0044c5a73c8
  - 78a39592b71a9943709c9cafe8f2079e
categories:
  - KDE

---
Hey ho everyone.

Dominik asked me to blog about a feature in Kate that is still (sadly!) pretty unknown and seldom used: _Kate Scripting_. [As][1] [you][2] [should][3] [know][4] you can script KatePart completely via JavaScript. As those articles explain, it&#8217;s rather simple to write functions and put them into a file to have them reusable. But what for those write-use-throwaway kind of cases, where you simply need to get a job done quickly and don&#8217;t want to go through the overhead of writing some full fledged, documented, action-binded, localized script?

##### Utility Functions and why JavaScript rocks

**Note:** Neither <span class="geshifilter"><code class="text geshifilter-text">map</code></span> nor <span class="geshifilter"><code class="text geshifilter-text">filter</code></span> will be shipped with 4.5 to my knowledge, sorry about that. But you can still use the <span class="geshifilter"><code class="text geshifilter-text">each</code></span> helper (see below) to achieve the same with a bit more typing&#8230;

Take a look at <span class="geshifilter"><code class="text geshifilter-text">utils.js</code></span> on current git master: <http://gitorious.org/kate/kate/blobs/master/part/script/data/utils.js>

Put a special note on the helper functions <span class="geshifilter"><code class="text geshifilter-text">map</code></span>, <span class="geshifilter"><code class="text geshifilter-text">filter</code></span> and <span class="geshifilter"><code class="text geshifilter-text">each</code></span> and how they are used to implement e.g. <span class="geshifilter"><code class="text geshifilter-text">rmblank</code></span>, <span class="geshifilter"><code class="text geshifilter-text">[rl]trim</code></span> and the other functions. Cool eh? And the best part, you can reuse them directly from inside KatePart to get a job done:

###### mail-style quoting

Lets assume you write an email or use something like Markdown or Textile and want to quote. You&#8217;ll have to prepend a few lines with the two chars &#8216;> &#8216;. Instead of copy&#8217;n&#8217;pasting like a maniac do this instead and save yourself some breath:

  1. press <span class="geshifilter"><code class="text geshifilter-text">F7</code></span> to open the Kate command line
  2. write e.g. <span class="geshifilter"><code class="text geshifilter-text">map "function(l) { return '&gt; ' + l; }"</code></span>
  3. execute

_Note:_ When you don&#8217;t have anything selected, the whole document will get &#8220;quoted&#8221;.

###### remove lines that match a pattern

This is something everyone needs to do sooner or later, happened quite a few times to me already. I bet <span class="geshifilter"><code class="text geshifilter-text">vim</code></span> has some esoteric command I cannot remember and <span class="geshifilter"><code class="text geshifilter-text">emacs</code></span> has something like [<span class="geshifilter"><code class="text geshifilter-text">C-x M-c M-butterfly</code></span>][5]. But with Kate most users only see search & replace and forfeit to the command line. Well, now it&#8217;s again a good time to use the command line:

  1. press <span class="geshifilter"><code class="text geshifilter-text">F7</code></span> again
  2. write e.g. <span class="geshifilter"><code class="text geshifilter-text">filter "function(l) { return l.indexOf('myNeedle') == -1; }"</code></span>
  3. execute

Now all lines that contain &#8216;myNeedle&#8217; will get removed. Sure, this is &#8220;verbose&#8221; but assuming you know JavaScript it&#8217;s actually quite easy, expendable and - best of all - good to remember. At least for me, YMMV.

###### shortcuts

For simple cases I&#8217;ve now introduced a shortcut way of doing the above, that saves you even more typing, but is limited to simple evaluations like the ones above. If you need something fancy, you&#8217;ll have to stick to the type-intensive way. Aynhow, here&#8217;s the shortcut version of the two scripts:

  1. <span class="geshifilter"><code class="text geshifilter-text">map "'&gt; ' + line"</code></span>
  2. <span class="geshifilter"><code class="text geshifilter-text">filter "line.indexOf('myNeedle') == -1"</code></span>

###### the guts: <span class="geshifilter"><code class="text geshifilter-text">each</code></span> (interesting for users of KDE 4.x, x < 6)

Both of the above are implemented using the <span class="geshifilter"><code class="text geshifilter-text">each</code></span> helper I introduced even before KDE 4.4 afair. If you are using KDE 4.5 and want to do one of the above, a bit more typing is required:

  1. for map, you write something like this:  
    <span class="geshifilter"><code class="text geshifilter-text">each "function(lines) { return lines.map(function(l){ /** actual map code **/ }); }"</code></span>
  2. for filter you do the same but replace <span class="geshifilter"><code class="text geshifilter-text">map</code></span> with <span class="geshifilter"><code class="text geshifilter-text">filter</code></span>:  
    <span class="geshifilter"><code class="text geshifilter-text">each "function(lines) { return lines.filter(function(l){ /** actual filter code **/ }); }"</code></span>

##### Conclusion

You see, it&#8217;s quite simple _and_ powerful. I really love map-reduce and how easy it is to use with JavaScript. Hope you like it as well.

<strike>PS: I actually think about making it yet even easier, by allowing some syntax like this: <span class="geshifilter"><code class="text geshifilter-text">map '&gt; ' + line</code></span> or <span class="geshifilter"><code class="text geshifilter-text">filter line.indexOf('myNeedle') == -1</code></span>, must take a look on how hard it would be (beside the need for extensive documentation, but hey we have the <span class="geshifilter"><code class="text geshifilter-text">help</code></span> command in the Kate CLI, who can complain now? :)</strike> **Implemented**

Bye

 [1]: /2009/10/29/extending-kate-with-scripts/
 [2]: /2009/11/01/scripting-kate/
 [3]: /2010/02/17/developer-meeting-more-on-scripting-kate/
 [4]: /2010/07/09/kate-scripted-actions/
 [5]: http://xkcd.com/378/