---
title: New Text Folding in kate.git master
author: Christoph Cullmann

date: 2013-03-27T21:16:45+00:00
url: /2013/03/27/new-text-folding-in-kate-git-master/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
In the kate.git master branch the text folding is now new and shiny.

In addition to be faster and less memory hungry (no folding tree is around if you fold nothing, it is only created on-demand exactly for the folded regions), the new code is less complex and smaller (and hopefully better documented + unit tested, it actually has a test for most internal operations).

There is actually now a clean separation, the folding does not mix with highlighting and can be used without it, too.

This allows for a better integration of indentation based foldings like in Python (and already fixes the most ugly miss-placements of folding regions there) and empowers the user to fold arbitrary regions manually.

This can be done at the moment with two kate commands (for the F7 command line):

  * &#8220;fold&#8221; will create persistent fold for the current selection, this fold will stay until next reload of document (will implement some dfold command later, to delete it)
  * &#8220;tfold&#8221; will create temporary fold for the current selection, this fold will disappear again on unfolding

This should allow better vi mode integration, too, as vi is able to fold arbitrary regions, too. (There is an internal API to fold any KTextEditor::Range you pass in, which should allow to fold by regex and other ways, too, if somebody steps up to implement it.)

If you find glitches, please report them as bugs and please attach some file to reproduce, I am very interested to get this all in even better shape for KDE 4.11, be folding crashs a thing of the past.

P.S.

For all people not using folding at all, be happy, it no longer will crash you (and me), as it is a nop if no folding ranges are created by the user and some extreme cases got a speedup of 1000% because of less overhead, like VERY large C++ sources ;)