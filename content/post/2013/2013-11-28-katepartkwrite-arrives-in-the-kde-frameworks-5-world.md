---
title: KatePart/KWrite arrives in the KDE Frameworks 5 world
author: Christoph Cullmann

date: 2013-11-28T22:03:20+00:00
url: /2013/11/28/katepartkwrite-arrives-in-the-kde-frameworks-5-world/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
After starting the &#8220;frameworks&#8221; branch in kate.git more than a week ago but doing not much beside an initial KTextEditor compile & link port I felt a big guilty ;)

Given a lot of people blog about the progress of programs X and Y for Qt 5.2 and KDE Frameworks 5 I guess it is time that KatePart joins this club.

Some hours later, a &#8216;working&#8217; version of KatePart and KWrite have landed in the &#8220;frameworks&#8221; branch of kate.git. KWrite launches, loads the part and the open action even works (command line parsing is off-line btw. ATM).

From the <a title="Commit to have some working KWrite on KF5" href="http://quickgit.kde.org/?p=kate.git&a=commit&h=90a97f7ad51e3d7e4af57265133dad37f57f2e89" target="_blank">let it run ;)</a>  commit on, KWrite should at least launch and open files via file dialog and here is the mandatory screenshot (a KDE Frameworks 5 KWrite over an Kate 4.x window):

[<img class="aligncenter size-full wp-image-3013" title="KWrite on KDE Frameworks 5" src="/wp-content/uploads/2013/11/kwrite_on_kf5.png" alt="" width="769" height="615" srcset="/wp-content/uploads/2013/11/kwrite_on_kf5.png 769w, /wp-content/uploads/2013/11/kwrite_on_kf5-300x239.png 300w" sizes="(max-width: 769px) 100vw, 769px" />][1]

I marked all places where I commented stuff out or did a hack with &#8220;FIXME KF5&#8221; in a comment or &#8220;#if 0&#8221; region. Help welcome, Kate application still to be ported ;) But please mark your &#8220;hacks&#8221; in the same fashion, otherwise, we will never find them again.

To remember the history, the initial KatePart KDE 4 port happened more than 8 years ago, here a screenshots of that time (2005-05-15):

[<img class="aligncenter size-full wp-image-3019" title="KWrite on KDE 4 - Initial Port" src="/wp-content/uploads/2013/11/katetest-kde4-20050515-more.png" alt="" width="856" height="497" srcset="/wp-content/uploads/2013/11/katetest-kde4-20050515-more.png 856w, /wp-content/uploads/2013/11/katetest-kde4-20050515-more-300x174.png 300w" sizes="(max-width: 856px) 100vw, 856px" />][2]

I think the first KDE Frameworks 5 version looks a bit better. Thanks a lot for all the people getting both the kdelibs frameworks branch in such good shape and all the contributors to Qt 5.x!

The Kate team will do its best to make the KDE Frameworks 5 version of Kate as popular <a href="http://aseigo.blogspot.de/2013/11/kate-has-won.html" target="_blank">as the 4.x variant is</a>.

 [1]: /wp-content/uploads/2013/11/kwrite_on_kf5.png
 [2]: /wp-content/uploads/2013/11/katetest-kde4-20050515-more.png