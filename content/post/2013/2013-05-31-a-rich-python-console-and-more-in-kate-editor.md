---
title: A rich python console and more in Kate editor
author: Pablo Martin

date: 2013-05-31T08:32:48+00:00
url: /2013/05/31/a-rich-python-console-and-more-in-kate-editor/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
tags:
  - django
  - planet
  - python

---
I have done some improvements in the plugins: python\_console\_ipython, python\_autocomplete, python\_utils, js\_utils, xml\_pretty and django_utils. <a href="/2013/02/18/new-plugins-to-the-kate-utils-to-python-javascript-django-and-xml/" target="_blank">These plugins</a> I added a month and a half ago (except python\_console\_ipython) to the kate repository. I have done two improvements and a new feature:

  * Now they work with Python2 and <a href="http://docs.python.org/3/howto/pyporting.html" target="_blank">Python3</a> (except python_autocomplete, this only works with Python2, due <a title="pysmell" href="https://pypi.python.org/pypi/pysmell" target="_blank">pysmell</a> dependence)
  * Now they have a configuration page (except python_autocomplete, this should not have configuration parameters)

[<img class="aligncenter  wp-image-2456" src="/wp-content/uploads/2013/04/config.png" alt="" width="459" height="331" srcset="/wp-content/uploads/2013/04/config.png 765w, /wp-content/uploads/2013/04/config-300x216.png 300w" sizes="(max-width: 459px) 100vw, 459px" />][1]

<h6 style="text-align: center">
  <em>Page Config Plugins</em>
</h6>

The new feature is the integration of the python\_autocomplete and python\_console_ipython plugins with the <a title="Using the projects plugin in Kate" href="/2012/11/02/using-the-projects-plugin-in-kate/" target="_blank">project plugin</a>. The behaviour of these plugins depends of the loaded project. That is to say, the python_autocomplete plugin autocompletes with our project modules.<span style="text-align: center"> Currently the kate community is working to add a new<a href="https://git.reviewboard.kde.org/r/109326/" target="_blank"> python autocomplete</a>  using <a title="jedi" href="https://github.com/davidhalter/jedi" target="_blank">jedi</a> (this will work with Python2 and Python3).</span>

<p style="text-align: center">
  <a href="/wp-content/uploads/2013/04/autocomplete.png"><img class="aligncenter  wp-image-2460" src="/wp-content/uploads/2013/04/autocomplete.png" alt="" width="472" height="281" srcset="/wp-content/uploads/2013/04/autocomplete.png 674w, /wp-content/uploads/2013/04/autocomplete-300x178.png 300w" sizes="(max-width: 472px) 100vw, 472px" /></a>
</p>

<h6 style="text-align: center">
  <em>Python Autocomplete (with our modules)</em>
</h6>

<p style="text-align: left">
  And the python_console_ipython plugin has something that we can name the project console. A ipython shell with the python path of our project and with the environments variables that the project plugin needs.
</p>

<p style="text-align: center">
  <a href="/wp-content/uploads/2013/04/ipython_console.png"><img class="aligncenter  wp-image-2463" src="/wp-content/uploads/2013/04/ipython_console.png" alt="" width="698" height="388" srcset="/wp-content/uploads/2013/04/ipython_console.png 1164w, /wp-content/uploads/2013/04/ipython_console-300x166.png 300w, /wp-content/uploads/2013/04/ipython_console-1024x569.png 1024w" sizes="(max-width: 698px) 100vw, 698px" /></a>
</p>

<h6 style="text-align: center">
  <em>IPython Console (converted in a django shell)</em>
</h6>

<p style="text-align: left">
  To use this we need to add in the project file (.kateproject) a new attribute named &#8220;python&#8221;, with this structure:
</p>

<pre>{
  "name": "MyProject",
  "files": [ { "git": 1 } ],
  "python": {
        "extraPath": ["/python/path/1",
                      "/python/path/2",
                      "/python/path/n"
        ],
        "environs": {"key": "value"},
        "version": "3.2"
    }
 }</pre>

I am a django developer, as we say in <a title="Django project" href="http://www.djangoproject.com/" target="_blank">Django</a> we can come to have a <a title="django shell" href="https://docs.djangoproject.com/en/dev/intro/tutorial01/#playing-with-the-api" target="_blank">django shell</a> (python manage.py shell) integrated in our editor. But this feature is a generic feature not tied to Django. This feature can be used by other Python developers. I have added only a specific feature to the django developers. If we set in the project file (.kateproject) the attribute projectType with the value Django, the ipython shell automatically loads the models and the settings like a <a title="Django extensions" href="https://pypi.python.org/pypi/django-extensions" target="_blank">shell_plus</a> does.

<pre>{
   "name": "MyProject",
   "files": [ { "hg": 1 } ],
   "python": {
        "extraPath": ["/python/path/1",
                      "/python/path/2",
                      "/python/path/n"
        ],
        "environs": {"DJANGO_SETTINGS_MODULE": "myproject.settings"},
        "version": "2.7.3",
        "projectType": "django"
    }
  }</pre>

I hope you like it :-)

 [1]: /wp-content/uploads/2013/04/config.png