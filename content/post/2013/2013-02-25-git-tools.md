---
title: Git Tools
author: Dominik Haumann

date: 2013-02-25T19:51:11+00:00
url: /2013/02/25/git-tools/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users
tags:
  - planet

---
[The Projects plugin][1] in Kate just gained a context menu for the tree view that shows several git tools, if available:

<img class="aligncenter size-full wp-image-2289" title="Context Menu of Projects Tree View" src="/wp-content/uploads/2013/02/git-tools.png" alt="" width="447" height="434" srcset="/wp-content/uploads/2013/02/git-tools.png 447w, /wp-content/uploads/2013/02/git-tools-300x291.png 300w, /wp-content/uploads/2013/02/git-tools-50x50.png 50w" sizes="(max-width: 447px) 100vw, 447px" /> 

Clicking will open the corresponding application in the correct working directory. Currently, only <a title="gitk" href="http://www.kernel.org/pub//software/scm/git/docs/gitk.html" target="_blank">gitk</a>, <a title="qgit" href="http://sourceforge.net/projects/qgit/" target="_blank">qgit</a> and <a title="git-cola" href="http://git-cola.github.com/" target="_blank">git-cola</a> are supported. If you want more git integration, you probably have to use <a title="KDevelop" href="http://www.kdevelop.org/" target="_blank">KDevelop</a> or <a title="QtCreator" href="http://doc.qt.digia.com/qtcreator-2.4/" target="_blank">QtCreator</a> :-)

 [1]: /2012/11/02/using-the-projects-plugin-in-kate/ "Using the Kate Projects Plugin"