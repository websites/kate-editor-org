---
title: 'Kate XML Completion: Converting DTD to MetaDTD'
author: Dominik Haumann

date: 2013-10-26T11:36:47+00:00
url: /2013/10/26/kate-xml-completion-converting-dtd-to-metadtd/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users
tags:
  - planet

---
Kate has this nifty little plugin called &#8220;<a title="Kate XML Completion Plugin" href="http://docs.kde.org/development/en/applications/kate/kate-application-plugin-xmltools.html" target="_blank">XML Completion</a>.&#8221; This plugin loads a Meta DTD file and uses this information for context sensitive completion. To use it, you first have to load it in the settings dialog, and then assign a Meta DTD through the XML menu:

<img class="size-full wp-image-2904 aligncenter" title="Kate XML Completion: Assign Meta DTD" src="/wp-content/uploads/2013/10/assign-dtd.png" alt="" width="511" height="178" srcset="/wp-content/uploads/2013/10/assign-dtd.png 511w, /wp-content/uploads/2013/10/assign-dtd-300x104.png 300w" sizes="(max-width: 511px) 100vw, 511px" /> 

In our example, we work on a Kate XML highlighting definition file and therefore loaded the file &#8220;language.dtd.xml&#8221; which is shipped with Kate. Having assigned a Meta DTD file, we now have these nice code hints:

<img class="aligncenter size-full wp-image-2905" title="Kate XML Completion in Action" src="/wp-content/uploads/2013/10/xml-completion.png" alt="" width="574" height="251" srcset="/wp-content/uploads/2013/10/xml-completion.png 574w, /wp-content/uploads/2013/10/xml-completion-300x131.png 300w" sizes="(max-width: 574px) 100vw, 574px" /> 

Kate ships with several Meta DTD files, such as HTML4 (strict, loose) or XHTML 1.0 transitional, KConfigXT DTD, KPartsGUI or XSLT. While this is really cool, you may ask about arbitrary DTDs you may be using. Unfortunately, Kate only supports Meta DTD, so what now?

### Installing dtdparser

Luckily, the tool <a title="DTDParse" href="http://nwalsh.com/perl/dtdparse/index.html" target="_blank">dtdparser</a> (<a title="DTD Parser on Sourceforge" href="http://sourceforge.net/projects/dtdparse/" target="_blank">on sourceforge</a>) converts a DTD to Meta DTD. We first need to install dtdparse. Since openSUSE does not provide a package (what about other distros?), I downloaded <a title="dtdparse download" href="http://sourceforge.net/projects/dtdparse/files/latest/download?source=dlp" target="_blank">SGML-DTDParse-2.00.zip</a>, extracted it and ran (see README file)

<pre style="padding-left: 30px;">perl Makefile.PL</pre>

Make sure there are no missing perl dependencies. I for instance had to install perl-Text-DelimMatch and perl-XML-DOM:

<pre style="padding-left: 30px;">sudo zypper install perl-Text-DelimMatch perl-XML-DOM</pre>

Then continue with the build and install process (the result of make test should be PASS):

<pre style="padding-left: 30px;">make
make test
sudo make install<span style="font-family: Georgia, 'Times New Roman', 'Bitstream Charter', Times, serif; font-size: 13px; line-height: 19px;"> </span></pre>

Now we successfully installed dtdparse on the system. So we are finally ready to convert DTDs.

### Converting DTD to Meta DTD with dtdparser

Having installed dtdparser, it is as easy as calling

<pre style="padding-left: 30px;">dtdparse file.dtd &gt; file.dtd.xml</pre>

to convert a DTD to Meta DTD. The conversion should work out of the box. If you want, you can edit the generated .xml file, for instance the &#8220;title&#8221; attribute is always set to &#8220;?untitled?&#8221;. While this is not used by the XML Completion plugin (yet?), it&#8217;s still nicer to have it properly fixed.

### Contributing Meta DTDs to Kate

Whenever you have a DTD that is of use also for other users, please send the generated Meta DTD to [kwrite-devel@kde.org][1] (our <a title="Kate Mailing List" href="https://mail.kde.org/mailman/listinfo/kwrite-devel" target="_blank">mailing list</a>). Further, it would be really cool if someone added support to convert DTDs on the fly to Meta DTD, so the Kate XML Completion plugin would just work for DTDs as well. Any takers?

### Call at Distribution Packagers

Please consider including dtdparser by default, as it seems to be a very useful too. Are there alternatives to convert DTD to Meta DTD?

 [1]: mailto:kwrite-devel@kde.org "Mail To kwrite-devel@kde.org"