---
title: Kate Git Statistics
author: Christoph Cullmann

date: 2013-02-27T19:26:43+00:00
url: /2013/02/27/kate-git-statistics/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
Now the statistics of the <a href="https://projects.kde.org/projects/kde/kde-baseapps/kate/repository" title="kate.git" target="_blank">kate.git</a> are online for public viewing.  
They will be updated daily, located on: <a title="Kate Git Statistics" href="/stats/" target="_blank">/stats/</a>

Unfortunately, the statistics of the last years are not that &#8220;representative&#8221;, as the moves of Kate around in SVN and to Git biased the statistics, as I did a lot of the commits for syncing and moving and so on.

Still it is amazing how MANY people did contribute during Kate&#8217;s history! (see <a href="/stats/authors/best_authors.html" title="Kate Authors" target="_blank">Kate Authors</a>)

A big THANK YOU to all contributors that appear there (and all the people that provided patches others merged in, did the testing, bug reporting, translations, &#8230;).

(And thanks to the authors of <a href="https://github.com/tomgi/git_stats" title="git_stats" target="_blank">git_stats</a>.)