---
title: Akademy 2013
author: Dominik Haumann

date: 2013-06-20T07:08:01+00:00
url: /2013/06/20/akademy-2013/
pw_single_layout:
  - "1"
categories:
  - Events
  - KDE
tags:
  - planet

---
So <a title="Akademy 2013" href="http://akademy2013.kde.org/" target="_blank">Akademy 2013</a> is nearing and this time it takes place at <a title="Bilbao on Wikipedia" href="http://en.wikipedia.org/wiki/Bilbao" target="_blank">Bilbao, Spain</a>. As it happens, Bilbao is located quite near to the beach as well as very close to the mountains. So I stumbled over a page with several <a title="Hiking trails in Bilbao" href="http://de.wikiloc.com/routen/wandern/spain/basque-country/bilbao" target="_blank">hiking trails starting in Bilbao</a> ranging from easy to hard. For instance, <a title="Tough hiking trail" href="http://de.wikiloc.com/wikiloc/view.do?id=3734247" target="_blank">this route</a> starts in a nearby village and then goes from about 65 meters in height up to 1000 (this is the point where you stop to hack on KDE, of course), and then down to 30 in Bilbao again. Anyone? :-)

PS: Maybe there are any locals who know the place around and can recommend a hiking track?

&nbsp;

<img class="aligncenter" src="http://akademy2013.kde.org/sites/akademy2013.kde.org/files/Ak2013Badge2.png" alt="" />