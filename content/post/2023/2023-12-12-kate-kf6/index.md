---
title: Kate KF6 Status
author: Christoph Cullmann
date: 2023-12-12T22:07:00+02:00
url: /post/2023/2023-12-12-kate-kf6/
---

## Current state of the port

Thanks to the help of our contributors the current state of Kate for the upcoming first Qt & KF 6 release looks very promising.
This includes not just people working on Kate and KTextEditor/KSyntaxHighlighing, but all of KDE Frameworks and Qt.

I now use the KF 6 based version both at work and home exclusively after we switched the master branch over to that.
So far, beside the natural issues that can occur using a branch under active development, nothing really did stick out as a blocking issue.

## Build it yourself and contribute

Our official [Build It](/build-it/) guide now shows how to get the KF 6 version, too.
That is the version we want to have new contributions for, the KF 5 variant is now in pure bugfix mode.
If you don't just want to fix a pure KF 5 bug, please aim to contribute to the current master branch.
We can still backport stuff, if really needed.

We encourage you to have a look at our [merge requests](/merge-requests/), see what else is going on there and maybe start working towards your very first own contribution.
The current state for the first release based on Qt & KF 6 is very good, but naturally, there is always stuff to improve!

## Comparison of the KF 6 and KF 5 versions

No post about some state without some proper screenhots ;0

Here the current state of Kate on KF 6, with current Breeze and Konsole part, built like described on [Build It](/build-it/):

![Kate KF 6](/post/2023/2023-12-12-kate-kf6//images/kate-kf6.png)

More or less the same with the latest KF 5 release as shipped with NixOS:

![Kate KF 5](/post/2023/2023-12-12-kate-kf6//images/kate-kf5.png)

Beside the small cleanups in the design both in Kate and Breeze, I think that looks more or less identical.

Naturally under the hood a lot of things did change, but there are no disruptive changes and all features we had in the current stable version did survive the KF 6 port.

We had more or less the same amount of feature development and bug fixing like during the KF 5 time frame, just with some KF 6 porting 'sprints' mixed in.

Thanks again to all that made that feasible.

Special thanks to Waqar Ahmed from me, he did not only get [Konsole to work on Windows](/post/2023/2023-02-15-kate-win-terminal/) in KF 5, he did help a lot to get that ported to KF 6, too, beside a lot of work on all other parts of Kate and Co.!
All that work is invaluable, thanks a lot :)

Just take a look at our [team page](/the-team/), more than 100 people did contribute to our stuff just in the last 12 months, that is amazing.
And not just one or two lines.
During the porting a lot of other people out of the KDE community stepped up to work on API adaptions and cleanups, that is awesome.
Not only did Frameworks improve, people do care and fix up the users of changed API, too.

## End of the year outlook

I think the current state of the port is good.
We can be proud that we are more than ready for the upcoming 'megarelease'.

If you have time to test the [beta](https://kde.org/announcements/megarelease/6/beta1/) or other snapshots, feedback is welcome.
Please report bugs and if you can, please help to fix them.

Not all is well in the world, but at least from the perspective of the move from KF 5 to 6, for Kate/KWrite, all seems to have worked out very good this year.

In any case, I wish all people a good end of this and an even better start in the next year.

If you celebrate Christmas or any other festivity in the next weeks, I hope you have there a good time with family and friends!
