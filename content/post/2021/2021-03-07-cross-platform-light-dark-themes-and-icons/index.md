---
title: Cross Platform Light & Dark Themes and Icons
author: Christoph Cullmann
date: 2021-03-07T14:11:00+02:00
url: /post/2021/2021-03-07-cross-platform-light-dark-themes-and-icons/
---

## The State on X11/Wayland (more or less since a few years)

On the most Unices that use X11/Wayland and therefore are capable of running the full [Plasma Desktop](https://kde.org/plasma-desktop/) the state of light & dark themes and the accompanied icon themes is really good for KDE Frameworks based application.

Just take a look at these two screenshots of a light and dark mode Kate running on GNU Linux/X11 & Plasma Desktop.

![Light themed Kate on Plasma/X11](/post/2021/2021-03-07-cross-platform-light-dark-themes-and-icons/images/kate-plasma-light.png)

![Dark themed Kate on Plasma/X11](/post/2021/2021-03-07-cross-platform-light-dark-themes-and-icons/images/kate-plasma-dark.png)

To create these, I even just switched between some light and dark Breeze themes on the fly via "Settings -> Color Scheme".

To be honest, the 20.12 version of Kate did then still miss to update some colors like in the Documents pane, that is fixed in master and therefore with the upcoming 21.04.
But overall, even 20.12 works well, in the worst case, you need to restart your application to have properly applied themes.
Given one (at least me) only rarely switches the color theme, that seems "OK".

## The State on Windows & macOS (January 2021)

Unfortunately, the state on other platforms isn't that charming.

Whereas the theme chooser in principle works even on Windows and macOS, one serious issue exists there:
The color is switched but the icons are not re-colored.
This leads to unusable results like can be seen below in the screenshot taken 1:1 from [this bug for Kate](https://bugs.kde.org/show_bug.cgi?id=432085).

![Dark themed Kate on Windows with broken icons](/post/2021/2021-03-07-cross-platform-light-dark-themes-and-icons/images/kate-windows-dark-broken.png)

The version there still had an old KTextEditor framework that didn't follow the global theme color to choose the matching KSyntaxHighlighting theme.
That was already rectified [in Frameworks 5.75 as described here](/post/2020/2020-09-06-kate-color-themes/).
Still, you end up with a program that has the most icons just close to invisible, as they stay black, even for dark themes.
This is no Kate specific issue, e.g. [Okular](https://okular.kde.org) will just look the same.

Naturally not all applications show these issues.
Some ship their own icons/styles/themes or will you just not let switch your theme to something dark.
Thought, if you do no special handling, like in Kate/Okular/Filelight/... and I assume most stuff we have around on invent.kde.org, the Windows and macOS builds will show the above behavior if you get them to use dark mode at all.

## How to fix these Icon Issue on Windows & macOS?

This issue was known since long, we just had no time to tackle it.

First, some generic info about how icon themes work on non-Unices for KDE Frameworks base application (at least if they have no own logic for that).

* The Breeze icons (the light variant usually) are installed inside the application bundle as binary Qt resource file.
For example the [binary-factory.kde.org](https://binary-factory.kde.org) tooling will do this via [Craft](https://community.kde.org/Craft).

* Some magic inside the KIconThemes Framework detects if such an icon theme resource file is around and does the needed setup to use that.
If you need details, take a look in [kicontheme.cpp](https://invent.kde.org/frameworks/kiconthemes/-/blob/master/src/kicontheme.cpp) and search for *initRCCIconTheme*.
It is open source, you can see all the awful stuff we do in no time ;=)

We went for this method [years ago](https://invent.kde.org/frameworks/kiconthemes/-/commit/7bcfa8a57444f6d8982e5da40d00cd8c8dd85663) to ensure we don't need to deploy XXX individual files and to have no need for boiler plate code in the individual applications.
As long as you link KIconThemes in some way, you are ready to go.
This might be hacky, but it works ;=)

Why does this inhibit the proper icon coloring?

The icon coloring is handled by  the KIconThemes Framework, too.
The SVG Breeze icons are processed on load and adapted to the current palette.
This works perfect, as long as the KIconLoader is at all used.

On your typical Plasma Desktop this is the case, as the *KdePlatformTheme* will ensure that the *KIconEngine* is used as QIconEngine for your application.
Unfortunately, that will cease to work if you either set a theme via QIcon::setThemeName like we do for our resource hack or if you don't have this platform theme loaded, as is the case for Windows & macOS.

One first idea how to fix this was to bundle both light & dark theme and just switch between them on application palette change.
We drafted [some prototype](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/79) for this.
Unfortunately, as least without more hacks, this would have needed some application modification to setup it, here [the boiler plate for Kate](https://invent.kde.org/utilities/kate/-/merge_requests/204).

After some more research, be better idea came to mind: why not let Qt just use the *KIconEngine* always as icon engine for SVG icons?
Then this would work even if some theme name is set or the platform theme doesn't know about it.

Interesting enough, KIconThemes already had a icon engine plugin, we just never registered it properly, as it was just thought for some serialization.
This [was now merged for Frameworks 5.80](https://invent.kde.org/frameworks/kiconthemes/-/merge_requests/21) and the Craft blueprints already have it as patch.
The latest 20.12 store builds of e.g. Kate/Okular/Filelight have this already deployed.

## The State on Windows & macOS (March 2021)

This means, we now have the following state on Windows and macOS (light & dark mode) with the Frameworks 5.80 variant without any adaptions to the application code.
The only requirement is that the KIconThemes framework is deployed inside the application bundle.

The current Windows Store version of Kate 20.12:

![Light themed Kate on Windows](/post/2021/2021-03-07-cross-platform-light-dark-themes-and-icons/images/kate-windows-light.png)

![Dark themed Kate on Windows](/post/2021/2021-03-07-cross-platform-light-dark-themes-and-icons/images/kate-windows-dark.png)

The current builds of Kate for macOS from the [Binary factory links on this page](/get-it/).

![Light themed Kate on macOS](/post/2021/2021-03-07-cross-platform-light-dark-themes-and-icons/images/kate-macos-light.png)

![Dark themed Kate on macOS](/post/2021/2021-03-07-cross-platform-light-dark-themes-and-icons/images/kate-macos-dark.png)

If you look closely to the macOS screenshots, you see there is still the above mentioned Documents pane error visible: the icon/text color there didn't swap, is fixed in master ;=)

## More help welcome ;=)

Naturally, not all is great.
The changes for this did show some difference in KIconEngine/KIconLoader vs. default SVG QIconEngine regarding non-square icons.
This [should be fixed](https://invent.kde.org/frameworks/kiconthemes/-/merge_requests/23) for Frameworks 5.81.
As you can see in all linked merge requests, we are short at hands, if you happen to have more issues with icons (or other stuff), [please show up and help out](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/)!

# Comments?

A matching thread for this can be found here on [r/KDE](https://www.reddit.com/r/kde/comments/lzqfv9/cross_platform_light_dark_themes_and_icons/).
