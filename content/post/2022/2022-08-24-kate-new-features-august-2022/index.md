---
title: Kate - New Features - August 2022
author: Christoph Cullmann
date: 2022-08-24T21:35:00+02:00
url: /post/2022/2022-08-24-kate-new-features-august-2022/
---

The 22.08 release of Kate hasn't arrived for many users yet, but we already have new cool stuff for upcoming releases.

As our [merge requests](/merge-requests/) page shows, alone in August we got at least [66 new things done](/merge-requests/2022/08/).
Naturally that are not all new features, but bug fixes, too.

Pablo Rauzy was nice enough to provide some short videos for the enhancements he contributed!
Thanks a lot for that, and thanks to all people that helped to work out the merge requests he submitted.

## The align command

First, some feature that was added on the [KTextEditor level](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/391), that means all applications using that framework, like Kate, KWrite, KDevelop, Kile, and Co. will profit.

The new command aligns lines in the selected block or whole document on the column given by a regular expression that you will be prompted for.

<video width=100% controls>
<source src="/post/2022/2022-08-24-kate-new-features-august-2022/videos/kate-align-on_h264.mp4" type="video/mp4">
Your browser does not support the video tag :P</video>

This was not only exposed as an UI action, it is available in the integrated command line as "alignon" and in the JS bindings, too.
Refer to the Kate manual for more information.

## Intuitive movements between split views

The Vi mode had this already via Vi commands, now it is available as [some UI actions](https://invent.kde.org/utilities/kate/-/merge_requests/836), too, that can be assigned shortcuts.

You can move between the individual split views by direction.

<video width=100% controls>
<source src="/post/2022/2022-08-24-kate-new-features-august-2022/videos/kate-intuitive-splitview-movements_h264.mp4" type="video/mp4">
Your browser does not support the video tag :P</video>

## Keyboard Macros Plugin

Last but not least, perhaps the most anticipated feature of these three: [a macro recording plugin](https://invent.kde.org/utilities/kate/-/merge_requests/823)

We had requests for this since close to ever, now we have some initial version.

More details are in the manual, too, below a short demonstration.

<video width=100% controls>
<source src="/post/2022/2022-08-24-kate-new-features-august-2022/videos/kate-keyboard-macros-select-commands_h264.mp4" type="video/mp4">
Your browser does not support the video tag :P</video>

## More?

There is more stuff done and still in work, if you skim over the list of [merge requests](/merge-requests/).

A nice thing to mention is: Pablo Rauzy is a new contributor, that means it is feasible to really contribute non-trivial features to Kate without months or years of experience on our code base.
Naturally C++/Qt/KF5 isn't the easiest entry level, but it is nothing insurmountable and our contributors are for sure trying to help out new people to find their ways and get things done.

If you are in doubt, just read the above linked merge requests for the new things shown in this post.

## Comments?

A matching thread for this can be found here on [r/KDE](https://www.reddit.com/r/kde/comments/wwsm57/kate_new_features_august_2022/).
