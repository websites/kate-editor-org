---
title: KF6 Sprint - Day Two
author: Christoph Cullmann
date: 2019-11-23T18:19:00+02:00
url: /post/2019/2019-11-23-kf6-sprint-day-two/
---

Second day of the KF6 sprint at the [MBition](https://mbition.io/en/home/index.html) office in Berlin.

Kevin prepared a work board to structure what we do.

<p align="center">
    <a href="/post/2019/2019-11-23-kf6-sprint-day-two/images/workboard-morning.jpg" target="_blank"><img width=500 src="/post/2019/2019-11-23-kf6-sprint-day-two/images/workboard-morning.jpg"></a>
</p>

We split up in four groups that will tackle tier 3 frameworks and review them for wanted/needed changes.
Stuff like cleaning up dependencies or API cleanups or even full deprecation for stuff that were porting aids already for KF5.

Before lunch we arrived at the following state for the first debriefing round.

<p align="center">
    <a href="/post/2019/2019-11-23-kf6-sprint-day-two/images/workboard-lunch.jpg" target="_blank"><img width=500 src="/post/2019/2019-11-23-kf6-sprint-day-two/images/workboard-lunch.jpg"></a>
</p>

Given I am the KTextEditor maintainer, the group I was in picked that up.

We could remove the [KIconThemes dependency](https://phabricator.kde.org/D25485), easy, beside that, we created a task to take a look at the [KF6 porting hints](https://phabricator.kde.org/T12103) we have in the framework.

All the tedious removing of deprecated Qt stuff and porting e.g. away from Q_FOREACH was luckily already done, thanks again to all people working on that in the past and future!

Beside the easy to remove KIconThemes stuff, the other dependencies are unavoidable, given we provide a KPart with some kind of network transparency.

We are still discussing the stuff before lunch now and after lunch the groups will grab more of the yet marked as TODO frameworks.

After some more group work after lunch, we arrived at this state:

<p align="center">
    <a href="/post/2019/2019-11-23-kf6-sprint-day-two/images/workboard-post-lunch.jpg" target="_blank"><img width=500 src="/post/2019/2019-11-23-kf6-sprint-day-two/images/workboard-post-lunch.jpg"></a>
</p>

Keep track of the circle of doom that needs attention by David Faure and the D4 post-it that tells were our virtual David takes part in the discussion :=)

The end of the day state can be seen below:

<p align="center">
    <a href="/post/2019/2019-11-23-kf6-sprint-day-two/images/workboard-dinner.jpg" target="_blank"><img width=500 src="/post/2019/2019-11-23-kf6-sprint-day-two/images/workboard-dinner.jpg"></a>
</p>

Now soon time for dinner :-)

For all concrete results, we created tasks on the [KF6 board](https://phabricator.kde.org/tag/kf6/) on the KDE Phabricator.

If you are interested to help in shaping the future of KF6, show up and grab some task :=)

There is more than enough to do for everybody.
