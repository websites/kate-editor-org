---
title: kate-editor.org Update
author: Christoph Cullmann
date: 2019-08-03T20:48:00+02:00
url: /post/2019-08-03-kate-editor-org-update/
---

Like my personal homepage [cullmann.io](https://cullmann.io), I ported the [kate-editor.org](https://kate-editor.org) website to the [Hugo framework](https://gohugo.io/).

The new website uses zero cookies (yes, no stupid cookie question) and no kind of analytic software.
It should be self-contained, too.
No external resources that will leak your data to e.g. Google are requested.

But more important: unlike before, the website content will be fully hosted in a git repository on KDE infrastructure.

The current intermediate location is on [invent.kde.org/websites/kate-editor-org](https://invent.kde.org/websites/kate-editor-org).

Before, the KDE sysadmin team just got access to a backup of the WordPress instance.

The new website contains an import of all old pages & posts.
I hopefully preserved the old URLs, but there might be some minor glitches for some older posts that still need fixing.

Some pages still have broken markup, that needs some fixes, too.

But all-in-all the import of the WordPress content did work really well.

Some nice step-by-step description how to do such a port can be found in the ["Moving 18 years of this personal blog from Wordpress to Hugo"](https://cpbotha.net/2019/03/31/wordpress-to-hugo/) post.

If you want to contribute, feel free to send me some patch or merge request ;=)

As a simple template for a new post, take a look at the sources of this one:

<pre>
---
title: kate-editor.org Update
author: Christoph Cullmann
date: 2019-08-03T20:48:00+02:00
---

Like my personal homepage [cullmann.io](https://cullmann.io), I ported the [kate-editor.org](https://kate-editor.org) website to the [Hugo framework](https://gohugo.io/).
...
</pre>

The date is important, no stuff in the future is visible on the real web server.
You can hide stuff from publishing by adding
<pre>
draft: true
</pre>
to the header information. The *server.sh* script will still show such things on your local machine.
