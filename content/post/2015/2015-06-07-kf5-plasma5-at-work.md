---
title: 'KF5 & Plasma 5 at work ;=)'
author: Christoph Cullmann

date: 2015-06-07T13:46:25+00:00
url: /2015/06/07/kf5-plasma5-at-work/
categories:
  - Developers
  - KDE
tags:
  - planet

---
In the last months, I didn&#8217;t get much time to work on Kate nor KTextEditor. Beside some small bugfixes and cleanups I got nothing commited :/

Guess one of the main issues is, I don&#8217;t use the KF5 based version of Kate that much ATM. At home, it is the only editor I use, but I have not much time to hack at home anyway.

Therefore today I installed the same setup on my work machine and will now use the KF5 based Kate for all of my day job, perhaps that motivates me more to take a look at the issues the Qt5/KF5 port still has.
(And I will work in a KDE Plasma 5 session with most stuff KF5 based now, including all important stuff like the nice Konsole ;=)

First update: The [Get It][1] page got an update to show how to install KF5 based Kate/KWrite from kate.git on openSUSE 13.2 ;) Feel free to send in the needed hints for other distros (to kwrite-devel@kde.org).

 [1]: /get-it/
