---
title: Reception of Plasma Mobile
author: Dominik Haumann

date: 2015-07-26T10:04:04+00:00
url: /2015/07/26/reception-of-plasma-mobile/
categories:
  - Developers
  - Events
  - KDE
  - Users
tags:
  - planet

---
The yearly KDE conference Akademy is currently being held with lots of interesting talks and workshops. One big thing that was announced yesterday is <a href="https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform" target="_blank">Plasma Mobile, a free mobile platform</a>.

[<img class="aligncenter size-full wp-image-3559" src="/wp-content/uploads/2015/07/phone-still-small.jpg" alt="Plasma Mobile, a Free Mobile Platform" width="400" height="256" srcset="/wp-content/uploads/2015/07/phone-still-small.jpg 400w, /wp-content/uploads/2015/07/phone-still-small-300x192.jpg 300w" sizes="(max-width: 400px) 100vw, 400px" />][1]

The presentation of Plasma Mobile was quite impressive: A video of using Plasma Mobile on a Nexus 5 showed a nice  visual design, smooth transitions and quite some usable functionality already. This impression was confirmed later when I was playing around with Plasma Mobile on the Nexus 5 myself.

So good job, plasma team!

Plasma Mobile already raised quite some interest in lots of news sites, with lots of user comments (mostly positive), see for instance:

  * Slashdot: <a href="http://tech.slashdot.org/story/15/07/25/1326220/kde-community-announces-fully-open-source-plasma-mobile" target="_blank">KDE Community Announces Fully Open Source Plasma Mobile</a>
  * OSNews: <a href="http://www.osnews.com/story/28730/Plasma_Phone_OS_a_KDE_project_for_mobile" target="_blank">Plasma Phone OS, a KDE project for mobile</a>
  * Hacker News: <a href="https://news.ycombinator.com/item?id=9947146" target="_blank">Plasma Mobile</a>
  * Reddit: <a href="http://www.reddit.com/r/kde/comments/3ek970/plasma_mobile_on_nexus_5_kwinwayland_in_action/" target="_blank">Plasma Mobile on Nexus 5 (Kwin/Wayland in action)</a>
  * Golem: <a href="http://www.golem.de/news/konkurrenz-fuer-jolla-und-ubuntu-phone-kde-plasma-5-laeuft-auf-smartphones-1507-115426.html" target="_blank">KDE Plasma 5 läuft auf Smartphones</a> (german)
  * Heise: <a href="http://www.heise.de/newsticker/meldung/Plasma-Mobile-bringt-KDE-5-aufs-Smartphone-2763073.html" target="_blank">Plasma Mobile bringt KDE 5 auf Smartphones</a> (german)

What&#8217;s important to note is that the project is still in a very early stage of development, and its target is to be really usable around mid of 2016. As such, there are most certainly stability issues and lots of features missing.

But this also opens opportunities: If you are into mobile platforms, this is the right time to get in contact and contribute! The plasma developers are really nice people and welcome every single contribution, be it in terms of brainstorming ideas (e.g. graphical mockups), code, or organizing developer events. So get in touch now through the <a href="http://forums.plasma-mobile.org/forumdisplay.php?fid=38" target="_blank">Plasma Mobile forums</a> and through the <a href="https://mail.kde.org/mailman/listinfo/plasma-devel" target="_blank">Plasma Contributor Mailing List</a>! Just say hi and ask how to get involved :-)

 [1]: /wp-content/uploads/2015/07/phone-still-small.jpg