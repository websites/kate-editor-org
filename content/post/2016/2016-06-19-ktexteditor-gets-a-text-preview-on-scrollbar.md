---
title: KTextEditor gets Scrollbar and Code Folding Preview (Updated)
author: Dominik Haumann

date: 2016-06-19T16:50:54+00:00
url: /2016/06/19/ktexteditor-gets-a-text-preview-on-scrollbar/
categories:
  - Events
  - Users
tags:
  - planet
  - sprint

---
The KTextEditor Framework just got a new features for the next KDE Frameworks 5.24 release: A Text Preview on the vertical scrollbar.

If enabled in the options, the KTextEditor now shows a preview of the text when the mouse hovers over the scrollbar. The feature is available both when the minimap is used and also when the standard scrollbar is used. The text preview is only visible when vertical scrolling is possible.

This feature is enabled by default, and is also automatically available in Kile and KDevelop, so we hope this is useful to you!

For developers, to ensure this feature is off, the ConfigInterface of the View can be used through the config key &#8220;scrollbar-preview&#8221; of type bool.

[<img class="aligncenter size-full wp-image-3925" src="/wp-content/uploads/2016/06/kate-text-preview.gif" alt="Kate Text Preview" width="695" height="440" />][1]And in addition to the preview in the scrollbar, it is now also possible to have a preview for folded code: Hovering over folded code pops up a preview of the hidden lines. When clicked, the code folding opens.

[<img class="aligncenter wp-image-3924 size-full" src="/wp-content/uploads/2016/06/kate-fold-preview.gif" alt="Kate Code Folding Preview" width="1300" height="1020" />][2]

These features got added during the KDE Randa sprint, where around 40 KDE developers focus on improving KDE software and bringing KDE software to other platforms such as [Windows and Mac OS][3]. You can [support us by donating][4] through the following banner: :)

<p class="editable">
  <a href="https://www.kde.org/fundraisers/randameetings2016/"><img class="aligncenter" src="https://www.kde.org/fundraisers/randameetings2016/images/banner-fundraising2016.png" width="1400" height="200" /></a>
</p>

 [1]: /wp-content/uploads/2016/06/kate-text-preview.gif
 [2]: /wp-content/uploads/2016/06/kate-fold-preview.gif
 [3]: http://kfunk.org/2016/06/18/kde-on-windows-update/
 [4]: https://www.kde.org/fundraisers/randameetings2016/