---
title: Kate’s TabBar gets Movable Tabs
author: Dominik Haumann

date: 2016-06-15T11:00:46+00:00
url: /2016/06/15/kates-tabbar-gets-movable-tabs/
categories:
  - Events
  - Users
tags:
  - planet
  - sprint

---
With the next Applications 16.08 release, Kate5&#8217;s tabs will be movable with the mouse. This was a [feature request][1] for quite some time, which is [now fixed][2].<img class="aligncenter size-full wp-image-3851" src="/wp-content/uploads/2016/06/moving-tabs.gif" alt="Kate5's Moving Tabs" width="680" height="385" />For the record, the animated gif was created with the following two commands:

  1. `ffmpeg -f alsa -f x11grab -r 30 -s 680x385 -i :0.0+0,0 -threads 0 output%04d.png`
  2. `convert -delay 1/30 -loop 0 -layers OptimizeFrame -size 680x385 -fuzz 2% output*.png output.gif`

Much more work is going on at the Randa spring currently held in Switzerland. Would be nice if you support us ;)  
[<img class="aligncenter" src="https://www.kde.org/fundraisers/randameetings2016/images/banner-fundraising2016.png" width="1400" height="200" />][3]

 [1]: https://bugs.kde.org/show_bug.cgi?id=344074
 [2]: http://commits.kde.org/kate/be7b90ee88d0f45d59b2fc10b4517709f4ec02a7
 [3]: https://www.kde.org/fundraisers/randameetings2016/