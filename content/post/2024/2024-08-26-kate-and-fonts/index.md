---
title: Kate & Fonts
author: Christoph Cullmann
date: 2024-08-26T20:45:00+02:00
url: /post/2024/2024-08-26-kate-and-fonts/
---

With the Qt 6.7 release, Qt introduced a wide range of improvement for the [text rendering and font shaping](https://www.qt.io/blog/text-improvements-in-qt-6.7).

One element of this is that you can now configure OpenType font features.

Many of the 'new cool' programming fonts have such features integrated.
That includes both free fonts like [Cascadia Code](https://github.com/microsoft/cascadia-code) or paid fonts like [MonoLisa](https://www.monolisa.dev).

Let's use the features of Cascadia Code as an example, that is the stuff they promote on their GitHub page:

![Cascadia Code OpenType font features](/post/2024/2024-08-26-kate-and-fonts/images/cascadia-code-stylistic-set.png)

For example if you set the *ss01* feature, you get some alternative italics.
The same holds for MonoLisa, there that is the feature *ss02*.
Already that shows: these feature are often not very usefully named and very font specific.

Thanks to Waqar and me, with the upcoming KF 6.6 release, one will be able to configure that in Kate and other KTextEditor based applications.

The generic KDE Frameworks [font chooser](https://invent.kde.org/frameworks/kwidgetsaddons/-/merge_requests/260) allows now to configure that stuff and KTextEditor will keep these [settings](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/720) around.

See here enabled alternative italics in Kate with the enhanced font chooser still open (look at the SPDX markers in the code):

![Kate font features demo](/post/2024/2024-08-26-kate-and-fonts/images/kate-font-features.png)

A remaining issue is how to best handle the configuration saving in a more generic way.
Ideas how to add that to KConfig without breaking compatibility of the configuration files we write with older applications would be welcome.
For KTextEditor we just add some extra key for just the features that will be ignored by old versions.
