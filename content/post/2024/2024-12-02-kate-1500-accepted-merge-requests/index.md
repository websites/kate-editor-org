---
title: Kate - 1500 accepted merge requests!
author: Christoph Cullmann
date: 2024-12-02T21:25:00+02:00
url: /post/2024/2024-12-02-kate-1500-accepted-merge-requests/
---

I just looked at our GitLab page today and thought: Amazing!

![Kate - 1500 accepted merge requests](/post/2024/2024-12-02-kate-1500-accepted-merge-requests/images/kate-1500-accepted-merge-requests.png)

I thank you all for the great contributions of the last years.

Let's hope we see even more contributions in the future.

If you are unsure how to contribute, just take a look at the existing [merged stuff](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged) as reference.

The upcoming 24.12 release will be a good one, we did polish Kate a lot.

I know not all is well on the world, but I still hope you have a good end of the year and an even better start in the new one!
