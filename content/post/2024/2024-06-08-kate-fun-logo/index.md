---
title: Kate Fun Logo
author: Christoph Cullmann
date: 2024-06-08T21:11:00+02:00
url: /post/2024/2024-06-08-kate-fun-logo/
---

[G2](https://github.com/G2-Games/fun-logos) posted some fun logos for Kate on reddit.

I think they are nice and flashy and well suited if you want to show your appreciation for Kate and like that art style and a good addition to our awesome [icon](/post/2020/2020-01-30-new-breeze-kate-icon/) and [mascot](/mascot/).

## Static Version

![Static Fun Logo for Kate](/post/2024/2024-06-08-kate-fun-logo/images/kate.svg)

## Animated Version

![Animated Fun Logo for Kate](/post/2024/2024-06-08-kate-fun-logo/images/kate-animated.svg)

## Licensing

G2 licensed these files under the [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en).
Feel free to share the stuff with this license and credit for G2.

## Comments?

A matching thread for this can be found here on [r/KDE](https://www.reddit.com/r/kde/comments/1dbabie/kate_fun_logo_thanks_to_g2/).
