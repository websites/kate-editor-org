---
title: 'Kate Love: HighlightInterface, Autobrace'
author: Milian Wolff

date: 2009-11-22T18:15:34+00:00
excerpt: |
  Well, I have to admit: I didn&#8217;t spent much time developing the PHP plugin for KDevelop these past weeks. Instead I hacked on Kate:
  
  HighlightInterface
  
  I added another Kate interface, this time to access some of the highlighting information:
  
  
  wh...
url: /2009/11/22/kate-love-highlightinterface-autobrace-2/
syndication_source:
  - 'Milian Wolff - kate'
syndication_source_uri:
  - http://milianw.de/taxonomy/term/170/0
syndication_source_id:
  - http://milianw.de/tag/kate/feed
"rss:comments":
  - 'http://milianw.de/blog/kate-love-highlightinterface-autobrace#comments'
"wfw:commentRSS":
  - http://milianw.de/crss/node/109
syndication_feed:
  - http://milianw.de/tag/kate/feed
syndication_feed_id:
  - "8"
syndication_permalink:
  - http://milianw.de/blog/kate-love-highlightinterface-autobrace
syndication_item_hash:
  - d2948051ffbe26ab72804863ba2faeae
categories:
  - KDE

---
Well, I have to admit: I didn&#8217;t spent much time developing the PHP plugin for KDevelop these past weeks. Instead I hacked on Kate:

##### HighlightInterface

I added another Kate interface, this time to access some of the highlighting information:

  * what&#8217;s the Attribute for a given default style right now? Default styles are those known from syntax files, e.g. <span class="geshifilter"><code class="text geshifilter-text">dsKeyword</code></span>, <span class="geshifilter"><code class="text geshifilter-text">dsFunction</code></span>,&#8230;
  * what are used Attributes in a given line and what range do they occupy?
  * what modes do we embed? E.g. PHP embeds HTML, JavaScript, CSS, &#8230;
  * what mode is used at a given Cursor position?

This made it possible to port the &#8220;Export to HTML&#8221; action to a real plugin. If you come up with other output formats I might add them, I wondered about LaTeX support&#8230; might do this at some point.

This should also make it possible to use KatePart in other applications and than export the highlighting to a different format, e.g. a Flake shape for Koffice. Afaik this is actually planned by The_User - lets see if it works out!

The other stuff gives huge potential in various places, but I fear it won&#8217;t make it in KDE 4.4. But think of it:

  * simple code completion based on keyword databases, dependent on the mode at the position where completion was requested
  * same as above for snippets (actually this will make it to 4.4).
  * insert your ideas here :)

##### Auto-Brace plugin

Jakob Petsovits created this gem of a plugin some time ago, yet it lived in playground was probably only used by few. I imported it to kdelibs, hence it will be shipped with KDE 4.4. It supersedes the limited &#8220;auto-brackets&#8221; feature of Kate and only adds braces when a newline gets added. I find this fits my personal coding habits much better than blindly copying brackets when they get added.

And I don&#8217;t just copied to kdelibs, I also added a few features:

  * automatically add a semicolon after the closing brace when we start a new struct/class in C++ mode
  * check for auto-brackets feature and disable it automatically

Also did this:

  * don&#8217;t add brace when current line contains <span class="geshifilter"><code class="text geshifilter-text">namespace</code></span> and a following line starts with <span class="geshifilter"><code class="text geshifilter-text">class</code></span> or <span class="geshifilter"><code class="text geshifilter-text">struct</code></span> (C++ mode only)
  * don&#8217;t add brace when current line contains <span class="geshifilter"><code class="text geshifilter-text">class</code></span>, <span class="geshifilter"><code class="text geshifilter-text">interface</code></span> or <span class="geshifilter"><code class="text geshifilter-text">struct</code></span> and the following line contains <span class="geshifilter"><code class="text geshifilter-text">private</code></span>, <span class="geshifilter"><code class="text geshifilter-text">public</code></span>, <span class="geshifilter"><code class="text geshifilter-text">protected</code></span>. C++ code is also checked for <span class="geshifilter"><code class="text geshifilter-text">signals</code></span>, <span class="geshifilter"><code class="text geshifilter-text">Q_SIGNALS", other modes are checked for</code></span>function\`.

This should fix the bug for code like (note the indendation levels):

<div class="geshifilter">
  <pre class="cpp geshifilter-cpp" style="font-family:monospace;"><ol>
  <li class="li1">
    <div class="de1">
      <span class="kw2">namespace</span> foo <span class="br0">&#123;</span> <span class="co1">// insert line here</span>
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      <span class="kw2">class</span> bar;
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      <span class="br0">&#125;</span>
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      <span class="kw2">class</span> asdf <span class="br0">&#123;</span> <span class="co1">// insert line here</span>
    </div>
  </li>
  
  <li class="li2">
    <div class="de2">
      <span class="kw2">private</span><span class="sy4">:</span>
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      ...
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      <span class="br0">&#125;</span>;
    </div>
  </li>
</ol></pre>
</div>