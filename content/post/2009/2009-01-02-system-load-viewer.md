---
title: System Load Viewer
author: Dominik Haumann

date: 2009-01-02T17:28:00+00:00
url: /2009/01/02/system-load-viewer/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2009/01/system-load-viewer.html
categories:
  - KDE

---
Last year I&#8217;ve blogged about the [missing system monitor][1] with the three bars for the panel and about its port to Plasma. Meanwhile other developers [also did a port][2] called System Status. In a collaboration with them we finally have the applet back in [KDE&#8217;s subversion][3], the name is now &#8220;System Load Viewer&#8221; and it uses the data engine &#8220;systemmonitor&#8221; that already exists in KDE 4.2.  
So if you want to have the plasmoid for your KDE4.2 desktop, it should be straightforward to compile/install.  
On the screenshot you can see the plasmoid in action. There are two instances, one on the panel and one on the desktop. The one on the left is the KDE3 one.  
<a onblur="try {parent.deselectBloggerImageGracefully();} catch(e) {}" href="http://3.bp.blogspot.com/_JcjnuQSFzjw/SV5C46qPUqI/AAAAAAAAACQ/BHHOPImx9kc/s1600-h/systemloadviewer.png"><img style="cursor: pointer; width: 400px; height: 251px;" src="http://3.bp.blogspot.com/_JcjnuQSFzjw/SV5C46qPUqI/AAAAAAAAACQ/BHHOPImx9kc/s400/systemloadviewer.png" alt="" id="BLOGGER_PHOTO_ID_5286736558166069922" border="0" /></a>  
It&#8217;s worth to mention that the plasmoid already supports more featues than the KDE3 version. Features include:

  * show all cpus (for computers with multicores)
  * tooltip updates continuously
  * nicer visualization (maybe needs some more tweaks)

As soon as the KDE 4.2 freeze is over we&#8217;ll have to see where we can put the plasmoid to make sure it&#8217;s available for KDE 4.3 :)

 [1]: http://dhaumann.blogspot.com/2008/05/i-miss-applet-which-shows-system-usage.html
 [2]: http://www.kde-look.org/content/show.php/System+Status?content=74891
 [3]: http://websvn.kde.org/trunk/kdereview/plasma/applets/systemloadviewer