---
title: GSoC 2011 – Kate Code Folding
author: Adrian Lungu

date: 2011-06-11T17:10:30+00:00
url: /2011/06/11/gsoc-2011-kate-code-folding/
categories:
  - Developers
  - KDE
  - Users

---
Hi everybody!

I am Adrian and, as Erlend already mentioned in a previous post, I will rewrite Kate’s code folding algorithm on this GSoC edition.

Starting from this week, I will post a weekly report of my work. At the end of each week I will post some info about the project’s progress and some details about what I have scheduled for the next week (according to my presented timeline).

I know that GSoC coding started for about 3 weeks, but, as you will see in my timeline, I had school exams until yesterday so I couldn’t work during this time. This won’t be a problem. I knew about that from the beginning and I scheduled my timeline starting with June 13<sup>th</sup>. Also, this was mentioned in my proposal, so all the mentors are aware of it.

My objective for the next week (**13 June – 19 June**) is to get familiar with Kate project and QT. I already downloaded the source files, compiled them and took a look through them, so I will do that again (more carefully) and build a list of questions which will be sent to my mentor or posted on the mailing list. This way I will be able to focus later on **what** I will write and not on **how** I will do it.

One more thing: Because this is my first post, I will also post my full timeline so that everyone knows what my next move will be. Here it is (pasted from my proposal):

**Tentative Timeline**

May 23 – June 12 – I won’t be able to work during this period because I will have some school exams during this time.

Weeks 1-2 (13 June – 26 June) – Plan on how to fit all the “puzzle pieces” (algorithms, data structure and other objects used); implementing the abstract class that will ensure backwards compatibility; take an overall picture of Kate code folding.

Weeks 3-4 (27 June – 10 July) – I will implement and test my tree-algorithms, which is the base of the code folding. The algorithms will be implemented in C++ using QT objects but as a separate project in order to test them as much (and easily) as possible.

Weeks 5-6 (11 July – 24 July) – Implementing the two classes that extend the abstract class using all the information gained in previous weeks.

Weeks 7-8 (25 June – 7 August) – Implementing a new feature for Kate code folding – “Code folding preview” (presented above)

Weeks 9-10 (8 August – 22 August) – Lots of testing and some documentation (I saw that Kate source files have a little lack of code documentation, but I think some explanations about the implementation would be great).

I am open to proposals about this project, so if you have any ideas that can make it better let me know and we will talk about them.

Wish me good luck! :)