---
title: On Being Wrong
author: Dominik Haumann

date: 2011-12-21T22:27:48+00:00
url: /2011/12/21/on-being-wrong/
pw_single_layout:
  - "1"
categories:
  - KDE

---
Maybe a good read: <a title="On being wrong" href="http://www.computer.org/portal/web/buildyourcareer/Nosce-te-Ipsum/-/blogs/on-being-wrong" target="_blank">On being wrong</a>. It&#8217;s about why it&#8217;s so hard to admit that you are wrong, and more.