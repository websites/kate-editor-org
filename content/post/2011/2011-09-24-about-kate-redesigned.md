---
title: About Kate Redesigned
author: Dominik Haumann

date: 2011-09-24T09:32:19+00:00
url: /2011/09/24/about-kate-redesigned/
pw_single_layout:
  - "1"
categories:
  - Users
tags:
  - planet

---
With some of my gimp skills, I updated Kate&#8217;s feature list. [Hope you like the face-lift :-)][1]

 [1]: /about-kate/ "About Kate / Kwrite"