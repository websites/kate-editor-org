---
title: Kate Highlighting Power
author: Dominik Haumann

date: 2007-09-12T19:23:00+00:00
url: /2007/09/12/kate-highlighting-power/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2007/09/kates-highlighting-capabilities-are.html
categories:
  - Users

---
Kate&#8217;s highlighting capabilities are amazing. If you want you can highlight really complex syntax, without having to hardcode rules in C++. As an example, we&#8217;ll take a look at how Lua comments can be realized:

  * <span style="font-family: courier new;">&#8211;[=[</span> starts a multiline comment (the &#8216;=&#8217; chars are optional)
  * <span style="font-family: courier new;">]=]</span> ends the multiline comment
  * the number of &#8216;=&#8217; chars in <span style="font-family: courier new;">]=]</span> must match the number of <span style="font-family: courier new;">&#8211;[=[</span>

That means: When the highlighting processor matches the end of a multiline comment, it has to know how many &#8216;=&#8217; chars started the comment. Thanks to the concept of <span style="font-style: italic;">dynamic rules and contexts</span> Kate is able to do that. The highlighting file looks like this. First comes the header

<pre>&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;!DOCTYPE language SYSTEM "language.dtd" &gt;
&lt;language name="Test" version="1.0" kateversion="2.4" section="Markup" extensions="" mimetype=""&gt;
  &lt;highlighting&gt;</pre>

Then the body with the contexts. We start in the first context called <span style="font-style: italic;">&#8220;Normal Text&#8221;</span>. When the regular expression <span style="font-family: courier new;">&#8211;[(=*)[</span> matches it switches to the context Comment.

<pre>&lt;contexts&gt;
      &lt;context attribute="Normal Text" lineEndContext="#stay" name="Normal"&gt;
        &lt;RegExpr attribute="Comment" context="Comment" String="--\[(=*)\[" dynamic="true"/&gt;
      &lt;/context&gt;</pre>

The part <span style="font-family: courier new;">(=*)</span> is now available as %1 in the rule below:

<pre>&lt;context name="Comment" attribute="Comment" lineEndContext="#stay" dynamic="true" &gt;
        &lt;RegExpr attribute="Comment" context="#pop" String="\]%1\]" dynamic="true" /&gt;
      &lt;/context&gt;
    &lt;/contexts&gt;</pre>

The last part is the footer:

<pre>&lt;itemDatas&gt;
      &lt;itemData name="Normal Text" defStyleNum="dsNormal" /&gt;
      &lt;itemData name="Comment"     defStyleNum="dsComment" /&gt;
    &lt;/itemDatas&gt;
  &lt;/highlighting&gt;
&lt;/language&gt;</pre>

If you want to know more about Kate&#8217;s highlighting, [have a look at the documentation][1] :) There are also [lots of bug reports][2], so if you want to contribute you can fix them!

_PS: As I don&#8217;t know much about Lua, comments might work differently. That does not really matter, as the example still shows what you can do :)_

 [1]: /2005/03/24/writing-a-syntax-highlighting-file/ "Writing a Kate Syntax Highlighting File"
 [2]: http://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&short_desc=&long_desc_type=allwordssubstr&long_desc=&product=kate&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=VERIFIED&bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bugidtype=include&bug_id=&votes=&emailtype1=substring&email1=&emailtype2=substring&email2=&changedin=&chfieldfrom=&chfieldto=Now&chfieldvalue=&newqueryname=&order=bugs.bug_id