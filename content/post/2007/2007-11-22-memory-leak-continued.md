---
title: Memory Leak Continued
author: Dominik Haumann

date: 2007-11-22T11:55:00+00:00
url: /2007/11/22/memory-leak-continued/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2007/11/memory-leak-continued.html
categories:
  - Developers

---
There was some confusion with regard to [my last blog][1] about leaking memory. Suppose the ui_mywidget.h files looks like this:

<pre>class Ui_Widget
{
public:
   QGridLayout *gridLayout;
   QGroupBox *groupBox;
   QGridLayout *gridLayout1;
   QListWidget *listWidget;
   QSpacerItem *spacerItem;
   QPushButton *pushButton;

   void setupUi(QWidget *Widget);
   void retranslateUi(QWidget *Widget);
};</pre>

Of course, those 6 QObject derived classes are deleted. But the sizeof(Ui\_Widget) = 6 \* sizeof(void\*) = 24 bytes are not deleted. As Ui\_Widget is not QObject derived those 24 bytes leak. Confirmed by [valgrind][2].

In a comment to my last blog Paolo suggested to use auto_ptr or [scoped_ptr][3], which is more elegant than an extra wrapper class :)

 [1]: http://dhaumann.blogspot.com/2007/11/memory-leak-ui-files-and-direct.html
 [2]: http://techbase.kde.org/Development/Tools/Valgrind
 [3]: http://www.boost.org/libs/smart_ptr/scoped_ptr.htm