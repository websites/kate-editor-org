---
title: Bug Hunting – Please help poor Kate ;)
author: Christoph Cullmann

date: 2012-11-09T21:03:16+00:00
url: /2012/11/09/bug-hunting-please-help-poor-kate/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
tags:
  - planet

---
The Kate/KDevelop sprint in Vienna was a really productive time.

Thanks to <a title="Joseph Wenninger" href="http://www.jowenn.net" target="_blank">Joseph Wenninger</a> for organizing it and the <a title="KDE e.V." href="http://ev.kde.org" target="_blank">KDE e.V.</a> for the additional funding!

The Kate team was able to really wade through a lot of bugs & wishes and fix/invalidate a lot of them.

After some more days of work, this really is visible in our nice bug chart ;)

[<img class="aligncenter size-full wp-image-2165" title="Kate Bug Chart, 2012-11-09" src="/wp-content/uploads/2012/11/kate_bug_chart.png" alt="" srcset="/wp-content/uploads/2012/11/kate_bug_chart.png 793w, /wp-content/uploads/2012/11/kate_bug_chart-300x221.png 300w" sizes="(max-width: 793px) 100vw, 793px" />][1]

Kate, KatePart & KWrite are now down to a bug level as low as years ago ;)

Still, there is a lot to do.

If you have some spare time and Qt/KDE knowledge, please help us to have an even better KDE 4.10 release and look at the remaining bugs.

We have the following <a title="Bugs" href="https://bugs.kde.org/buglist.cgi?product=kate&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&list_id=281469" target="_blank">~60 bugs</a> and <a title="Wishes" href="https://bugs.kde.org/buglist.cgi?product=kate&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=wishlist&list_id=281470" target="_blank">~330 wishes</a> still around. They vary from small stuff to &#8220;a lot&#8221; of work. If you want to help out, just pick the bug you want to work on and assign it ;)

 [1]: /wp-content/uploads/2012/11/kate_bug_chart.png