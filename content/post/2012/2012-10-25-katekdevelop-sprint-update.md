---
title: Kate/KDevelop Sprint – Update
author: Christoph Cullmann

date: 2012-10-25T10:57:46+00:00
url: /2012/10/25/katekdevelop-sprint-update/
pw_single_layout:
  - "1"
categories:
  - Events
  - KDE
tags:
  - planet

---
Its nice to be here in Vienna with all the other hackers ;)  
Most have already arrived and are busy fixing issues in Kate & KDevelop, some more will arrive tomorrow.

Already got bit work done on existing bugs, one bug was really nasty: a nearly 8 year old typo by myself messing around with highlighting in combination with line continuation.

But a guy did a really perfect bug report: <a title="Bug 300009" href="https://bugs.kde.org/show_bug.cgi?id=300009" target="_blank">Bug 300009</a>  
He wrote even a example highlighting file + test to trigger the issue, with that help, the fix was trivial to do, after I got time to read the bug ;)

Thanks to all people reporting bugs in such a useful way!