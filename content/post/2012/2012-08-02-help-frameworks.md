---
title: Help Frameworks ;)
author: Christoph Cullmann

date: 2012-08-02T20:01:40+00:00
url: /2012/08/02/help-frameworks/
pw_single_layout:
  - "1"
categories:
  - Developers
  - KDE
tags:
  - planet

---
It is easy and fun, please help to fix the remaining small things in the cleanups section of the frameworks branch.

See here:  
<http://community.kde.org/Frameworks/Epics/kdelibs_cleanups>

And use the nice and complete build howto here:  
<http://community.kde.org/Frameworks/Building>

And yeah, I have already stolen some really easy tasks :)  
Don&#8217;t allow David to have all the fun.