---
title: Crash through D-Bus calls
author: Dominik Haumann

date: 2012-04-06T10:36:51+00:00
url: /2012/04/06/crash-through-d-bus-calls/
pw_single_layout:
  - "1"
categories:
  - Developers
tags:
  - planet

---
Years ago, there was a blog on the planet with the title &#8220;<a title="How to crash (almost) every Qt/KDE Application and how to fix it" href="http://blogs.kde.org/node/3919" target="_blank">How to crash (almost) every Qt/KDE Application and how to fix it</a>&#8220;. In short, if you are showing a dialog, KWin prevents you from closing the application by clicking on the close button in the window decoration. However, through D-Bus, you can still quit the application. A solution was also provided: Use a guarded pointer to create the dialog.While this fixes the issue, it looks like fixing the blame, and not the real issue. Stricktly speaking, even the <a title="Code Examples" href="http://qt-project.org/doc/qt-4.8/QDialog.html#code-examples" target="_blank">Qt documentation</a> would be wrong then.

<a title="lxr.kde.org" href="http://lxr.kde.org/ident?i=Accepted" target="_blank">Searching for &#8216;Accepted&#8217; on lxr.kde.org</a> shows lots of dialogs that lead to possible crashes. I wonder whether developers are really aware of this crash? Even if we took care of this issue as proposed, it&#8217;s just a matter of time until dialogs are created the \`wrong&#8217; way again (do we have krazy checks for that?). In Kate, no one took care of this situation, meaning that you can indeed crash the application through D-Bus.

Is there a better way to fix this?