---
title: Merge Requests - July 2021
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2021/07/
---

#### Week 30

- **Kate - [move more work for projects to the worker threads](https://invent.kde.org/utilities/kate/-/merge_requests/469)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [Python: Fix r&quot;\\&quot; and derivatives](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/231)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **Kate - [Fix missing include](https://invent.kde.org/utilities/kate/-/merge_requests/467)**<br />
Request authored by [Robert Hoffmann](https://invent.kde.org/hoffmannrobert) and merged at creation day.

- **Kate - [Fix remaining toolview sidebar issues](https://invent.kde.org/utilities/kate/-/merge_requests/468)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **Kate - [Netshare performance optimizations](https://invent.kde.org/utilities/kate/-/merge_requests/368)**<br />
Request authored by [Robert Hoffmann](https://invent.kde.org/hoffmannrobert) and merged after 111 days.

- **Kate - [add helper to create consistent git process](https://invent.kde.org/utilities/kate/-/merge_requests/466)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Add a test for location history stuff](https://invent.kde.org/utilities/kate/-/merge_requests/463)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [lspclient support for server progress notification](https://invent.kde.org/utilities/kate/-/merge_requests/460)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 9 days.

- **Kate - [Replace the LSP server for Python with a maintained one](https://invent.kde.org/utilities/kate/-/merge_requests/461)**<br />
Request authored by [Boris Petrov](https://invent.kde.org/iorvethe) and merged after 7 days.

- **KTextEditor - [Remove module prefix of include](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/179)**<br />
Request authored by [Robert Hoffmann](https://invent.kde.org/hoffmannrobert) and merged at creation day.

- **KSyntaxHighlighting - [Python: Make unescaped single quote (&#x27; or &quot;) strings non-multiline](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/230)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

#### Week 29

- **Kate - [semantic highlighter: Use a timer to reduce amount of requests being sent](https://invent.kde.org/utilities/kate/-/merge_requests/464)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Netshare performance optimizations](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/133)**<br />
Request authored by [Robert Hoffmann](https://invent.kde.org/hoffmannrobert) and merged after 108 days.

- **KSyntaxHighlighting - [Python: Fix single quote bytes literals (b&quot;&quot;)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/227)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 4 days.

- **KSyntaxHighlighting - [cmake.xml: Update variables for `find_package()` interface](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/229)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **Kate - [ensure new loaded project is active](https://invent.kde.org/utilities/kate/-/merge_requests/459)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

#### Week 28

- **KSyntaxHighlighting - [cmake.xml: Add CMake 3.21 features](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/226)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after 2 days.

- **Kate - [LocHistory: When limiting size, also rewind currentLocation](https://invent.kde.org/utilities/kate/-/merge_requests/458)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [[Splitter] Improve double click detection](https://invent.kde.org/utilities/kate/-/merge_requests/457)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged at creation day.

- **KTextEditor - [KateView: speed up large view jumps](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/178)**<br />
Request authored by [Michal Humpula](https://invent.kde.org/michalhumpula) and merged after one day.

- **Kate - [LSP Rename: add word under cursor to input line](https://invent.kde.org/utilities/kate/-/merge_requests/456)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [XSLT: Enhance highlighter colors (like XML) + region for tag](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/223)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

- **KSyntaxHighlighting - [Ini: value as int, float or keyword only if it represents the full value](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/224)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

- **Kate - [sessions: fix testcase for anon session](https://invent.kde.org/utilities/kate/-/merge_requests/455)**<br />
Request authored by [Michal Humpula](https://invent.kde.org/michalhumpula) and merged at creation day.

- **Kate - [Make collapsed sidebars resizable by mouse again](https://invent.kde.org/utilities/kate/-/merge_requests/453)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

- **KSyntaxHighlighting - [Agda: update keyword list to Agda 2.6.2](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/225)**<br />
Request authored by [Andreas Abel](https://invent.kde.org/abel) and merged at creation day.

#### Week 27

- **Kate - [avoid that tool view split view collapse](https://invent.kde.org/utilities/kate/-/merge_requests/451)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 4 days.

- **Kate - [Location history improvements](https://invent.kde.org/utilities/kate/-/merge_requests/452)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [normalize splitter to 50%:50% on double click](https://invent.kde.org/utilities/kate/-/merge_requests/450)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 3 days.

- **KTextEditor - [move close button to right for bottom widgets](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/177)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 3 days.

- **KSyntaxHighlighting - [XML: fix &lt;!DOCTYPE detection and reordered rules for faster reading (+20%)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/222)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [Python: }} in a f-string as an escape character and add !a flag](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/219)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [systemd unit: fix highlighting of unit name, update to systemd v249](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/221)**<br />
Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr) and merged at creation day.

- **KSyntaxHighlighting - [Mark katehighlightingindexer as a non-GUI executable](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/220)**<br />
Request authored by [Alex Richardson](https://invent.kde.org/arichardson) and merged at creation day.

- **Kate - [Use Ctrl+Shift+F for search and F11 for fullscreen](https://invent.kde.org/utilities/kate/-/merge_requests/449)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Disable using QtXMLPatterns with sanitizer build](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/218)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [add gitlab-ci for testing merge requests](https://invent.kde.org/utilities/kate/-/merge_requests/447)**<br />
Request authored by [Michal Humpula](https://invent.kde.org/michalhumpula) and merged after one day.

- **Kate - [LSP maintenance](https://invent.kde.org/utilities/kate/-/merge_requests/448)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

#### Week 26

- **KTextEditor - [Port away from deprecated KFilterDev](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/176)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after one day.

- **KSyntaxHighlighting - [Indexer: suggest LineContinue instead of RegExpr](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/217)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [move coroutine keywords in controlflow and replace some keyword rule with WordDetect](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/216)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 5 days.

- **Kate - [avoid to pollute terminal with failure output](https://invent.kde.org/utilities/kate/-/merge_requests/446)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Improve appearance of sessions plasmoid](https://invent.kde.org/utilities/kate/-/merge_requests/441)**<br />
Request authored by [Mufeed Ali](https://invent.kde.org/mufeedali) and merged after 11 days.

- **KSyntaxHighlighting - [Indexer: suggest more RangeDetect instead of RegExpr](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/215)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **KSyntaxHighlighting - [Use cstyle indenter for PHP (HTML)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/214)**<br />
Request authored by [Heinz Wiesinger](https://invent.kde.org/wiesinger) and merged after 3 days.

