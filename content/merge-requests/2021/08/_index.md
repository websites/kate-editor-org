---
title: Merge Requests - August 2021
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2021/08/
---

#### Week 35

- **Kate - [Search Plugin: Implement a simple Match Export Dialog](https://invent.kde.org/utilities/kate/-/merge_requests/477)**<br />
Request authored by [Markus Ebner](https://invent.kde.org/mebner) and merged after 8 days.

- **KSyntaxHighlighting - [Python: Add &quot;yield from&quot; keyword](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/249)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KSyntaxHighlighting - [optimize KeywordList::contains()](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/250)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

#### Week 34

- **Kate - [Make working directory gadget pick a directory.](https://invent.kde.org/utilities/kate/-/merge_requests/479)**<br />
Request authored by [Christopher Yeleighton](https://invent.kde.org/cyeleighton) and merged at creation day.

- **Kate - [KWrite: use KStandardAction::preferences](https://invent.kde.org/utilities/kate/-/merge_requests/478)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 4 days.

- **KTextEditor - [Python indentation: decrease indent when appropriate keyword is typed](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/189)**<br />
Request authored by [Ilia Kats](https://invent.kde.org/iliakats) and merged after one day.

- **KTextEditor - [indentation: add Julia indentation script](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/190)**<br />
Request authored by [Ilia Kats](https://invent.kde.org/iliakats) and merged at creation day.

- **KSyntaxHighlighting - [Julia highlighting fixes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/248)**<br />
Request authored by [Ilia Kats](https://invent.kde.org/iliakats) and merged at creation day.

- **KSyntaxHighlighting - [Simplify helpers of Repository::definition[s]For*()](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/247)**<br />
Request authored by [Igor Kushnir](https://invent.kde.org/igorkushnir) and merged at creation day.

- **KSyntaxHighlighting - [Repository: unify definitionsFor*() and optimize definitionFor*()](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/246)**<br />
Request authored by [Igor Kushnir](https://invent.kde.org/igorkushnir) and merged after one day.

- **KTextEditor - [Extract, test, fix, refactor KateModeManager::mimeTypesFind()](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/187)**<br />
Request authored by [Igor Kushnir](https://invent.kde.org/igorkushnir) and merged after one day.

- **Kate - [Show hint about invalid regex](https://invent.kde.org/utilities/kate/-/merge_requests/476)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged after 2 days.

- **KTextEditor - [replace &quot;MacOSX&quot; with &quot;macOS&quot;](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/188)**<br />
Request authored by [Frederik Schwarzer](https://invent.kde.org/schwarzer) and merged at creation day.

- **Kate - [Escape shortcut mnemonic markers (&amp;) in tab texts](https://invent.kde.org/utilities/kate/-/merge_requests/475)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

- **KTextEditor - [Simplify presentation of word wrapping options](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/182)**<br />
Request authored by [Jan Blackquill](https://invent.kde.org/cblack) and merged after 5 days.

- **KTextEditor - [Test, benchmark and fix KateModeManager::wildcardsFind(); KateWildcardMatcher =&gt; KSyntaxHighlighting::WildcardMatcher](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/185)**<br />
Request authored by [Igor Kushnir](https://invent.kde.org/igorkushnir) and merged after one day.

- **KSyntaxHighlighting - [add extra resource path lookups for app files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/244)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [Export and optimize WildcardMatcher](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/245)**<br />
Request authored by [Igor Kushnir](https://invent.kde.org/igorkushnir) and merged after one day.

- **KSyntaxHighlighting - [Definition[Ref]: add move constructor and move assignment operator](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/237)**<br />
Request authored by [Igor Kushnir](https://invent.kde.org/igorkushnir) and merged after 6 days.

#### Week 33

- **KSyntaxHighlighting - [Repository::definitionsForFileName: don&#x27;t detach a temporary QVector](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/243)**<br />
Request authored by [Igor Kushnir](https://invent.kde.org/igorkushnir) and merged after one day.

- **KSyntaxHighlighting - [Add QtQuick bindings](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/236)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged after 5 days.

- **KTextEditor - [completion: invoke always](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/183)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [WildcardMatcher: treat unmatched filename prefixes as no match](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/239)**<br />
Request authored by [Igor Kushnir](https://invent.kde.org/igorkushnir) and merged after one day.

- **KSyntaxHighlighting - [Replace &quot;SConscript&quot; with &quot;.profile&quot; to fix a test failure](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/241)**<br />
Request authored by [Igor Kushnir](https://invent.kde.org/igorkushnir) and merged at creation day.

- **KSyntaxHighlighting - [Port QStringRef (deprected) to QStringView](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/240)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after one day.

- **KTextEditor - [Port QStringRef (deprecated) to QStringView](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/142)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 110 days.

#### Week 32

- **KTextEditor - [Port text area config widget to QFormLayout](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/181)**<br />
Request authored by [Jan Blackquill](https://invent.kde.org/cblack) and merged after 3 days.

- **KSyntaxHighlighting - [PL/I: Also recognise &quot;pl1&quot; extension and MIME type](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/234)**<br />
Request authored by [Jonathan Marten](https://invent.kde.org/marten) and merged after 4 days.

- **KSyntaxHighlighting - [isocpp.xml: Highlight raw string delimeters (+corresponding tweaks to default themes)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/228)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 23 days.

- **KTextEditor - [Do not show encoding error when file only contains BOM](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/180)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 10 days.

#### Week 31

- **KSyntaxHighlighting - [Update Kconfig highlighter to Linux 5.13](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/233)**<br />
Request authored by [Martin Walch](https://invent.kde.org/martinwalch) and merged after 7 days.

- **Kate - [Allow multiple tabs for Konsole plugin(s)](https://invent.kde.org/utilities/kate/-/merge_requests/470)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **Kate - [Lspclient tweaks](https://invent.kde.org/utilities/kate/-/merge_requests/471)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

#### Week 30

- **KSyntaxHighlighting - [toml.xml: single-quoted keys and a fir for integers](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/232)**<br />
Request authored by [Denis Lisov](https://invent.kde.org/tanriol) and merged at creation day.

