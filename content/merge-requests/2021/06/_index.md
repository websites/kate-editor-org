---
title: Merge Requests - June 2021
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2021/06/
---

#### Week 26

- **Kate - [plugins: Convert desktop files to JSON](https://invent.kde.org/utilities/kate/-/merge_requests/445)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **Kate - [S&amp;R: fix crash when closing search tabs](https://invent.kde.org/utilities/kate/-/merge_requests/443)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after one day.

- **KSyntaxHighlighting - [Haskell: fix comment in an import and some improvement](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/213)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

#### Week 25

- **Kate - [Update name of irc service used by #vim](https://invent.kde.org/utilities/kate/-/merge_requests/444)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KTextEditor - [Revert width changes to completionWidget](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/175)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KSyntaxHighlighting - [Indexer: checks that the rules of a context are all reachable at least once](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/212)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [all rules are now final classes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/210)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Indexer: (#12) remove check for unused keyword lists](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/211)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KTextEditor - [Fix possible mark leak in katedocument](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/174)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix mark handling in katedocument.cpp](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/173)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Python: add bytes literals](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/209)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **kate-editor.org - [Fixed Laptop markup](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/33)**<br />
Request authored by [Tobias Kündig](https://invent.kde.org/tktktk) and merged at creation day.

- **Kate - [LSP workspace folders](https://invent.kde.org/utilities/kate/-/merge_requests/436)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 10 days.

- **Kate - [lspclient: bypass shutdown delay if not needed and cleanup defunct code](https://invent.kde.org/utilities/kate/-/merge_requests/440)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 5 days.

- **KSyntaxHighlighting - [GLSL: based on GLSL 4.6](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/205)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **KSyntaxHighlighting - [Bash/Zsh: fix file descriptor and group in sub-shell, noglob and some Control Flow](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/208)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [Python: highlight errors in numbers and some optimizations](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/207)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [LSP Completion: Support textEdit](https://invent.kde.org/utilities/kate/-/merge_requests/438)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 5 days.

- **KTextEditor - [Improvements to the sizing / positioning of the completion widget](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/172)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Remove unneeded LGPL-2.0-only license text](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/206)**<br />
Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr) and merged at creation day.

- **kate-editor.org - [Blog about the recently added dart support](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/32)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 24

- **KSyntaxHighlighting - [GLSL: add types, qualifiers and reserved keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/204)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KTextEditor - [Some more improvements to minimap perf](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/171)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Improve minimap performance](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/170)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Falcon theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/202)**<br />
Request authored by [Alberto Salvia Novella](https://invent.kde.org/albertosalvianovella) and merged after 4 days.

- **KTextEditor - [Make completion more efficient](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/168)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Always emit delayedUpdateOfView to fix update issues](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/169)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Improve minimap performance](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/165)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **KTextEditor - [Always check for shouldStartCompletion](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/167)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [LSP maintenance](https://invent.kde.org/utilities/kate/-/merge_requests/439)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **KTextEditor - [Update mode config page UI when a filetype&#x27;s name is changed](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/166)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

- **KSyntaxHighlighting - [Update IRC network name in wml.xml comment](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/203)**<br />
Request authored by [Nicolás Alvarez](https://invent.kde.org/nalvarez) and merged at creation day.

- **Kate - [LSP var substitution and path search](https://invent.kde.org/utilities/kate/-/merge_requests/435)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 5 days.

- **KSyntaxHighlighting - [Julia highlighting fixes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/201)**<br />
Request authored by [Ilia Kats](https://invent.kde.org/iliakats) and merged after 2 days.

- **Kate - [implement workspace/symbols request](https://invent.kde.org/utilities/kate/-/merge_requests/437)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KTextEditor - [improve handling of repainting for ranges](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/163)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

#### Week 23

- **KTextEditor - [Set devicePixelRatio for drag pixmap](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/164)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged at creation day.

- **KTextEditor - [Show Custom Filetypes on config page](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/156)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 16 days.

- **KTextEditor - [Mark TextRange and TextCursor classes as final](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/162)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [katetextrange: Use non-virtual functions internally](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/161)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Dont create moving ranges when attribute is null](https://invent.kde.org/utilities/kate/-/merge_requests/434)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Simplify Ctrl-Click cursor unsetting](https://invent.kde.org/utilities/kate/-/merge_requests/432)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [katepluginmanager: Remove unneeded check for service types](https://invent.kde.org/utilities/kate/-/merge_requests/431)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **Kate - [Set ZDepth before setAttribute for MovingRanges](https://invent.kde.org/utilities/kate/-/merge_requests/433)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [fallback to directory of document for root](https://invent.kde.org/utilities/kate/-/merge_requests/427)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [LSP maintenance](https://invent.kde.org/utilities/kate/-/merge_requests/426)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **Kate - [LSP: Support textDocument/SemanticTokens/range request](https://invent.kde.org/utilities/kate/-/merge_requests/429)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Clean up katedocument](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/158)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **KTextEditor - [Always call MovingRange::setZDepth() before setAttribute()](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/160)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KTextEditor - [Un-overload signals](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/137)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 55 days.

#### Week 22

- **Kate - [Add Dart LSP](https://invent.kde.org/utilities/kate/-/merge_requests/428)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Clean up katefiletreemodel](https://invent.kde.org/utilities/kate/-/merge_requests/425)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **KTextEditor - [syntax: less manual memory mgmt and cleanup](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/157)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 8 days.

- **KTextEditor - [Take into account wordCompletionRemoveTail in completionRange() default implementation](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/148)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 29 days.

- **KSyntaxHighlighting - [Add Dart language](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/200)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix vim cursor gets stuck with &#x27;b&#x27;](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/159)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [SQL addon: use KPasswordLineEdit instead of KLineEdit::passwordMode](https://invent.kde.org/utilities/kate/-/merge_requests/423)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

