---
title: Merge Requests - November 2022
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2022/11/
---

#### Week 48

- **Kate - [Port away from deprecated KDirOperator::setViewMode](https://invent.kde.org/utilities/kate/-/merge_requests/1022)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after 2 days.

#### Week 47

- **KTextEditor - [Convert part metadata to JSON](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/447)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after 2 days.

- **KSyntaxHighlighting - [TOML: add number prefix, inf, nan and more datetime format ; fix multi-strings closing](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/394)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Python: add pyi extension (python interface)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/395)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Javascript: remove previousDibling keyword (probably a typo for previousSibling)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/393)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Add GPS Exchange Format (GPX) type](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/392)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged after 3 days.

- **Kate - [lsp: specify codeActionKind valueSet](https://invent.kde.org/utilities/kate/-/merge_requests/1020)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [[gdbplugin] GDB backend reimplemented using GDB/MI](https://invent.kde.org/utilities/kate/-/merge_requests/1009)**<br />
Request authored by [Héctor Mesa Jiménez](https://invent.kde.org/hectorm) and merged after 10 days.

- **Kate - [LSP: Inlay Hint support](https://invent.kde.org/utilities/kate/-/merge_requests/1011)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 8 days.

- **KSyntaxHighlighting - [CSS family: add properties, functions and fr unit ; remove FPWD and old proposed properties](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/391)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [share more filesystem watchers](https://invent.kde.org/utilities/kate/-/merge_requests/1019)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KTextEditor - [Kate:TextLine: switch to std::shared_ptr](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/446)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 46

- **Kate - [ensure to better pass focus back to right widget](https://invent.kde.org/utilities/kate/-/merge_requests/1016)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [reuse our filesystemwatchers](https://invent.kde.org/utilities/kate/-/merge_requests/1017)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [lspclient: only use snippets if adding parentheses is so configured](https://invent.kde.org/utilities/kate/-/merge_requests/1018)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **KSyntaxHighlighting - [install a index.katesyntax when QRC_SYNTAX is OFF](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/389)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 6 days.

- **Kate - [[WelcomeView] Fix layout stretching](https://invent.kde.org/utilities/kate/-/merge_requests/1012)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged after 3 days.

- **Kate - [[WelcomeView] Make UI more responsive for windows of large size](https://invent.kde.org/utilities/kate/-/merge_requests/1015)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged after one day.

- **KTextEditor - [Fix setting default mark when ctrl is pressed](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/445)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **Kate - [avoid all session restore/save work for KWrite](https://invent.kde.org/utilities/kate/-/merge_requests/1014)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [Make the Rust language mode use the cstyle indenter](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/390)**<br />
Request authored by [Bharadwaj Raju](https://invent.kde.org/bharadwaj-raju) and merged after 2 days.

- **KTextEditor - [Fix caret with inline notes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/444)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [lsp: enable snippet support](https://invent.kde.org/utilities/kate/-/merge_requests/1005)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

#### Week 45

- **Kate - [avoid dangling document pointers](https://invent.kde.org/utilities/kate/-/merge_requests/1007)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [delay plugin load/unload until we save](https://invent.kde.org/utilities/kate/-/merge_requests/1008)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [C23: missing wN length modifiers with d and i in printf_like](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/388)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [CMake: remove duplicate &lt;list&gt; and &lt;context&gt; ; some speed optimizations](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/382)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 8 days.

- **Kate - [lsp format on save](https://invent.kde.org/utilities/kate/-/merge_requests/995)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 10 days.

- **Kate - [Fix typo in option name](https://invent.kde.org/utilities/kate/-/merge_requests/1006)**<br />
Request authored by [Antonio Rojas](https://invent.kde.org/arojas) and merged at creation day.

- **KSyntaxHighlighting - [Add syntax definition for CSV and TSV](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/387)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [JSON: small optimization on number regex](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/386)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [TabMimeData: Set URLs on mime data](https://invent.kde.org/utilities/kate/-/merge_requests/1004)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged at creation day.

- **Kate - [KateTabBar: Add document icon to drag pixmap](https://invent.kde.org/utilities/kate/-/merge_requests/1003)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged at creation day.

- **KSyntaxHighlighting - [Go: improved support for literal numbers, add unicode escape characters and new predefined type](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/385)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [dynamic RegExpr has its own type (~2.2% faster)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/384)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KTextEditor - [Add &quot;Character&quot; the &quot;Insert Tab&quot; action name.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/443)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **KTextEditor - [Fix condition for installing desktop file](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/442)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

#### Week 44

- **Kate - [avoid stall for hanging network shares](https://invent.kde.org/utilities/kate/-/merge_requests/1002)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [lspclient: add rootfile pattern to detect rootpath](https://invent.kde.org/utilities/kate/-/merge_requests/907)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 41 days.

- **Kate - [Output: Use a QTextBrowser instead of QTreeView](https://invent.kde.org/utilities/kate/-/merge_requests/1001)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Build-plugin: Provide the run terminals in the plugin](https://invent.kde.org/utilities/kate/-/merge_requests/999)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

- **Kate - [Make LSP context menu actually contextual](https://invent.kde.org/utilities/kate/-/merge_requests/974)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 13 days.

- **Kate - [lsp: Optimize diagnostics handling](https://invent.kde.org/utilities/kate/-/merge_requests/1000)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [XML: character &lt; in an ENTITY is highlighted as an error](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/383)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [Allow setting breakpoints from the icon border](https://invent.kde.org/utilities/kate/-/merge_requests/997)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after one day.

- **KTextEditor - [Set default mark type only if control is pressed](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/441)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **KSyntaxHighlighting - [Jira: fix Bold and Stroked Out at start of line and various improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/381)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [Focus search line in config dialog](https://invent.kde.org/utilities/kate/-/merge_requests/996)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **KTextEditor - [Don&#x27;t install desktop file for katepart when building against Qt6](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/439)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **KSyntaxHighlighting - [CMake: fix nested parentheses highlinthing](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/378)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **KSyntaxHighlighting - [systemd_unit: update to systemd v252](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/380)**<br />
Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr) and merged at creation day.

- **Kate - [katesessionmanager: Make Actions of desktop file start new instance of Kate](https://invent.kde.org/utilities/kate/-/merge_requests/991)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged after one day.

- **Kate - [Add files opened via open action to recent files](https://invent.kde.org/utilities/kate/-/merge_requests/993)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after one day.

- **KSyntaxHighlighting - [Common Lisp: add .el extension ; highlight &amp;operator, #&#x27;symbol and &#x27;symbol ; fix number preceded with -](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/379)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [powershell.xml: Improvements in strings recongnition](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/377)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after 13 days.

