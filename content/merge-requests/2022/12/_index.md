---
title: Merge Requests - December 2022
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2022/12/
---

#### Week 52

- **Kate - [Save new external tool under selected category](https://invent.kde.org/utilities/kate/-/merge_requests/1053)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Resolve conflict on .ex file extension](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/412)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **Kate - [Localize messages about as root at startup](https://invent.kde.org/utilities/kate/-/merge_requests/1013)**<br />
Request authored by [Ilya Pominov](https://invent.kde.org/ipominov) and merged after 44 days.

- **Kate - [Fix session restore for KWrite](https://invent.kde.org/utilities/kate/-/merge_requests/1045)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 11 days.

- **Kate - [Add Formatting plugin](https://invent.kde.org/utilities/kate/-/merge_requests/1040)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 14 days.

- **KTextEditor - [Fix crash on undo after reload](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/459)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix 1 space indent wrongly detected](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/458)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Update the cursor and anchor to sync with the visual but not scroll for selectAll](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/457)**<br />
Request authored by [Xuetian Weng](https://invent.kde.org/xuetianweng) and merged after 4 days.

- **KSyntaxHighlighting - [Markdown: remove dynamic rules that are not needed](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/411)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [Julia: merge hundreds of rules into a single regex ; fix adjoint operator ; includes ##Comments](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/410)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

#### Week 51

- **Kate - [Fix current desktop check on Wayland](https://invent.kde.org/utilities/kate/-/merge_requests/1050)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **Kate - [Reuse DiffWidget instead of creating new ones](https://invent.kde.org/utilities/kate/-/merge_requests/1049)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Port away from deprecated KWindowSystem API](https://invent.kde.org/utilities/kate/-/merge_requests/1048)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after 2 days.

- **KTextEditor - [Implement invokeAction and commit preedit when click outside the preedit.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/456)**<br />
Request authored by [Xuetian Weng](https://invent.kde.org/xuetianweng) and merged after one day.

- **Kate - [Added Commands To Copy and Cut Searched Lines](https://invent.kde.org/utilities/kate/-/merge_requests/1030)**<br />
Request authored by [Bob 872185](https://invent.kde.org/bobeightseven) and merged after 13 days.

- **Kate - [Show a simpler dialog when closing a window with only one changed file](https://invent.kde.org/utilities/kate/-/merge_requests/1047)**<br />
Request authored by [Tobias Leupold](https://invent.kde.org/tleupold) and merged at creation day.

- **Kate - [avoid to show welcome view multiple times](https://invent.kde.org/utilities/kate/-/merge_requests/1042)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

- **Kate - [lspclient: Respect ENV:GOPATH and ENV:CARGO_HOME](https://invent.kde.org/utilities/kate/-/merge_requests/1046)**<br />
Request authored by [Neehar Vijay](https://invent.kde.org/env) and merged after one day.

#### Week 50

- **Kate - [try to implement some pure SDI mode](https://invent.kde.org/utilities/kate/-/merge_requests/1038)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 5 days.

- **Kate - [Don&#x27;t pass startup id to KStartupInfo::appStarted](https://invent.kde.org/utilities/kate/-/merge_requests/1044)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **Kate - [Remove duplicate headers between cpp/h files](https://invent.kde.org/utilities/kate/-/merge_requests/1043)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **KTextEditor - [Add missing KWindowSystem dependency](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/455)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **Kate - [Properly implement window activation on X11](https://invent.kde.org/utilities/kate/-/merge_requests/1041)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **KSyntaxHighlighting - [avoid temporary constructions of Definition in AbstractHighlighter::highlightLine](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/407)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [GCode: numbers are optional with parameters](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/406)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [cmake: Use ECMQmlModule for qtquick plugin](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/404)**<br />
Request authored by [Biswapriyo Nath](https://invent.kde.org/biswapriyo) and merged after 2 days.

- **Kate - [Fix inlayHints not appearing with rust_analyzer](https://invent.kde.org/utilities/kate/-/merge_requests/1037)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [lspclient: tweak default Dart server](https://invent.kde.org/utilities/kate/-/merge_requests/1039)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **KTextEditor - [Fix 1 space indent not detected](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/454)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [documents: Hide &#x27;Open Widgets&#x27; when no widgets open](https://invent.kde.org/utilities/kate/-/merge_requests/1036)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Draw a background around inlay hints](https://invent.kde.org/utilities/kate/-/merge_requests/1035)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix size issue with config widet](https://invent.kde.org/utilities/kate/-/merge_requests/1034)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 49

- **Kate - [Inlay hints fixes](https://invent.kde.org/utilities/kate/-/merge_requests/1033)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [lspclient: support adjusting trigger characters](https://invent.kde.org/utilities/kate/-/merge_requests/1031)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 2 days.

- **KTextEditor - [Improve handling of indentation based folding](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/451)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KTextEditor - [Added New Script Sort Uniq](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/453)**<br />
Request authored by [Bob 872185](https://invent.kde.org/bobeightseven) and merged at creation day.

- **KSyntaxHighlighting - [Add new keywords: `get` and `set`](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/396)**<br />
Request authored by [Marat Nagayev](https://invent.kde.org/nagayev) and merged after 14 days.

- **KSyntaxHighlighting - [Add MapCSS highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/403)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **Kate - [Add context, Catalan translation was wrong](https://invent.kde.org/utilities/kate/-/merge_requests/1032)**<br />
Request authored by [Albert Astals Cid](https://invent.kde.org/aacid) and merged at creation day.

- **KTextEditor - [Accept event when clearing multicursors](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/452)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Markdown: Use nim highlighting in nim blocks](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/401)**<br />
Request authored by [Jake Leahy](https://invent.kde.org/amadan) and merged after 2 days.

- **KSyntaxHighlighting - [Nim: Small fixes for syntax](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/402)**<br />
Request authored by [Jake Leahy](https://invent.kde.org/amadan) and merged after one day.

- **Kate - [lsp: dont use kate template for snippet completion](https://invent.kde.org/utilities/kate/-/merge_requests/1029)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Fix generating index.katesyntax when QRC_SYNTAX is off](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/400)**<br />
Request authored by [Antonio Rojas](https://invent.kde.org/arojas) and merged at creation day.

- **KSyntaxHighlighting - [ini: Add more extensions](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/399)**<br />
Request authored by [Biswapriyo Nath](https://invent.kde.org/biswapriyo) and merged after one day.

#### Week 48

- **Kate - [LSP: Add completionItem/resolve support for additionalTextEdits](https://invent.kde.org/utilities/kate/-/merge_requests/1027)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [GitWidget Adjustments](https://invent.kde.org/utilities/kate/-/merge_requests/1028)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Save view config when closing tab](https://invent.kde.org/utilities/kate/-/merge_requests/1025)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [filetree: reuse katefileactions code](https://invent.kde.org/utilities/kate/-/merge_requests/1024)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after one day.

- **Kate - [Do not show tabswitcher when there are no documents](https://invent.kde.org/utilities/kate/-/merge_requests/1026)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **KTextEditor - [Add Dart and Go to katemoderc](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/449)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **Kate - [Introduce CommitView](https://invent.kde.org/utilities/kate/-/merge_requests/1023)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Some changes and fixes to projects.](https://invent.kde.org/utilities/kate/-/merge_requests/1021)**<br />
Request authored by [Patrick Northon](https://invent.kde.org/patlefort) and merged after 5 days.

