---
title: Merge Requests - February 2022
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2022/02/
---

#### Week 08

- **Kate - [bottom bar spans full window width](https://invent.kde.org/utilities/kate/-/merge_requests/646)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KTextEditor - [minimal fix to keep font styles](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/301)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Remove menu-indicator from buttons](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/300)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [set the windowFilePath property to have a proxy icon on macOS](https://invent.kde.org/utilities/kate/-/merge_requests/645)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged at creation day.

- **Kate - [search: Give file items a slightly different background color](https://invent.kde.org/utilities/kate/-/merge_requests/637)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [add status bar to bottom row](https://invent.kde.org/utilities/kate/-/merge_requests/641)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [kateconsole: wordwrap the label](https://invent.kde.org/utilities/kate/-/merge_requests/644)**<br />
Request authored by [Jan Blackquill](https://invent.kde.org/cblack) and merged at creation day.

- **Kate - [Fix crash when closing multiple tabs if limited number of tabs is enabled](https://invent.kde.org/utilities/kate/-/merge_requests/643)**<br />
Request authored by [Francisco Boni Neto](https://invent.kde.org/insilications) and merged at creation day.

- **Kate - [Fix assertion when closing the last document in a split view](https://invent.kde.org/utilities/kate/-/merge_requests/642)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged after one day.

- **KTextEditor - [remove modified icon from the statusbar](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/297)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KTextEditor - [katebookmarks: always populate the menu with some actions](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/296)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged after one day.

- **Kate - [Urlbar: Collapse project base](https://invent.kde.org/utilities/kate/-/merge_requests/640)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [simplify toolview titles](https://invent.kde.org/utilities/kate/-/merge_requests/639)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [introduce a object library for shared stuff](https://invent.kde.org/utilities/kate/-/merge_requests/638)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [urlbar: Add fuzzy filtering and filtering for symbols](https://invent.kde.org/utilities/kate/-/merge_requests/633)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [docmanager: reuse document list](https://invent.kde.org/utilities/kate/-/merge_requests/635)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [view: fix buggy scrolling on macOS](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/295)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged after one day.

- **Kate - [UrlBar: Implement filtering](https://invent.kde.org/utilities/kate/-/merge_requests/632)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Port away from deprecated KAuth includes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/288)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after 4 days.

- **Kate - [Sort by url instead of comparing strings](https://invent.kde.org/utilities/kate/-/merge_requests/631)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix urlbar seperator icons](https://invent.kde.org/utilities/kate/-/merge_requests/630)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [katestatusbar: fix margin in status bar buttons on certain styles](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/294)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged at creation day.

- **KTextEditor - [vimode: fix swapped modifiers and arrow keys in insert/replace modes on macOS](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/291)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged after one day.

- **Kate - [S&amp;R: Fix project-plugin integration](https://invent.kde.org/utilities/kate/-/merge_requests/629)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

#### Week 07

- **Kate - [Fix tab jumping with unlimited tabs](https://invent.kde.org/utilities/kate/-/merge_requests/628)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Make url bar separators smaller](https://invent.kde.org/utilities/kate/-/merge_requests/626)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Urlbar improvements](https://invent.kde.org/utilities/kate/-/merge_requests/623)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [handle project file watching inside KateProject](https://invent.kde.org/utilities/kate/-/merge_requests/621)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Fix LRU tab behaviour](https://invent.kde.org/utilities/kate/-/merge_requests/625)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Make our sidebars smaller in height](https://invent.kde.org/utilities/kate/-/merge_requests/624)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Statusbar improvements](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/290)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Urlbar updates](https://invent.kde.org/utilities/kate/-/merge_requests/622)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [search: Make file names more consistent](https://invent.kde.org/utilities/kate/-/merge_requests/619)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [search: Fix expand triangle color with light themes](https://invent.kde.org/utilities/kate/-/merge_requests/618)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Remove superfluous QTextStream::setCodec call](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/289)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **KSyntaxHighlighting - [Propagate Qt major version to external project build of the host tools](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/300)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **Kate - [highlight open documents in file list](https://invent.kde.org/utilities/kate/-/merge_requests/615)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [urlbar: Don&#x27;t rely on focus change](https://invent.kde.org/utilities/kate/-/merge_requests/620)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix clangtidy readibility warnings](https://invent.kde.org/utilities/kate/-/merge_requests/617)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [store url bar pointer, don&#x27;t rely on parent](https://invent.kde.org/utilities/kate/-/merge_requests/616)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [urlbar: handle untitled docs](https://invent.kde.org/utilities/kate/-/merge_requests/614)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [lspclient: avoid segfault on restart](https://invent.kde.org/utilities/kate/-/merge_requests/613)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **Kate - [Urlbar improvements](https://invent.kde.org/utilities/kate/-/merge_requests/612)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Remove margins in File Browser Toolbar + Git Widget](https://invent.kde.org/utilities/kate/-/merge_requests/610)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after one day.

- **Kate - [Remove margins in the search plugin](https://invent.kde.org/utilities/kate/-/merge_requests/611)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after one day.

- **Kate - [Move paintItemViewText to drawing_utils](https://invent.kde.org/utilities/kate/-/merge_requests/609)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Documents toolview: Remove margins in toolbar](https://invent.kde.org/utilities/kate/-/merge_requests/608)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after one day.

- **Kate - [Resize the Kate Session-Applet](https://invent.kde.org/utilities/kate/-/merge_requests/602)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 2 days.

- **Kate - [Add action to focus navigation bar](https://invent.kde.org/utilities/kate/-/merge_requests/606)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Update Go LSP configuration](https://invent.kde.org/utilities/kate/-/merge_requests/584)**<br />
Request authored by [Adam Jimerson](https://invent.kde.org/vendion) and merged after 9 days.

#### Week 06

- **Kate - [Introduce KateNavigationBar](https://invent.kde.org/utilities/kate/-/merge_requests/597)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [Improve history actions tooltips](https://invent.kde.org/utilities/kate/-/merge_requests/604)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Minor LSP Goto symbol dialog fixes](https://invent.kde.org/utilities/kate/-/merge_requests/605)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [urlbar: reuse the current document if not modified](https://invent.kde.org/utilities/kate/-/merge_requests/607)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Refactor and use common utils to apply LSP edits](https://invent.kde.org/utilities/kate/-/merge_requests/603)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **KTextEditor - [use the url of the latest used document for save](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/286)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Remove KItemModels requirement from lsp](https://invent.kde.org/utilities/kate/-/merge_requests/601)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [fix menu parents for wayland](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/287)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Fix crash in LSPClientCompletion](https://invent.kde.org/utilities/kate/-/merge_requests/600)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Shorten the tool files names](https://invent.kde.org/utilities/kate/-/merge_requests/599)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [lspclient: use multiple items for a diagnostic item with embedded newlines](https://invent.kde.org/utilities/kate/-/merge_requests/595)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **Kate - [LSP completion: Support additionalTextEdits](https://invent.kde.org/utilities/kate/-/merge_requests/598)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [S&amp;R: Fix Look-ahead &amp; behind highlighting &amp; replace](https://invent.kde.org/utilities/kate/-/merge_requests/587)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 4 days.

- **KSyntaxHighlighting - [Bash/Zsh: fix keyword ! in ! cmd](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/299)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 4 days.

- **Kate - [TabBar: Allow to configure tab scrollability and text eliding](https://invent.kde.org/utilities/kate/-/merge_requests/594)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Search: Adjust spacing to be more like previous version](https://invent.kde.org/utilities/kate/-/merge_requests/593)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Remove stray KColorScheme var](https://invent.kde.org/utilities/kate/-/merge_requests/592)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix tabs not created for stashed documents](https://invent.kde.org/utilities/kate/-/merge_requests/591)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix stashing not working when Kate is quit using Ctrl+Q](https://invent.kde.org/utilities/kate/-/merge_requests/590)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [LSP: Color the completion icons using theme colors](https://invent.kde.org/utilities/kate/-/merge_requests/589)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Default sync for filebrowser plugin](https://invent.kde.org/utilities/kate/-/merge_requests/588)**<br />
Request authored by [Aline Lermen](https://invent.kde.org/alinelermen) and merged after one day.

- **KTextEditor - [Differentiate docs with identical filenames](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/279)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KTextEditor - [Rewrite completion delegates](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/284)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Remove more dead completion code](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/283)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Remove ExpandingTree, merge with ArgumentHintTree](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/282)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Remove dead code from completion](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/281)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

#### Week 05

- **Kate - [search: Use documentName for untitled files](https://invent.kde.org/utilities/kate/-/merge_requests/586)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Make sure we always only reselect old item if its still the first](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/276)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 7 days.

- **Kate - [Don&#x27;t use a nested eventloop when renaming files](https://invent.kde.org/utilities/kate/-/merge_requests/569)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 17 days.

- **Kate - [Add options to disable search as you type](https://invent.kde.org/utilities/kate/-/merge_requests/566)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 19 days.

- **Kate - [Improve search results painting](https://invent.kde.org/utilities/kate/-/merge_requests/583)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Move the tab-bar to the top of the tool-view](https://invent.kde.org/utilities/kate/-/merge_requests/575)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 11 days.

- **KTextEditor - [completion: show doc in a tooltip instead of as a tree node](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/271)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 7 days.

- **KTextEditor - [Don&#x27;t copy QKeyEvents, use a simple data class instead](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/270)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged after 7 days.

- **Kate - [Create filebrowser on demand](https://invent.kde.org/utilities/kate/-/merge_requests/577)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 9 days.

- **KTextEditor - [Abort completion on view scroll on wayland](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/274)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 5 days.

- **Kate - [Only match filename by default in quickopen](https://invent.kde.org/utilities/kate/-/merge_requests/581)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Update External Tools doc](https://invent.kde.org/utilities/kate/-/merge_requests/582)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [lspclient: also support some alternative WorkspaceEdit replies](https://invent.kde.org/utilities/kate/-/merge_requests/580)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 3 days.

