---
title: Merge Requests - May 2022
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2022/05/
---

#### Week 22

- **Kate - [lspclient: create a single servermanager at plugin level](https://invent.kde.org/utilities/kate/-/merge_requests/751)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 3 days.

#### Week 21

- **Kate - [Add action to hide all tool views](https://invent.kde.org/utilities/kate/-/merge_requests/749)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 2 days.

- **Kate - [Ask user for permission to start a LSP server](https://invent.kde.org/utilities/kate/-/merge_requests/745)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 4 days.

- **KTextEditor - [Don&#x27;t add empty dictionary to context menu](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/376)**<br />
Request authored by [loh tar](https://invent.kde.org/lohtar) and merged after one day.

- **KSyntaxHighlighting - [Bash: fix comments in double braces](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/311)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [systemd unit: update to systemd v251](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/310)**<br />
Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr) and merged after 4 days.

- **Kate - [Port away from deprecated KNewStuff API](https://invent.kde.org/utilities/kate/-/merge_requests/747)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **KTextEditor - [Fix Qt6 build issue about using QVector without template arg](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/375)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

#### Week 20

- **Kate - [LSP: Add expand and shrink selection actions](https://invent.kde.org/utilities/kate/-/merge_requests/719)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 24 days.

- **KTextEditor - [Fix whitespace slider &amp; group text related options](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/373)**<br />
Request authored by [loh tar](https://invent.kde.org/lohtar) and merged at creation day.

- **KTextEditor - [port to standard C++ smart pointers where possible](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/372)**<br />
Request authored by [Xaver Hugl](https://invent.kde.org/zamundaaa) and merged at creation day.

- **Kate - [Ensure cursor is valid if only --line is set](https://invent.kde.org/utilities/kate/-/merge_requests/742)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after one day.

- **KTextEditor - [Ensure to keep special dictionary setting on replaced word](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/370)**<br />
Request authored by [loh tar](https://invent.kde.org/lohtar) and merged after 5 days.

- **KTextEditor - [observe changes on textChanged for QSpinBox](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/371)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Center align git blame inline notes text](https://invent.kde.org/utilities/kate/-/merge_requests/743)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 19

- **Kate - [Move shared code of Kate &amp; KWrite and the addons to some libkateprivate shared library](https://invent.kde.org/utilities/kate/-/merge_requests/739)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 5 days.

- **KTextEditor - [add multi cursor API](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/368)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 6 days.

- **KTextEditor - [context menu / spell check improvements](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/369)**<br />
Request authored by [loh tar](https://invent.kde.org/lohtar) and merged after 3 days.

- **Kate - [Fix tab closing with multiple viewspaces](https://invent.kde.org/utilities/kate/-/merge_requests/740)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [katetabbar: Fix drag pixmap size on high-dpi](https://invent.kde.org/utilities/kate/-/merge_requests/738)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged at creation day.

#### Week 18

- **KTextEditor - [Add an action to remove cursors from empty lines](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/367)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Add methods to set/get cursors and selections](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/366)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Fix issue #14 - Enable setting view theme from C++](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/365)**<br />
Request authored by [Ibrahim Abdullah](https://invent.kde.org/abcdcba) and merged after 2 days.

- **Kate - [Make SnippetRepository a non qobject](https://invent.kde.org/utilities/kate/-/merge_requests/734)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Snippet fixes](https://invent.kde.org/utilities/kate/-/merge_requests/731)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [Add git actions to an action collection](https://invent.kde.org/utilities/kate/-/merge_requests/732)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 2 days.

- **Kate - [don&#x27;t hide toolbar on first start for Kate](https://invent.kde.org/utilities/kate/-/merge_requests/733)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [refactor menu-bar](https://invent.kde.org/utilities/kate/-/merge_requests/702)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 20 days.

- **Kate - [Highlight current file when &quot;sync with doc&quot; is checked](https://invent.kde.org/utilities/kate/-/merge_requests/730)**<br />
Request authored by [loh tar](https://invent.kde.org/lohtar) and merged after one day.

- **KTextEditor - [cache regex on first use](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/364)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [EmulatedCommandBarTest: port from processEvents to QTRY_VERIFY](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/363)**<br />
Request authored by [David Faure](https://invent.kde.org/dfaure) and merged at creation day.

- **KTextEditor - [EmulatedCommandBarTest: shorten timeout from 4s to 2s.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/362)**<br />
Request authored by [David Faure](https://invent.kde.org/dfaure) and merged at creation day.

- **KTextEditor - [avoid storing instance in commands/motions](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/361)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KTextEditor - [avoid cursor move on insert of line at EOF on save](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/360)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

#### Week 17

- **Kate - [Add (empty) Selection menu, for katepart](https://invent.kde.org/utilities/kate/-/merge_requests/728)**<br />
Request authored by [David Faure](https://invent.kde.org/dfaure) and merged at creation day.

- **Kate - [Fix update text on branch button after branch change](https://invent.kde.org/utilities/kate/-/merge_requests/726)**<br />
Request authored by [loh tar](https://invent.kde.org/lohtar) and merged after one day.

- **KTextEditor - [Refactor menu bar](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/353)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 18 days.

