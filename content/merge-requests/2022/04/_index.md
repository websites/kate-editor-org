---
title: Merge Requests - April 2022
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2022/04/
---

#### Week 17

- **Kate - [no delayed readSessionConfig](https://invent.kde.org/utilities/kate/-/merge_requests/727)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Buildplugin: Improve error / warning detection](https://invent.kde.org/utilities/kate/-/merge_requests/723)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [changed a string from plural to singular](https://invent.kde.org/utilities/kate/-/merge_requests/718)**<br />
Request authored by [Frank Steinmetzger](https://invent.kde.org/steinmetzger) and merged after one day.

#### Week 16

- **Kate - [fix appstream data](https://invent.kde.org/utilities/kate/-/merge_requests/714)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [add model tester](https://invent.kde.org/utilities/kate/-/merge_requests/715)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [implement close all projects](https://invent.kde.org/utilities/kate/-/merge_requests/710)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 3 days.

- **Kate - [Add default config for c-sharp language server](https://invent.kde.org/utilities/kate/-/merge_requests/713)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after one day.

- **Kate - [Changing the number of used colors in the rainbow parens plugin](https://invent.kde.org/utilities/kate/-/merge_requests/711)**<br />
Request authored by [Giovana Santana](https://invent.kde.org/giovanadionisio) and merged after one day.

- **KTextEditor - [Multicursors small improvements](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/358)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 2 days.

- **Kate - [LSP Client: Add more default language server configurations](https://invent.kde.org/utilities/kate/-/merge_requests/706)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 4 days.

- **Kate - [Mouse forward/backward buttons configurable](https://invent.kde.org/utilities/kate/-/merge_requests/704)**<br />
Request authored by [Philip K. Gisslow](https://invent.kde.org/ripxorip) and merged after 3 days.

- **Kate - [make project restoration configurable](https://invent.kde.org/utilities/kate/-/merge_requests/708)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Some fixes on project plugin](https://invent.kde.org/utilities/kate/-/merge_requests/703)**<br />
Request authored by [loh tar](https://invent.kde.org/lohtar) and merged after 4 days.

- **Kate - [Set correct name for history actions](https://invent.kde.org/utilities/kate/-/merge_requests/705)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **KTextEditor - [Add a minimal example to help devs](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/357)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix completion leaks, dont use null parent](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/356)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Add option to show folding markers on hover only](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/355)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

#### Week 15

- **KTextEditor - [Custom selection painting improvements](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/354)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Simplify header install locations](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/305)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 23 days.

#### Week 14

- **Kate - [katetabbar: do not set any icon when the document is modified](https://invent.kde.org/utilities/kate/-/merge_requests/701)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged after one day.

- **KTextEditor - [vimode: add another motion command for moving down one line](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/352)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged after 2 days.

- **KSyntaxHighlighting - [Fix Haxe RawString escaping](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/306)**<br />
Request authored by [Ben Harless](https://invent.kde.org/bulbyvr) and merged after 12 days.

- **Kate - [katetabbar: do not set any icon when the document is modified](https://invent.kde.org/utilities/kate/-/merge_requests/700)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged after one day.

- **Kate - [fix --tempfile handling](https://invent.kde.org/utilities/kate/-/merge_requests/699)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

- **KTextEditor - [Fix help link for editing command line](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/350)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after one day.

- **Kate - [replace QtSingleApplication](https://invent.kde.org/utilities/kate/-/merge_requests/698)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

- **KSyntaxHighlighting - [cmake.xml: Updates for CMake 3.23](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/307)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

#### Week 13

- **Kate - [Add convenience split actions](https://invent.kde.org/utilities/kate/-/merge_requests/695)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 2 days.

- **Kate - [build-plugin: Remove Select Target dialog](https://invent.kde.org/utilities/kate/-/merge_requests/694)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 2 days.

- **Kate - [Add Nim support](https://invent.kde.org/utilities/kate/-/merge_requests/680)**<br />
Request authored by [Shalok Shalom](https://invent.kde.org/shalokshalom) and merged after 15 days.

- **KTextEditor - [Improve Color theme config page](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/349)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 5 days.

- **KTextEditor - [Suggestion: Enable auto-brackets by default](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/345)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 10 days.

- **Kate - [auto hide tabs if we have just one document open](https://invent.kde.org/utilities/kate/-/merge_requests/697)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [change tabbing default to something more common](https://invent.kde.org/utilities/kate/-/merge_requests/696)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

