---
title: Merge Requests - July 2022
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2022/07/
---

#### Week 30

- **KSyntaxHighlighting - [Lua: new styles (Special Variable and Self Variable), remove hard color and...](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/323)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [Kate Snippets: add examples for working with selections](https://invent.kde.org/utilities/kate/-/merge_requests/821)**<br />
Request authored by [Beluga Whale](https://invent.kde.org/belugawhale) and merged after 2 days.

- **Kate - [Cleanup git blame tooltip](https://invent.kde.org/utilities/kate/-/merge_requests/818)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Disable spellcheck for ...](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/322)**<br />
Request authored by [loh tar](https://invent.kde.org/lohtar) and merged after one day.

- **Kate - [php_parser: reduce scope variable detected by code quality scanning](https://invent.kde.org/utilities/kate/-/merge_requests/817)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after one day.

- **Kate - [Git: Support submodules](https://invent.kde.org/utilities/kate/-/merge_requests/819)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [php_parser: fix wrong argument parsing](https://invent.kde.org/utilities/kate/-/merge_requests/813)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after 3 days.

#### Week 29

- **Kate - [Warn user if kateproject file&#x27;s JSON is malformed and failed to parse [proper]](https://invent.kde.org/utilities/kate/-/merge_requests/816)**<br />
Request authored by [Pablo Rauzy](https://invent.kde.org/pablorackham) and merged at creation day.

- **Kate - [Konsole plugin: quote args before passing them to the shell](https://invent.kde.org/utilities/kate/-/merge_requests/811)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 2 days.

- **Kate - [LSP: Fix hand cursor remaining in some cases](https://invent.kde.org/utilities/kate/-/merge_requests/812)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Port away from deprecated KMessageBox::sorry](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/389)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after 5 days.

- **Kate - [DocumentsTree: Allow rearranging items by DND](https://invent.kde.org/utilities/kate/-/merge_requests/810)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Port away from deprecated KMessageBox::sorry](https://invent.kde.org/utilities/kate/-/merge_requests/809)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after 4 days.

#### Week 28

- **Kate - [Add show file git history action to actionCollection](https://invent.kde.org/utilities/kate/-/merge_requests/807)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Restore find_package call for Qt{5,6}Sql](https://invent.kde.org/utilities/kate/-/merge_requests/808)**<br />
Request authored by [Heiko Becker](https://invent.kde.org/heikobecker) and merged at creation day.

- **Kate - [Fix startHostProcess() for KProcess &lt; KF5.97](https://invent.kde.org/utilities/kate/-/merge_requests/803)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

- **Kate - [Fix delayed popup for session-related buttons](https://invent.kde.org/utilities/kate/-/merge_requests/805)**<br />
Request authored by [Máté Pozsgay](https://invent.kde.org/matepozsgay) and merged after one day.

- **KTextEditor - [Pass parent widget to print preview dialog](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/388)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after 5 days.

- **Kate - [avoid that we create invalid file names](https://invent.kde.org/utilities/kate/-/merge_requests/735)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 68 days.

#### Week 27

- **KSyntaxHighlighting - [Fix atom-one-*.theme diff colors](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/319)**<br />
Request authored by [Demmark Forrester](https://invent.kde.org/demmark) and merged after 3 days.

- **Kate - [Apply symbols filtering when symbols are parsed](https://invent.kde.org/utilities/kate/-/merge_requests/797)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after one day.

- **Kate - [use the same icon for toolviews and config page](https://invent.kde.org/utilities/kate/-/merge_requests/802)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **Kate - [Use better icons for a few actions](https://invent.kde.org/utilities/kate/-/merge_requests/798)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **Kate - [Clean up filetree context menu order](https://invent.kde.org/utilities/kate/-/merge_requests/799)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **Kate - [Pass parent widget to file properties dialog](https://invent.kde.org/utilities/kate/-/merge_requests/800)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **Kate - [[addons/project] Pass parent widget to dialogs](https://invent.kde.org/utilities/kate/-/merge_requests/796)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **Kate - [File delete confirm dialogs: avoid Yes/No buttons](https://invent.kde.org/utilities/kate/-/merge_requests/795)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **Kate - [automatic clang-format run (clang 14)](https://invent.kde.org/utilities/kate/-/merge_requests/794)**<br />
Request authored by [Harald Sitter](https://invent.kde.org/sitter) and merged after 2 days.

- **Kate - [use startHostProcess when we intend a process to run on the host](https://invent.kde.org/utilities/kate/-/merge_requests/776)**<br />
Request authored by [Harald Sitter](https://invent.kde.org/sitter) and merged after 8 days.

#### Week 26

- **Kate - [Remove unused includes](https://invent.kde.org/utilities/kate/-/merge_requests/793)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **Kate - [Tabbar: Start drag on leaving current viewspace](https://invent.kde.org/utilities/kate/-/merge_requests/791)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Compiler explorer: avoid Yes/No buttons](https://invent.kde.org/utilities/kate/-/merge_requests/786)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **Kate - [Remove create new text file entry](https://invent.kde.org/utilities/kate/-/merge_requests/765)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after 13 days.

- **Kate - [Always show icon for tabs](https://invent.kde.org/utilities/kate/-/merge_requests/782)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Git confirmation dialogs: avoid Yes/No buttons](https://invent.kde.org/utilities/kate/-/merge_requests/790)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **Kate - [File browser: avoid Yes/No buttons in dialog](https://invent.kde.org/utilities/kate/-/merge_requests/787)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **Kate - [Branch delete dialog: avoid Yes/No button](https://invent.kde.org/utilities/kate/-/merge_requests/788)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **Kate - [Session already opened dialog: avoid Yes/No buttons](https://invent.kde.org/utilities/kate/-/merge_requests/789)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **Kate - [Viewspace close dialog: avoid Yes/No buttons](https://invent.kde.org/utilities/kate/-/merge_requests/785)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **Kate - [ColorPicker: avoid emitting inlineNotesChanged too much](https://invent.kde.org/utilities/kate/-/merge_requests/783)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Dir drop dialog: use &quot;open&quot; term for consistency, avoid Yes/No buttons](https://invent.kde.org/utilities/kate/-/merge_requests/784)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KTextEditor - [KateSearchBar: prefer action texts over Yes / No buttons in dialogs](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/386)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **Kate - [Allow configuring sidebar icon size](https://invent.kde.org/utilities/kate/-/merge_requests/781)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

