---
title: Merge Requests - January 2022
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2022/01/
---

#### Week 04

- **Kate - [S&amp;R: Fix assert when no result is selected](https://invent.kde.org/utilities/kate/-/merge_requests/579)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **KTextEditor - [Avoid flicker on refresh](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/277)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [install plugin in kf&lt;version&gt;](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/275)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **KTextEditor - [KateModOnHdPrompt::slotDiff: Create diff file in temp folder](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/273)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KTextEditor - [avoid flicker for border on size changes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/272)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Allow DND of tabs among viewspaces](https://invent.kde.org/utilities/kate/-/merge_requests/564)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 13 days.

- **KSyntaxHighlighting - [Use KF standard scheme for the include headers install location](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/294)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 13 days.

- **Kate - [Backport of QProcess stuff](https://invent.kde.org/utilities/kate/-/merge_requests/578)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [Python: fix line continuation starting with a string](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/298)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **KSyntaxHighlighting - [CSS: fix nested function call](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/297)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **KSyntaxHighlighting - [Bash: fix line-break in double bracket condition](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/296)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

- **KTextEditor - [When using tabs, use tabs to auto indent](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/267)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [External tools: Add &quot;execute on save&quot; option](https://invent.kde.org/utilities/kate/-/merge_requests/543)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 23 days.

- **Kate - [Don&#x27;t create KColorScheme uselessly all the time](https://invent.kde.org/utilities/kate/-/merge_requests/576)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix leaks in undo manager](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/269)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Update LSP docs to mention new features](https://invent.kde.org/utilities/kate/-/merge_requests/574)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [fix execution of diff in cwd](https://invent.kde.org/utilities/kate/-/merge_requests/573)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

- **KTextEditor - [Don&#x27;t scroll on select all](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/248)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 12 days.

- **KTextEditor - [Fix auto-reloading files in git repositories](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/268)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **KTextEditor - [Handle single statement condition blocks better](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/266)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [[R] add new pipe from R 4.1](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/295)**<br />
Request authored by [christophe dervieux](https://invent.kde.org/cderv) and merged at creation day.

#### Week 03

- **KTextEditor - [Fix cstyle for cases where there is a func in param](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/265)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Dont bring kate to front when files externally modified](https://invent.kde.org/utilities/kate/-/merge_requests/572)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Reload doc on OnDiskCreated](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/264)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Backport LSP fixes (including cwd issue fix)](https://invent.kde.org/utilities/kate/-/merge_requests/571)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [avoid that we execute LSP binaries from cwd](https://invent.kde.org/utilities/kate/-/merge_requests/570)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KTextEditor - [Allow disabling focus frame](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/263)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Fix comment toggling when all lines in selection aren&#x27;t commented](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/260)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KTextEditor - [Fix selection scrolling from line border works only downwards](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/261)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Fix camel cursor when last word is of one letter only](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/258)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KTextEditor - [only start programs in user&#x27;s path](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/262)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [always sort views by usage](https://invent.kde.org/utilities/kate/-/merge_requests/567)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KTextEditor - [Do proper fuzzy matching in completion](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/257)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Improve views closing in split view behaviour](https://invent.kde.org/utilities/kate/-/merge_requests/560)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [S&amp;R: Disable folder options for In Open File(s)](https://invent.kde.org/utilities/kate/-/merge_requests/565)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **KTextEditor - [Fix undo history wrongfully restored in some cases](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/256)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 02

- **Kate - [Fix terminal focus action text when focusing/defocusing](https://invent.kde.org/utilities/kate/-/merge_requests/559)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [step down warning level when LSP not found](https://invent.kde.org/utilities/kate/-/merge_requests/563)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [filetree: Fix folders collapse after opening new file](https://invent.kde.org/utilities/kate/-/merge_requests/558)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Clear highlights if text field becomes empty](https://invent.kde.org/utilities/kate/-/merge_requests/553)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 5 days.

- **Kate - [Move ScrollBarRestorer to ktexteditor_utils](https://invent.kde.org/utilities/kate/-/merge_requests/556)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [addons: sprinkle some more kcoreaddons_add_plugin](https://invent.kde.org/utilities/kate/-/merge_requests/561)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **KTextEditor - [Introduce auto indent detection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/253)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KTextEditor - [Restore undo history when document is same](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/252)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [gitblame: Don&#x27;t return error on untracked files](https://invent.kde.org/utilities/kate/-/merge_requests/557)**<br />
Request authored by [Mufeed Ali](https://invent.kde.org/mufeedali) and merged after one day.

- **KTextEditor - [Fix drag pixmap highlight sometimes does not match original text](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/251)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [lspclient: only allow symbol view header toggle sort when applicable](https://invent.kde.org/utilities/kate/-/merge_requests/555)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 2 days.

- **KTextEditor - [Find: Update working range when replacing](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/247)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KTextEditor - [set QClipboard::Selection for select all](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/250)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Update Stan highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/293)**<br />
Request authored by [Brian Ward](https://invent.kde.org/wardbrian) and merged after 3 days.

- **Kate - [Optimize project file filtering a bit](https://invent.kde.org/utilities/kate/-/merge_requests/551)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Fix crash when switching between tabs while search is running](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/246)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Search: Utilize context lengths more efficiently](https://invent.kde.org/utilities/kate/-/merge_requests/552)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix search for unsaved files](https://invent.kde.org/utilities/kate/-/merge_requests/554)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 01

- **Kate - [Introduce ktexteditor utils](https://invent.kde.org/utilities/kate/-/merge_requests/550)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [OutputView: Don&#x27;t resize to contents all the time](https://invent.kde.org/utilities/kate/-/merge_requests/549)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Update org.kde.kate.desktop](https://invent.kde.org/utilities/kate/-/merge_requests/548)**<br />
Request authored by [Sam Greenwood](https://invent.kde.org/samgreenwood) and merged after one day.

- **Kate - [Fix multiple info strings in search](https://invent.kde.org/utilities/kate/-/merge_requests/547)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KSyntaxHighlighting - [Add CI qt6 support](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/292)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged after one day.

- **Kate - [ExternalTools: Maintain scroll position on doc reload](https://invent.kde.org/utilities/kate/-/merge_requests/544)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Filter out binary files in folder based projects + untracked git](https://invent.kde.org/utilities/kate/-/merge_requests/545)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 52

- **Kate - [LSPConfigWidget: port to QFormLayout](https://invent.kde.org/utilities/kate/-/merge_requests/542)**<br />
Request authored by [Jan Blackquill](https://invent.kde.org/cblack) and merged at creation day.

- **KTextEditor - [Various Qt6 build fixes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/238)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged after 5 days.

- **KTextEditor - [Introduce Document::aboutToSave signal](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/243)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [More Qt6 fixes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/245)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **KTextEditor - [Change the code so as not to copy a QWheelEvent](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/244)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

