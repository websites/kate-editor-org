---
title: Merge Requests - March 2022
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2022/03/
---

#### Week 13

- **Kate - [Fix possible infinite formatting calls](https://invent.kde.org/utilities/kate/-/merge_requests/693)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Implement KWrite as simplified Kate without plugins &amp; sessions](https://invent.kde.org/utilities/kate/-/merge_requests/692)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

- **Kate - [[stable] Replace KateColorSchemeChooser with using KColorSchemeManager directly](https://invent.kde.org/utilities/kate/-/merge_requests/690)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

#### Week 12

- **Kate - [build: Append to textEdit directly for better performance](https://invent.kde.org/utilities/kate/-/merge_requests/688)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Fix switching back to normal mode from VI](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/348)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [simplify + optimize uniq command](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/347)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Go syntax - Separate ControlFlow from Keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/304)**<br />
Request authored by [Amaury Bouchra Pilet](https://invent.kde.org/abp) and merged after 4 days.

- **Kate - [Snippets addon: comment out debug output](https://invent.kde.org/utilities/kate/-/merge_requests/687)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KTextEditor - [Implement multicursor copy/paste](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/339)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KTextEditor - [Hide statusbar with permanent widget](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/344)**<br />
Request authored by [David Redondo](https://invent.kde.org/davidre) and merged at creation day.

- **KTextEditor - [Improve cursor movement with RTL text in doc](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/343)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix cursor in RTL text with dyn wrap on](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/342)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Add support for custom line height](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/338)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Don&#x27;t try to convert non-char key codes to strings](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/340)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **KTextEditor - [Add CI qt6 support](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/278)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged after 44 days.

- **KTextEditor - [Try to fix completion positioning on Wayland](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/285)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 41 days.

#### Week 11

- **Kate - [ignore blame errors](https://invent.kde.org/utilities/kate/-/merge_requests/685)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [Bash/Zsh: fix expression closing parenthesis in regex](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/303)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KTextEditor - [Move print and export actions into submenu](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/337)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **KTextEditor - [Add actions to allow adding a new line above/below](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/332)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [multicursor: Use same code path as primary for cursor placing](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/336)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix multicursor position after selection removal](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/335)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Allow auto saving document](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/334)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Move filebrowser related actions into submenu](https://invent.kde.org/utilities/kate/-/merge_requests/678)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **Kate - [provide menu with all sessions](https://invent.kde.org/utilities/kate/-/merge_requests/675)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 3 days.

- **Kate - [Remove the run-query default shortcut as its ambiguous](https://invent.kde.org/utilities/kate/-/merge_requests/679)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **Kate - [lspclient: support rust-analyzer expandMacro](https://invent.kde.org/utilities/kate/-/merge_requests/681)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **KTextEditor - [Multicursor text insertion improvements](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/331)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Replace KateColorSchemeChooser with using KColorSchemeManager directly](https://invent.kde.org/utilities/kate/-/merge_requests/668)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after 7 days.

- **KTextEditor - [multicursors: combine cursor and selection into one type](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/330)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Remove shortcut for duplicateLinesUp](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/328)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Rename forward/backward actions to better distinguish them](https://invent.kde.org/utilities/kate/-/merge_requests/673)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 2 days.

- **Kate - [Fix triplicated documents when occurs git conflict](https://invent.kde.org/utilities/kate/-/merge_requests/634)**<br />
Request authored by [Davi Marinho](https://invent.kde.org/davimarinho) and merged after 19 days.

- **KTextEditor - [Multicursor perf fixes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/327)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix toggle comment with space at the start](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/326)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 10

- **KTextEditor - [Fix cursor merging in first/last line + tests](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/325)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Make the multicursor modifier configurable](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/324)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [store projects in session as json lines](https://invent.kde.org/utilities/kate/-/merge_requests/672)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KTextEditor - [Move some editing related actions to Edit menu from Tools](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/255)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 55 days.

- **KTextEditor - [Fix multicursor undo selection restoration](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/323)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Allow toggling camel cursor via action](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/322)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Clear highlights when multiselecting next occurence](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/321)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Implemenet KTextEditor::SessionConfigInterface on project plugin](https://invent.kde.org/utilities/kate/-/merge_requests/596)**<br />
Request authored by [Felipe Borges](https://invent.kde.org/bumbleblo) and merged after 29 days.

- **Kate - [Add windows installer to appstream artifacts](https://invent.kde.org/utilities/kate/-/merge_requests/667)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **Kate - [Docs: Update Kate swap file path](https://invent.kde.org/utilities/kate/-/merge_requests/670)**<br />
Request authored by [Daniel Aleksandersen](https://invent.kde.org/aleksandersen) and merged at creation day.

- **Kate - [Docs: Clarify “disk file” in the context of backup saves](https://invent.kde.org/utilities/kate/-/merge_requests/671)**<br />
Request authored by [Daniel Aleksandersen](https://invent.kde.org/aleksandersen) and merged at creation day.

- **KTextEditor - [Find next occurence and select it](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/320)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Make multicursor shortcuts actions](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/319)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Multicursor fixes and more tests](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/317)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [New shortcuts for duplicate-line-up/down](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/318)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Show projects view when folder is opened](https://invent.kde.org/utilities/kate/-/merge_requests/669)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **KTextEditor - [Add some tests for multicursor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/316)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Read script action shortcuts as portable text instead of native text](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/315)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **kate-editor.org - [multicursor blog](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/37)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix LSPHover, honor markup kind](https://invent.kde.org/utilities/kate/-/merge_requests/666)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [project: Use same indentation as document tree](https://invent.kde.org/utilities/kate/-/merge_requests/664)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Multicursor support](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/311)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **KTextEditor - [Don&#x27;t build KAuth integration on Windows](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/314)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **Kate - [S&amp;R: Adjust search result colors](https://invent.kde.org/utilities/kate/-/merge_requests/665)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Search&amp;Replace: improve filtering behaviour and match counts](https://invent.kde.org/utilities/kate/-/merge_requests/663)**<br />
Request authored by [Alexander Neundorf](https://invent.kde.org/neundorf) and merged at creation day.

- **Kate - [Search&amp;Replace: add &quot;Role&quot; suffix to &quot;MatchItem&quot; role](https://invent.kde.org/utilities/kate/-/merge_requests/661)**<br />
Request authored by [Alexander Neundorf](https://invent.kde.org/neundorf) and merged at creation day.

- **Kate - [show documents as loaded from config](https://invent.kde.org/utilities/kate/-/merge_requests/660)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Search&amp;Replace: also update the number of matches in the file-items](https://invent.kde.org/utilities/kate/-/merge_requests/662)**<br />
Request authored by [Alexander Neundorf](https://invent.kde.org/neundorf) and merged at creation day.

- **Kate - [Search&amp;Replace: update info line correctly depending on current filter](https://invent.kde.org/utilities/kate/-/merge_requests/648)**<br />
Request authored by [Alexander Neundorf](https://invent.kde.org/neundorf) and merged after 8 days.

- **KTextEditor - [vimode: fix extra newline when using the &quot;Find&quot; action](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/313)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged at creation day.

#### Week 09

- **KTextEditor - [vimode: hlsearch fixes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/312)**<br />
Request authored by [Martin Seher](https://invent.kde.org/quatu) and merged at creation day.

- **KTextEditor - [fix config dialog sizes for KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/309)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [ensure some proper initial size](https://invent.kde.org/utilities/kate/-/merge_requests/659)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Minor cosmetics](https://invent.kde.org/utilities/kate/-/merge_requests/657)**<br />
Request authored by [Alexander Neundorf](https://invent.kde.org/neundorf) and merged at creation day.

- **Kate - [make config pages in config dialog scrollable](https://invent.kde.org/utilities/kate/-/merge_requests/653)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 3 days.

- **KTextEditor - [add new line on save inside the buffer](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/310)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Implement TextBuffer::saveBuffer without QTextStream](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/298)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged after 7 days.

- **KTextEditor - [clazy/clang-tidy fixes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/303)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KTextEditor - [Use lineLength in more places](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/305)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Remove the KateTextLine string() accessor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/306)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [TextLine: Remove unused operator[]](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/307)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [textline: Remove empty destructor and inline ctors](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/308)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [lspclient: fix possible use-after-free](https://invent.kde.org/utilities/kate/-/merge_requests/655)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged at creation day.

- **Kate - [Port away from deprecated KToolInvocation::invokeTerminal](https://invent.kde.org/utilities/kate/-/merge_requests/656)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **Kate - [invent project file names for generated projects](https://invent.kde.org/utilities/kate/-/merge_requests/654)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [Bash: fix parameter expansion replacement with path without extended glob](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/301)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **KSyntaxHighlighting - [Lua: fix function highlighting with local function (#17)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/302)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [Handle SIGINT/SIGTERM to prevent accidental data loss](https://invent.kde.org/utilities/kate/-/merge_requests/649)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [unify signal handling for KWrite &amp; Kate](https://invent.kde.org/utilities/kate/-/merge_requests/651)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [vimode: implement hlsearch](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/293)**<br />
Request authored by [Martin Seher](https://invent.kde.org/quatu) and merged after 8 days.

- **KTextEditor - [Mark some internal classes as final](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/304)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [sessionapplet: Allow resizing the sessionapplet](https://invent.kde.org/utilities/kate/-/merge_requests/647)**<br />
Request authored by [Mufeed Ali](https://invent.kde.org/mufeedali) and merged after 2 days.

- **Kate - [Block view creation when opening docs from cmd line](https://invent.kde.org/utilities/kate/-/merge_requests/627)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 9 days.

- **KTextEditor - [consistent right aligned status bar content](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/302)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KTextEditor - [Fix default font weight never gets honored](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/299)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **kate-editor.org - [content: correct path to kdesrc-buildrc](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/36)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged after 4 days.

