---
title: Merge Requests - February 2025
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2025/02/
---

#### Week 06

- **Kate - [Appium test: Allow snap test](https://invent.kde.org/utilities/kate/-/merge_requests/1713)**<br />
Request authored by [Benjamin Port](https://invent.kde.org/bport) and merged after one day.

- **Kate - [Improve diff widget](https://invent.kde.org/utilities/kate/-/merge_requests/1709)**<br />
Request authored by [Leo Ruggeri](https://invent.kde.org/mrpink) and merged after 6 days.

- **KSyntaxHighlighting - [Add xml/yaml linting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/678)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged after 11 days.

- **KSyntaxHighlighting - [cmake: Remove error highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/680)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [addons/git-blame: take into account blame-ignore-revs](https://invent.kde.org/utilities/kate/-/merge_requests/1696)**<br />
Request authored by [Méven Car](https://invent.kde.org/meven) and merged after 17 days.

- **Kate - [Fix minor typo (stared -&gt; started)](https://invent.kde.org/utilities/kate/-/merge_requests/1711)**<br />
Request authored by [Josep M. Ferrer](https://invent.kde.org/jferrer) and merged at creation day.

- **Kate - [abort search with error if the base dir is not there](https://invent.kde.org/utilities/kate/-/merge_requests/1710)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

- **Kate - [Fix selenium test from flakiness](https://invent.kde.org/utilities/kate/-/merge_requests/1703)**<br />
Request authored by [Benjamin Port](https://invent.kde.org/bport) and merged after 12 days.

- **KTextEditor - [Kate::TextBlock::splitBlock changes (see commit messages)](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/782)**<br />
Request authored by [BZZZZ DZZZZ](https://invent.kde.org/bzzzz) and merged after 3 days.

