---
title: Merge Requests - January 2025
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2025/01/
---

#### Week 05

- **Kate - [Fix build with Qt 6.10](https://invent.kde.org/utilities/kate/-/merge_requests/1708)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **KTextEditor - [Adjusted theme config page margins](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/781)**<br />
Request authored by [Leo Ruggeri](https://invent.kde.org/mrpink) and merged after one day.

- **Kate - [Buildsystem - Rename CMake option from BUILD_PCH to ENABLE_PCH](https://invent.kde.org/utilities/kate/-/merge_requests/1707)**<br />
Request authored by [Allen Winter](https://invent.kde.org/winterz) and merged at creation day.

- **KTextEditor - [Allow disabling &#x27;cycle through bookmarks&#x27; behaviour](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/780)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Added a syntax highlighting file for the ARMv7-A assembly language](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/677)**<br />
Request authored by [Leo Marušić](https://invent.kde.org/leomarusic) and merged after 8 days.

- **Kate - [ensure diagnostics stuff doesn&#x27;t die to early](https://invent.kde.org/utilities/kate/-/merge_requests/1706)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

- **Kate - [Improved CommitDelegate appearence](https://invent.kde.org/utilities/kate/-/merge_requests/1705)**<br />
Request authored by [Leo Ruggeri](https://invent.kde.org/mrpink) and merged after 3 days.

- **KTextEditor - [avoid text hint if already triggered when popup menu is requested](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/778)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

- **KTextEditor - [Fix KateCompletionTree width](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/779)**<br />
Request authored by [Leo Ruggeri](https://invent.kde.org/mrpink) and merged after one day.

#### Week 04

- **KTextEditor - [Add xml/yaml linting](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/777)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **Kate - [Fix Search MatchExport](https://invent.kde.org/utilities/kate/-/merge_requests/1704)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Fix crash in KateTextHintManager](https://invent.kde.org/utilities/kate/-/merge_requests/1699)**<br />
Request authored by [Leo Ruggeri](https://invent.kde.org/mrpink) and merged after 4 days.

- **Kate - [GIT_SILENT: it compiles fine without kf6.10 deprecated methods](https://invent.kde.org/utilities/kate/-/merge_requests/1698)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged after 4 days.

- **Kate - [Try a new approach to build target column widths](https://invent.kde.org/utilities/kate/-/merge_requests/1702)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 3 days.

- **Kate - [BP: Add a progress message to show when output is hidden](https://invent.kde.org/utilities/kate/-/merge_requests/1694)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 4 days.

- **Kate - [Removed symbolview separator](https://invent.kde.org/utilities/kate/-/merge_requests/1700)**<br />
Request authored by [Leo Ruggeri](https://invent.kde.org/mrpink) and merged after one day.

- **KTextEditor - [GIT_SILENT: it compiles fine without kf6.10 deprecated methods](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/776)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged after one day.

#### Week 03

- **Kate - [Load templates on demand (speed up startup)](https://invent.kde.org/utilities/kate/-/merge_requests/1695)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

- **KTextEditor - [autotests/src/swapfiletest.h - include &lt;QTest&gt;](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/775)**<br />
Request authored by [Allen Winter](https://invent.kde.org/winterz) and merged at creation day.

- **Kate - [Avoid conflicts between requestors in KateTextHintManager](https://invent.kde.org/utilities/kate/-/merge_requests/1692)**<br />
Request authored by [Leo Ruggeri](https://invent.kde.org/mrpink) and merged after 5 days.

- **KTextEditor - [ensure we do not kill symlinks](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/774)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

- **KTextEditor - [Stop hover timer when cursor changes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/773)**<br />
Request authored by [Leo Ruggeri](https://invent.kde.org/mrpink) and merged after 3 days.

- **Kate - [Allow ignoring QSqlQuery prepare failure](https://invent.kde.org/utilities/kate/-/merge_requests/1691)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [Update the build plugin documentation](https://invent.kde.org/utilities/kate/-/merge_requests/1693)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

- **KTextEditor - [CI: Add linux-qt6-next build](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/772)**<br />
Request authored by [Albert Astals Cid](https://invent.kde.org/aacid) and merged at creation day.

- **KSyntaxHighlighting - [CI: Add linux-qt6-next build](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/676)**<br />
Request authored by [Albert Astals Cid](https://invent.kde.org/aacid) and merged at creation day.

#### Week 02

- **Kate - [Prevent hint separator lines to disappear](https://invent.kde.org/utilities/kate/-/merge_requests/1690)**<br />
Request authored by [Leo Ruggeri](https://invent.kde.org/mrpink) and merged at creation day.

- **Kate - [Improved lsp tooltip resize logic](https://invent.kde.org/utilities/kate/-/merge_requests/1689)**<br />
Request authored by [Leo Ruggeri](https://invent.kde.org/mrpink) and merged after one day.

- **KTextEditor - [Store Search/Replace history in state config](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/771)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after 2 days.

- **Kate - [Removed gitwidget buttons layout hardcoded margins](https://invent.kde.org/utilities/kate/-/merge_requests/1688)**<br />
Request authored by [Leo Ruggeri](https://invent.kde.org/mrpink) and merged after one day.

#### Week 01

- **Kate - [Add .kateproject to CMake kapptemplates if missing](https://invent.kde.org/utilities/kate/-/merge_requests/1687)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 4 days.

- **KTextEditor - [Fix default shortcuts for mac os](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/769)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KTextEditor - [simpler Kate::TextBlock::mergeBlock](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/767)**<br />
Request authored by [BZZZZ DZZZZ](https://invent.kde.org/bzzzz) and merged after 9 days.

