---
title: Merge Requests - November 2020
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2020/11/
---

#### Week 49

- **KSyntaxHighlighting - [systemd unit: update to systemd v247](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/112)**<br />
Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr) and merged at creation day.

#### Week 48

- **Kate - [filetree addon: make methods const where possible](https://invent.kde.org/utilities/kate/-/merge_requests/128)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 2 days.

- **Kate - [ui.rc files: consistenly use &lt;gui&gt; instead of deprecated &lt;kpartgui&gt;](https://invent.kde.org/utilities/kate/-/merge_requests/129)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after one day.

- **Kate - [Remove unused includes](https://invent.kde.org/utilities/kate/-/merge_requests/130)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **KSyntaxHighlighting - [ILERPG: simplify and test](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/110)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **Kate - [filetree addon: fix crash in filetree_model_test](https://invent.kde.org/utilities/kate/-/merge_requests/126)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KSyntaxHighlighting - [Zsh, Bash, Fish, Tcsh: add truncate and tsort in unixcommand keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/108)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Latex: some math environments can be nested](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/109)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

#### Week 47

- **Kate - [Port some KComboBox usage to QComboBox](https://invent.kde.org/utilities/kate/-/merge_requests/125)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **Kate - [Require KF 5.68](https://invent.kde.org/utilities/kate/-/merge_requests/124)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **KTextEditor - [Port KComboBox to QComboBox](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/43)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **Kate - [Properly unset EDITOR env var, instead of setting it as empty.](https://invent.kde.org/utilities/kate/-/merge_requests/123)**<br />
Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry) and merged after one day.

- **KSyntaxHighlighting - [Bash: many fixes and improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/105)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

- **KSyntaxHighlighting - [add --syntax-trace=stackSize](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/106)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

#### Week 46

- **Kate - [Port to new KPluginMetaData-based KParts API](https://invent.kde.org/utilities/kate/-/merge_requests/121)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after 2 days.

- **KSyntaxHighlighting - [php.xml: Fix matching endforeach](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/104)**<br />
Request authored by [Fabian Vogt](https://invent.kde.org/fvogt) and merged after 2 days.

- **KSyntaxHighlighting - [Move bestThemeForApplicationPalette from KTextEditor here](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/103)**<br />
Request authored by [Xaver Hugl](https://invent.kde.org/zamundaaa) and merged after one day.

- **KTextEditor - [Improve the automatic theme selection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/41)**<br />
Request authored by [Xaver Hugl](https://invent.kde.org/zamundaaa) and merged after one day.

- **Kate - [Port away from deprecated Qt::MidButton](https://invent.kde.org/utilities/kate/-/merge_requests/120)**<br />
Request authored by [Robert-André Mauchin](https://invent.kde.org/rmauchin) and merged at creation day.

- **KSyntaxHighlighting - [Update README](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/101)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **kate-editor.org - [Improve get-it page](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/9)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **kate-editor.org - [Update images in about-kate page](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/10)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

#### Week 45

- **KSyntaxHighlighting - [alert.xml: Add `NOQA` yet another popular alert in source code](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/100)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KSyntaxHighlighting - [The &quot;compact&quot; core function is missing.](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/99)**<br />
Request authored by [Vincenzo Buttazzo](https://invent.kde.org/buttazzo) and merged after one day.

- **KSyntaxHighlighting - [The position:sticky value is missing.](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/97)**<br />
Request authored by [Vincenzo Buttazzo](https://invent.kde.org/buttazzo) and merged at creation day.

- **KTextEditor - [[Vimode] Prevent search box from disappearing](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/40)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KSyntaxHighlighting - [Feature: Add the `comments.xml` as an umbrella syntax for various comment kinds](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/96)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after one day.

#### Week 44

- **KSyntaxHighlighting - [Feature: Add `SPDX-Comments` syntax generator and XML](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/90)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after 4 days.

