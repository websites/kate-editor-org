---
title: Merge Requests - March 2020
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2020/03/
---

#### Week 13

- **Kate - [lspclient: use pyls entry script for python LSP server](https://invent.kde.org/utilities/kate/-/merge_requests/71)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 13 days.

- **Kate - [build-plugin: pick and build default target when so requested](https://invent.kde.org/utilities/kate/-/merge_requests/72)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 3 days.

- **Kate - [[XMLTools Plugin] Port QRegExp to QRegularExpression](https://invent.kde.org/utilities/kate/-/merge_requests/62)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 47 days.

#### Week 11

- **Kate - [Use MarkInterfaceV2 to pass QIcons for the marker sidebar, not QPixmaps](https://invent.kde.org/utilities/kate/-/merge_requests/68)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after 19 days.

