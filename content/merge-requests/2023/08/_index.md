---
title: Merge Requests - August 2023
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2023/08/
---

#### Week 35

- **Kate - [Add CSS LSP like existing HTML one](https://invent.kde.org/utilities/kate/-/merge_requests/1292)**<br />
Request authored by [Mohammad Kazemi](https://invent.kde.org/mokazemi) and merged at creation day.

- **Kate - [DiagnosticsView: add actions to the Tools menu](https://invent.kde.org/utilities/kate/-/merge_requests/1291)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **Kate - [start open dialog in home if not started from terminal](https://invent.kde.org/utilities/kate/-/merge_requests/1290)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [Remove unneeded setting CMP0063 to NEW, implied by requiring CMake 3.16](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/528)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

#### Week 34

- **Kate - [lsp rapid json parsing fixes](https://invent.kde.org/utilities/kate/-/merge_requests/1288)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Add LSP support for window/showMessageRequest](https://invent.kde.org/utilities/kate/-/merge_requests/1284)**<br />
Request authored by [J. T. Elscott](https://invent.kde.org/jtelscott) and merged after 2 days.

- **Kate - [Fix typographical error in formatter](https://invent.kde.org/utilities/kate/-/merge_requests/1282)**<br />
Request authored by [Jan Keith Darunday](https://invent.kde.org/jankeithd) and merged at creation day.

- **Kate - [Pass parent window to KateSaveModifiedDialog](https://invent.kde.org/utilities/kate/-/merge_requests/1283)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **Kate - [Tests: avoid confusion by not putting testdata into unrelated qrc prefix](https://invent.kde.org/utilities/kate/-/merge_requests/1285)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KSyntaxHighlighting - [Bash: fix ${!}](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/527)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

#### Week 33

- **Kate - [Improve README structure, add screenshot](https://invent.kde.org/utilities/kate/-/merge_requests/1281)**<br />
Request authored by [Joshua Goins](https://invent.kde.org/redstrate) and merged at creation day.

- **Kate - [Fix crash on viewspace close](https://invent.kde.org/utilities/kate/-/merge_requests/1279)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [[addons/filebrowser] Enable building for KF6](https://invent.kde.org/utilities/kate/-/merge_requests/1280)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **KSyntaxHighlighting - [Improve dart support](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/526)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

#### Week 32

- **KTextEditor - [Don&#x27;t snap cursor to 0 if position is invalid](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/589)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [support composed keys in vi mode](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/588)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 6 days.

- **Kate - [Fix Open With in Filesystem view](https://invent.kde.org/utilities/kate/-/merge_requests/1278)**<br />
Request authored by [Gary Li](https://invent.kde.org/li-gary) and merged at creation day.

- **KSyntaxHighlighting - [Indexer: suggest more column=0, RegExpr to AnyChar ; fix the detection regex of DetectIdentifier](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/525)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [lsp: Port json reading to RapidJson](https://invent.kde.org/utilities/kate/-/merge_requests/1277)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KSyntaxHighlighting - [HTML: trailing &#x27;/&#x27; as attribute value and Attribute Separator as dsOperator](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/524)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

#### Week 31

- **Kate - [KateMainWindow: Prevent hamburger button from being pushed into the overflow](https://invent.kde.org/utilities/kate/-/merge_requests/1276)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged after 2 days.

- **KTextEditor - [allow to cycle through bookmarks](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/587)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [cmake.xml: `target_link_libraries` recognize library types](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/522)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after one day.

- **KSyntaxHighlighting - [add Homunculus theme in theme-data.qrc](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/521)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **Kate - [File Browser plugin: make search string partial-matching](https://invent.kde.org/utilities/kate/-/merge_requests/1274)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after one day.

- **Kate - [Fix diagnostics tab overlay](https://invent.kde.org/utilities/kate/-/merge_requests/1275)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Raku: fix quote-words and more](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/520)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Add syntax highlighting for jsonnet](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/519)**<br />
Request authored by [Ribhav Kaul](https://invent.kde.org/ribhavkaul) and merged after one day.

- **Kate - [avoid that the session save timer messes up session saving](https://invent.kde.org/utilities/kate/-/merge_requests/1272)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Add missing $ to fix installed translated documentation](https://invent.kde.org/utilities/kate/-/merge_requests/1273)**<br />
Request authored by [Heiko Becker](https://invent.kde.org/heikobecker) and merged at creation day.

- **KTextEditor - [Remove code variants for building with Qt 6](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/586)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KTextEditor - [Fix icon border not updated on save](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/585)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [cmake.xml: improve the generator to produce optimized regexes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/518)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after one day.

