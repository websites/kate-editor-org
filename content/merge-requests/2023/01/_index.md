---
title: Merge Requests - January 2023
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2023/01/
---

#### Week 05

- **Kate - [Allow to build against last kf6](https://invent.kde.org/utilities/kate/-/merge_requests/1085)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged after one day.

- **Kate - [git stuff improvements](https://invent.kde.org/utilities/kate/-/merge_requests/1091)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Commit View improvements](https://invent.kde.org/utilities/kate/-/merge_requests/1090)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [terraform: add .tfvars as extra extension](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/439)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged after one day.

- **KSyntaxHighlighting - [Replace skippableOffsetId with a boolean that indicates if skipOffset is available](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/437)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **kate-editor.org - [Blog/Tutorial about kate&#x27;s git features](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/50)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Update the documentation to reflect menu changes](https://invent.kde.org/utilities/kate/-/merge_requests/1086)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **Kate - [DiffWidget: Support Automatic / manual reloading](https://invent.kde.org/utilities/kate/-/merge_requests/1089)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix DiffWidget scrolled to bottom on open](https://invent.kde.org/utilities/kate/-/merge_requests/1087)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Clean up some Qt 5 removal leftovers](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/438)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

#### Week 04

- **Kate - [lspclient: add default configuration for OpenSCAD](https://invent.kde.org/utilities/kate/-/merge_requests/1083)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged at creation day.

- **Kate - [Tooltips: Use QFrame::Box](https://invent.kde.org/utilities/kate/-/merge_requests/1084)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Port away from deprecated QMouseEvent ctors](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/481)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **KSyntaxHighlighting - [HAML: re-use existing Ruby highlighter definition and add support for more filters](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/434)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged after 6 days.

- **KTextEditor - [kf6: Pass range, cursor, linerange by value](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/471)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 8 days.

- **KTextEditor - [Fold AnnotationViewInterfaceV2 into V1 (API breakage)](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/478)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after one day.

- **KTextEditor - [Port away from deprecated QEvent members](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/480)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **KTextEditor - [Adapt to QKeyCombination changes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/479)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **KSyntaxHighlighting - [Port indexer host build to use the Qt 6 host prefix path](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/436)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **KSyntaxHighlighting - [Implement XML validation with Xerces](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/435)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [[gdbplugin] show CPU registers for GDB backend.](https://invent.kde.org/utilities/kate/-/merge_requests/1072)**<br />
Request authored by [Héctor Mesa Jiménez](https://invent.kde.org/hectorm) and merged after 5 days.

- **KTextEditor - [Port away from deprecated QVariant API](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/476)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **KTextEditor - [Port away from deprecated container and string APIs](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/477)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **KTextEditor - [Fix home key doesn&#x27;t move secondary cursor to start](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/475)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Adapt to changed KSelectAction triggered signal API](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/473)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after one day.

- **Kate - [Build-plugin: Add actions to switch between build tabs](https://invent.kde.org/utilities/kate/-/merge_requests/1081)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

- **Kate - [Replace QScopedPointer with std::unique_ptr](https://invent.kde.org/utilities/kate/-/merge_requests/1082)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Remove deprecated API](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/474)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **Kate - [Fix colors for GDB view being wrong way around](https://invent.kde.org/utilities/kate/-/merge_requests/1070)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after 2 days.

- **Kate - [Replace QSharedPointer with std::shared_ptr](https://invent.kde.org/utilities/kate/-/merge_requests/1069)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Fix multiple branch buttons in statusbar with multiple projects](https://invent.kde.org/utilities/kate/-/merge_requests/1080)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add missing includes and libraries](https://invent.kde.org/utilities/kate/-/merge_requests/1079)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

#### Week 03

- **Kate - [Diagnostics and LSP tweaks](https://invent.kde.org/utilities/kate/-/merge_requests/1074)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **Kate - [Fix katebuild error/warning colors](https://invent.kde.org/utilities/kate/-/merge_requests/1071)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged at creation day.

- **Kate - [katefiletree katefileactions.h is included twice](https://invent.kde.org/utilities/kate/-/merge_requests/1077)**<br />
Request authored by [Raresh Rus](https://invent.kde.org/nmariusp) and merged at creation day.

- **Kate - [kateconfigdialog kwidgetsaddons_version.h was included twice](https://invent.kde.org/utilities/kate/-/merge_requests/1075)**<br />
Request authored by [Raresh Rus](https://invent.kde.org/nmariusp) and merged at creation day.

- **Kate - [kateviewmanager kwidgetsaddons_version.h is included twice](https://invent.kde.org/utilities/kate/-/merge_requests/1076)**<br />
Request authored by [Raresh Rus](https://invent.kde.org/nmariusp) and merged at creation day.

- **Kate - [Fix urlbar doesn&#x27;t select first item on filtering](https://invent.kde.org/utilities/kate/-/merge_requests/1073)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Racket: Remove Color bracket hack](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/428)**<br />
Request authored by [shenleban tongying](https://invent.kde.org/slbtongying) and merged after 2 days.

- **KSyntaxHighlighting - [Log: fix slow search regex ; add Critical section ; some improvement in Log File (advanced)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/429)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [Show active git branch in statusbar](https://invent.kde.org/utilities/kate/-/merge_requests/1068)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Prepare link interface to clean-up in KParts](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/472)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KSyntaxHighlighting - [xml: Add mallard support](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/426)**<br />
Request authored by [sabri unal](https://invent.kde.org/sabriunal) and merged after 2 days.

- **KSyntaxHighlighting - [Remove deprecated API](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/432)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **KSyntaxHighlighting - [Remove Qt 5 support](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/433)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **KSyntaxHighlighting - [Bump Qt deprecation level to 6.4](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/431)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **KSyntaxHighlighting - [add basic highlighting for nginx configs](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/430)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged at creation day.

- **KSyntaxHighlighting - [HAML: support multi-line Ruby blocks ending with `,`](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/427)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged after 2 days.

- **Kate - [Fix clash between konsole&#x27;s search shortcut and ours](https://invent.kde.org/utilities/kate/-/merge_requests/1067)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Expose more &#x27;Editing&#x27; scripts as actions](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/470)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Add v language](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/425)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [use the skipOffsets cache only when a rule can be found in it (highlighter_benchmark 7% faster)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/424)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **kate-editor.org - [Update Windows release link](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/49)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged at creation day.

- **kate-editor.org - [Bring &quot;Get It&quot; page more in line with other KDE websites](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/47)**<br />
Request authored by [Joshua Goins](https://invent.kde.org/redstrate) and merged after one day.

- **Kate - [Dont init output and diag toolviews in kwrite](https://invent.kde.org/utilities/kate/-/merge_requests/1064)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Ninja: add dyndep rule and |@ operator](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/423)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **KSyntaxHighlighting - [Zig: fix number and escape char ; add ##Comments](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/422)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

#### Week 02

- **Kate - [Formatting plugin improvements](https://invent.kde.org/utilities/kate/-/merge_requests/1065)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Port xmlcheck plugin to use diagnostics view](https://invent.kde.org/utilities/kate/-/merge_requests/1063)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Introduce a MessageType enum and use that instead of QString](https://invent.kde.org/utilities/kate/-/merge_requests/1062)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Formatting improvements](https://invent.kde.org/utilities/kate/-/merge_requests/1061)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Add missing KF5::Codecs link](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/469)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **KSyntaxHighlighting - [Cobol: add extensions ; add exec sql block ; picture clause more permissive](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/421)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Add Zig language](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/420)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix indent failing due to unknown method &#x27;replace&#x27;](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/467)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KTextEditor - [Perform some initialization in initialization list](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/468)**<br />
Request authored by [Willyanto Willyanto](https://invent.kde.org/willyanto) and merged after one day.

- **KSyntaxHighlighting - [Add Cabal syntax](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/419)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 4 days.

- **Kate - [Add a diagnostic toolview](https://invent.kde.org/utilities/kate/-/merge_requests/1051)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 15 days.

- **KTextEditor - [Don&#x27;t show selection count when there are none](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/465)**<br />
Request authored by [Thomas Surrel](https://invent.kde.org/thsurrel) and merged after one day.

- **KTextEditor - [indentation/ruby: add support for Ruby 3&#x27;s endless method syntax](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/463)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged after 2 days.

#### Week 01

- **Kate - [In kate-ctags, go to declaration if no definition found](https://invent.kde.org/utilities/kate/-/merge_requests/1056)**<br />
Request authored by [Thomas Surrel](https://invent.kde.org/thsurrel) and merged after 4 days.

- **KTextEditor - [Remove unused code from KateViewEncodingAction](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/464)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **KTextEditor - [Don&#x27;t normalize urls.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/448)**<br />
Request authored by [Patrick Northon](https://invent.kde.org/patlefort) and merged after 43 days.

- **KSyntaxHighlighting - [Add Log File syntax](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/415)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [Add COBOL syntax](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/418)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Some minor optimizations to theme handling and removal of a shared definition in format](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/405)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 22 days.

- **KSyntaxHighlighting - [Replace DefinitionRef in StateData with a definition id](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/408)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 21 days.

- **kate-editor.org - [Kate 22.12 is released for a month.](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/46)**<br />
Request authored by [Shalok Shalom](https://invent.kde.org/shalokshalom) and merged after one day.

- **KSyntaxHighlighting - [Indexer: suggest more minimal=1 or other rule for RegExpr with lookhaed=1 and .*](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/414)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Indexer: check xml validity](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/416)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [feat: Add new syntax for `Earthfile`](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/413)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after one day.

- **Kate - [Add a toolbar for keyboard macros plugin.](https://invent.kde.org/utilities/kate/-/merge_requests/1054)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 2 days.

- **Kate - [Convert header guards to #pragma once](https://invent.kde.org/utilities/kate/-/merge_requests/1055)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after one day.

- **Kate - [Improvements around file history and commit view](https://invent.kde.org/utilities/kate/-/merge_requests/1058)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix showing diff for new and deleted files](https://invent.kde.org/utilities/kate/-/merge_requests/1059)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix widget closing and activation](https://invent.kde.org/utilities/kate/-/merge_requests/1057)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Add missing include](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/462)**<br />
Request authored by [David Edmundson](https://invent.kde.org/davidedmundson) and merged at creation day.

#### Week 52

- **Kate - [lsp: Downgrade window/logMessage to MessageType::Log](https://invent.kde.org/utilities/kate/-/merge_requests/1052)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

