---
title: Merge Requests - May 2023
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2023/05/
---

#### Week 22

- **Kate - [Adapt to KIconThemes changes](https://invent.kde.org/utilities/kate/-/merge_requests/1232)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

#### Week 21

- **Kate - [Move openDirectoryOrProject to Q_SLOTS](https://invent.kde.org/utilities/kate/-/merge_requests/1231)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after one day.

- **Kate - [Addition to Kate symbol viewer plugin to support Julia source files and changes to the support for Python source files.](https://invent.kde.org/utilities/kate/-/merge_requests/1230)**<br />
Request authored by [Cezar Tigaret](https://invent.kde.org/ctigaret) and merged after 3 days.

- **Kate - [Terminate all build processes on cancel](https://invent.kde.org/utilities/kate/-/merge_requests/1228)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 3 days.

- **Kate - [Make DiagnosticsView compatible with other hosts](https://invent.kde.org/utilities/kate/-/merge_requests/1229)**<br />
Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry) and merged after 2 days.

#### Week 20

- **Kate - [Add a default entry for GLSL LSP client](https://invent.kde.org/utilities/kate/-/merge_requests/1227)**<br />
Request authored by [Marián Konček](https://invent.kde.org/mkoncek) and merged at creation day.

- **KTextEditor - [Make KTextEditor Plugin project template KF5-co-installable](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/564)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KSyntaxHighlighting - [New Folding for markdown fenced code](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/485)**<br />
Request authored by [Fabian Wunsch](https://invent.kde.org/fabi) and merged after one day.

#### Week 19

- **Kate - [Allow showDiff() to be used in other hosting apps](https://invent.kde.org/utilities/kate/-/merge_requests/1225)**<br />
Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry) and merged after one day.

- **Kate - [Adapt to KTextEditor doc/view interface removal for Qt6](https://invent.kde.org/utilities/kate/-/merge_requests/1226)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Remove all extension interfaces, merge them into View/Document](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/547)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 14 days.

- **Kate - [Fix diagnostics filtering](https://invent.kde.org/utilities/kate/-/merge_requests/1224)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Add dedicated page for hands-on documentation on hosting ktexteditor plugins](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/562)**<br />
Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry) and merged after one day.

- **KSyntaxHighlighting - [Work/tpb](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/484)**<br />
Request authored by [Alexander Potashev](https://invent.kde.org/aspotashev) and merged at creation day.

- **KSyntaxHighlighting - [Add TextProto syntax (protocol buffer Text Format Language).](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/483)**<br />
Request authored by [Alexander Potashev](https://invent.kde.org/aspotashev) and merged at creation day.

- **KTextEditor - [Improve test running performance](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/561)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Unexport internal classes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/560)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Use Godot 4&#x27;s LSP port instead of Godot 3&#x27;s](https://invent.kde.org/utilities/kate/-/merge_requests/1223)**<br />
Request authored by [Michael Alexsander](https://invent.kde.org/yeldham) and merged at creation day.

- **Kate - [Add Godot LSP support](https://invent.kde.org/utilities/kate/-/merge_requests/1220)**<br />
Request authored by [Michael Alexsander](https://invent.kde.org/yeldham) and merged at creation day.

- **Kate - [Dispatch to newly added main window functions as slots](https://invent.kde.org/utilities/kate/-/merge_requests/1216)**<br />
Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry) and merged after 2 days.

- **Kate - [KF*NewStuff is only used in addons/project and addons/snippets](https://invent.kde.org/utilities/kate/-/merge_requests/1222)**<br />
Request authored by [Andreas Sturmlechner](https://invent.kde.org/asturmlechner) and merged at creation day.

- **Kate - [Add missing include (make it compile)](https://invent.kde.org/utilities/kate/-/merge_requests/1221)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **KTextEditor - [Remove KateTextBlockTest &amp; Unexport internal classes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/559)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Make vimode_keys test faster](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/558)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix double drive letters on &quot;Run Current Doc&quot;](https://invent.kde.org/utilities/kate/-/merge_requests/1217)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [send &quot;\r\n&quot; instead of &quot;\n&quot; on windows](https://invent.kde.org/utilities/kate/-/merge_requests/1218)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Adapt build against last KColorSchemeMenu.](https://invent.kde.org/utilities/kate/-/merge_requests/1219)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **KTextEditor - [Cleanup and optimize Kate::TextCursor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/556)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [fix some kf6 todos](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/555)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Make completiontest faster](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/557)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 18

- **Kate - [Get the DAP.json path from config](https://invent.kde.org/utilities/kate/-/merge_requests/1215)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged at creation day.

- **Kate - [Add configuration page for Debug plugin](https://invent.kde.org/utilities/kate/-/merge_requests/1213)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after one day.

- **Kate - [build-plugin: Fix case insensitive paths on windows](https://invent.kde.org/utilities/kate/-/merge_requests/1211)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 5 days.

- **KTextEditor - [Cleanup Cursor,Range &amp; LineRange](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/554)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Kate::TextBuffer cleanup](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/552)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Q_DECL_EXPORT -&gt; KATE_PRIVATE_EXPORT](https://invent.kde.org/utilities/kate/-/merge_requests/1212)**<br />
Request authored by [Vlad Zahorodnii](https://invent.kde.org/vladz) and merged at creation day.

- **KTextEditor - [better word completion limit](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/551)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [Add support for Elvish scripting language](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/480)**<br />
Request authored by [Lilith Houtjes](https://invent.kde.org/lilith) and merged at creation day.

- **Kate - [Debug buttons in debug toolview](https://invent.kde.org/utilities/kate/-/merge_requests/1210)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after one day.

- **KTextEditor - [ensure words that are ok spell check wise always end up in the completion](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/550)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Register the KateHextHintManager as a regular QObject property](https://invent.kde.org/utilities/kate/-/merge_requests/1209)**<br />
Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry) and merged at creation day.

- **KTextEditor - [Fix pch warnings on windows](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/549)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

