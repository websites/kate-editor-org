---
title: Merge Requests - September 2023
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2023/09/
---

#### Week 39

- **Kate - [Avoid some ubsan warnings](https://invent.kde.org/utilities/kate/-/merge_requests/1314)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [a11y: Actually return line when requested](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/609)**<br />
Request authored by [Michael Weghorn](https://invent.kde.org/michaelweghorn) and merged at creation day.

- **KTextEditor - [a11y: Implement QAccessibleEditableTextInterface](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/608)**<br />
Request authored by [Michael Weghorn](https://invent.kde.org/michaelweghorn) and merged at creation day.

- **KTextEditor - [Fix key delete in block selection when range contains no characters](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/607)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KTextEditor - [Improve pageup/down performance](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/605)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [CSS, SCSS: update properties and functions](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/552)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KTextEditor - [Avoid negative values in paintIndentMarker](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/604)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Remove KF_MAJOR_VERSION/QT_MAJOR_VERSION as we depend against qt6/kf6](https://invent.kde.org/utilities/kate/-/merge_requests/1313)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **Kate - [Remove check qt5 as we depend against qt6 now](https://invent.kde.org/utilities/kate/-/merge_requests/1312)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **Kate - [a11y: Set buddies in &quot;Projects&quot; settings page](https://invent.kde.org/utilities/kate/-/merge_requests/1311)**<br />
Request authored by [Michael Weghorn](https://invent.kde.org/michaelweghorn) and merged at creation day.

- **Kate - [improve language id computation](https://invent.kde.org/utilities/kate/-/merge_requests/1310)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KTextEditor - [Optimise readLine by iterating over m_text in a tighter loop](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/602)**<br />
Request authored by [Joe Dight](https://invent.kde.org/joedight) and merged at creation day.

- **KTextEditor - [a11y: Improve tab order for &quot;Appeareance&quot; settings](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/603)**<br />
Request authored by [Michael Weghorn](https://invent.kde.org/michaelweghorn) and merged at creation day.

- **KTextEditor - [a11y: Set more buddies in settings pages](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/601)**<br />
Request authored by [Michael Weghorn](https://invent.kde.org/michaelweghorn) and merged after one day.

- **Kate - [a11y: Set buddy for meta-info label](https://invent.kde.org/utilities/kate/-/merge_requests/1309)**<br />
Request authored by [Michael Weghorn](https://invent.kde.org/michaelweghorn) and merged at creation day.

- **Kate - [try to re-use open views to avoid massive windows spawning](https://invent.kde.org/utilities/kate/-/merge_requests/1307)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 3 days.

- **Kate - [show a xxx.yyy @ dir representation in the welcome page](https://invent.kde.org/utilities/kate/-/merge_requests/1308)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

#### Week 38

- **KSyntaxHighlighting - [Highlight MapCSS numeric and string condition values](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/550)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **KSyntaxHighlighting - [Highlight MapCSS numeric and string condition values](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/549)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **Kate - [Dont write to metainfo if there is nothing to save](https://invent.kde.org/utilities/kate/-/merge_requests/1306)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Explicitly call QCoreApplication::exit()](https://invent.kde.org/utilities/kate/-/merge_requests/1305)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged after one day.

- **Kate - [don&#x27;t enforce meta data sync](https://invent.kde.org/utilities/kate/-/merge_requests/1304)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Fix build](https://invent.kde.org/utilities/kate/-/merge_requests/1303)**<br />
Request authored by [Aleix Pol Gonzalez](https://invent.kde.org/apol) and merged at creation day.

- **KTextEditor - [doc: only write changed metadata to session config](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/600)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 37

- **Kate - [Update syntax highlighting documentation](https://invent.kde.org/utilities/kate/-/merge_requests/1297)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 5 days.

- **KTextEditor - [Optimize and simplify cursorAtBracket](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/598)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **KTextEditor - [katedocument.h update documentation since QColor::setNamedColor is not used anymore](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/596)**<br />
Request authored by [Raresh Rus](https://invent.kde.org/nmariusp) and merged after 7 days.

- **KTextEditor - [Fix rtl space visualization issues](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/599)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [LaTeX: fix bash code block](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/548)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [validatehl.sh: search language.xsd in the same directory as validatehl.sh...](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/546)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **KSyntaxHighlighting - [Update README with new url for documentation and note on ksyntaxhighlighter6](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/547)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [Remove Qt5 ifdef&#x27;d out code](https://invent.kde.org/utilities/kate/-/merge_requests/1302)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Drop dead branches for Qt5-based KUserFeedback](https://invent.kde.org/utilities/kate/-/merge_requests/1301)**<br />
Request authored by [Heiko Becker](https://invent.kde.org/heikobecker) and merged after one day.

- **KSyntaxHighlighting - [CLI: use a default dark theme with ansi format](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/545)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [Save edited (project) build targets in .kateproject.build](https://invent.kde.org/utilities/kate/-/merge_requests/1289)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 19 days.

- **Kate - [move kate to kf6](https://invent.kde.org/utilities/kate/-/merge_requests/1300)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Check KateLineLayout* for nullness everywhere](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/597)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Require Windows/Qt6 tests to pass again](https://invent.kde.org/utilities/kate/-/merge_requests/1299)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

#### Week 36

- **KSyntaxHighlighting - [highlighter for ANTLR](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/523)**<br />
Request authored by [Andrzej Borucki](https://invent.kde.org/andrzejbor) and merged after 35 days.

- **KSyntaxHighlighting - [Cherry-pick textproto syntax: master -&gt; kf5](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/544)**<br />
Request authored by [Alexander Potashev](https://invent.kde.org/aspotashev) and merged at creation day.

- **Kate - [Search: Don&#x27;t format replaced text as italic](https://invent.kde.org/utilities/kate/-/merge_requests/1296)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [a11y: Specify buddies for &quot;Appearance&quot; -&gt; &quot;General&quot;](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/594)**<br />
Request authored by [Michael Weghorn](https://invent.kde.org/michaelweghorn) and merged after one day.

- **KTextEditor - [KateLineLayout: Fix an if check](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/595)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [avoid clash with KF5](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/540)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 3 days.

- **KSyntaxHighlighting - [CLI: make --stdin optional when --output-format=ansi is used](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/539)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

- **KSyntaxHighlighting - [Add a tool that lists identical contexts in a syntax file](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/541)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **Kate - [gitblame: Fix inline notes with rtl text](https://invent.kde.org/utilities/kate/-/merge_requests/1295)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Fix inline notes with dynamic wrap disabled](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/593)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [AnsiHighlighter: fix not bold ANSI code](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/538)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [project plugin: add a reload action](https://invent.kde.org/utilities/kate/-/merge_requests/1294)**<br />
Request authored by [Karthik Nishanth](https://invent.kde.org/nishanthkarthik) and merged at creation day.

- **KSyntaxHighlighting - [new theme: Tokyo Night](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/537)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [relax region id to int, too](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/533)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Add parent window to KEditToolBar](https://invent.kde.org/utilities/kate/-/merge_requests/1293)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **KTextEditor - [InlineNote: Support RTL](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/591)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **KTextEditor - [use FoldingRegion](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/592)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [rewriting generate-dot-file in python and add new attributes...](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/536)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [PHP: add functions of php 8.3](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/534)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [JSP: groups identical rules in contexts used with IncludeRules](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/535)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [relax format id to int](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/532)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [helper to return the matching start or end region.](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/531)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [CSS, SCSS: update properties, functions, at-rules and more highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/529)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

#### Week 35

- **KSyntaxHighlighting - [Makefile: fix parenthesis in call function](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/530)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

