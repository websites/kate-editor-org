---
title: Merge Requests - November 2023
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2023/11/
---

#### Week 48

- **Kate - [Migrate Craft Appimage from Jenkins](https://invent.kde.org/utilities/kate/-/merge_requests/1319)**<br />
Request authored by [Julius Künzel](https://invent.kde.org/jlskuz) and merged after 45 days.

- **Kate - [Adds separator to terminal panel](https://invent.kde.org/utilities/kate/-/merge_requests/1367)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after 2 days.

- **Kate - [Allow reordering sidebar tabs using DND](https://invent.kde.org/utilities/kate/-/merge_requests/1366)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Set WrapMode on all TextArea components, and fix up some of the Labels](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/588)**<br />
Request authored by [ivan tkachenko](https://invent.kde.org/ratijas) and merged after one day.

- **Kate - [Avoid asking the user twice for required actions when closing a window](https://invent.kde.org/utilities/kate/-/merge_requests/1363)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged at creation day.

- **Kate - [Don&#x27;t include katemainwindow.h in kateapp.h](https://invent.kde.org/utilities/kate/-/merge_requests/1362)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 47

- **Kate - [Improve GDBVariableParser](https://invent.kde.org/utilities/kate/-/merge_requests/1351)**<br />
Request authored by [Rémi Peuchot](https://invent.kde.org/remipch) and merged after 13 days.

- **Kate - [Allow DND of toolviews between sidebars](https://invent.kde.org/utilities/kate/-/merge_requests/1361)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [theme contrast: add HTML output format option](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/587)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [code analysis tools improvements](https://invent.kde.org/utilities/kate/-/merge_requests/1360)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [check if screenshot saving worked](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/640)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Port away from deprecated implicit this lambda captures](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/639)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged after one day.

- **Kate - [link to renamed kuserfeedback](https://invent.kde.org/utilities/kate/-/merge_requests/1350)**<br />
Request authored by [Jonathan Riddell](https://invent.kde.org/jriddell) and merged after 10 days.

- **Kate - [GDB plugin cleanup](https://invent.kde.org/utilities/kate/-/merge_requests/1340)**<br />
Request authored by [Rémi Peuchot](https://invent.kde.org/remipch) and merged after 12 days.

- **Kate - [DocManager: Also listen to documentUrlChanged signal](https://invent.kde.org/utilities/kate/-/merge_requests/1358)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Adjust quickopen position to be the same as command bar](https://invent.kde.org/utilities/kate/-/merge_requests/1357)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Adjust to KIO::ListJob API change](https://invent.kde.org/utilities/kate/-/merge_requests/1356)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged at creation day.

- **KTextEditor - [Make completion a normal widget that is floating](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/638)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [add a theme contrast checker](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/586)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

#### Week 46

- **Kate - [handle quickopen like a normal widget](https://invent.kde.org/utilities/kate/-/merge_requests/1355)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Dont include KEncodingProber everywhere](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/637)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Don&#x27;t show kate url bar in kwrite](https://invent.kde.org/utilities/kate/-/merge_requests/1353)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged after one day.

- **KSyntaxHighlighting - [Use explicit lambda captures](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/585)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **KSyntaxHighlighting - [C++: use dsDataType as default style for (Raw) String Literal Prefix](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/584)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 4 days.

- **KTextEditor - [Merge moving*.cpp files into one movingapi.cpp](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/636)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [include katerenderer.h less often](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/635)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [lsp client: enable documentLanguageId for typescript](https://invent.kde.org/utilities/kate/-/merge_requests/1352)**<br />
Request authored by [Leia uwu](https://invent.kde.org/leia) and merged at creation day.

- **Kate - [Use more standard value for layout spacing and margins in diagnostics and search pannel](https://invent.kde.org/utilities/kate/-/merge_requests/1347)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged after 2 days.

- **Kate - [Newest version of the Typescript/Javascript LSP needs the LanguageID set](https://invent.kde.org/utilities/kate/-/merge_requests/1349)**<br />
Request authored by [Lukas Friedrich](https://invent.kde.org/lukasfriedrich) and merged at creation day.

- **Kate - [kateprivate: Use std::vector instead of QList](https://invent.kde.org/utilities/kate/-/merge_requests/1346)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Add new scripts: Duplicate selection, Go to Next / Previous Paragraph](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/630)**<br />
Request authored by [Henri K](https://invent.kde.org/henrik) and merged after 6 days.

#### Week 45

- **Kate - [Switch to a frameless design in kate project info view](https://invent.kde.org/utilities/kate/-/merge_requests/1343)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged after one day.

- **Kate - [Remove some frames and add standard spacing where needed](https://invent.kde.org/utilities/kate/-/merge_requests/1345)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged after one day.

- **Kate - [use window text palette color](https://invent.kde.org/utilities/kate/-/merge_requests/1342)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Declusterify the project info view code analysis](https://invent.kde.org/utilities/kate/-/merge_requests/1344)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged after one day.

- **KTextEditor - [Mark tab widget as frameless](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/634)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged after one day.

- **KSyntaxHighlighting - [PowerShell: fix string and variable substitution ; add highlighting (Number, Control Flow, etc)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/583)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [Ensure that sizeHint is 0 when sidebar is not visible](https://invent.kde.org/utilities/kate/-/merge_requests/1341)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged at creation day.

- **KTextEditor - [line and column limit it 32 big signed, full buffer limit is larger, use size type](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/632)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

- **KSyntaxHighlighting - [MS-DOS Batch: add escape line for multiline command](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/582)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [remove potential allocation in Repository::themeForPalette](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/581)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Fish: fix [...] which are not preceded by a variable or command substitution, command path and multi-line comment](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/580)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Simplify some expression and add noexcept for copy ctor/assignment of State](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/579)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **Kate - [Add separator in to adapt to frameless breeze change](https://invent.kde.org/utilities/kate/-/merge_requests/1286)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged after 80 days.

- **KSyntaxHighlighting - [remove lookup in katepart5](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/578)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

- **Kate - [Add a long comment explaining what checked means.](https://invent.kde.org/utilities/kate/-/merge_requests/1339)**<br />
Request authored by [Albert Astals Cid](https://invent.kde.org/aacid) and merged at creation day.

- **Kate - [Replace QMap with std::map](https://invent.kde.org/utilities/kate/-/merge_requests/1334)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KTextEditor - [KateSearchBar: ignore block mode if selectionOnly option is disabled](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/631)**<br />
Request authored by [Rémi Peuchot](https://invent.kde.org/remipch) and merged after one day.

- **Kate - [GIT_SILENT: Adapt to KConfigGroup name officially being a QString type](https://invent.kde.org/utilities/kate/-/merge_requests/1337)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **Kate - [snapcraft: Don&#x27;t bundle c++ libs.](https://invent.kde.org/utilities/kate/-/merge_requests/1338)**<br />
Request authored by [Scarlett Moore](https://invent.kde.org/scarlettmoore) and merged at creation day.

- **KSyntaxHighlighting - [C++: use &quot;dsOperator&quot; as default style for UDL Numeric &amp; String Suffix](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/567)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after 11 days.

- **KSyntaxHighlighting - [use a cache for dynamic regexp](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/574)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [speedup translating](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/577)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [Load themeData lazily](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/576)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [C++: add &#x27;Raw String Literal Prefix&#x27; style and restricts the style of invalid raw string](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/572)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

#### Week 44

- **KTextEditor - [KateCompletionModel updates](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/628)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [remove group number 0 in captures extracted from rules manipulating a regex](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/573)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Add support of anonymous hyperlink](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/569)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after 4 days.

- **KSyntaxHighlighting - [Add new color theme: VSCodium Dark](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/571)**<br />
Request authored by [Henri K](https://invent.kde.org/henrik) and merged at creation day.

- **Kate - [snapcraft: Fix wayland installs by bundling qtwayland libs.](https://invent.kde.org/utilities/kate/-/merge_requests/1333)**<br />
Request authored by [Scarlett Moore](https://invent.kde.org/scarlettmoore) and merged at creation day.

- **KTextEditor - [More QMap removal](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/627)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Remove/Replace some QMap usages](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/626)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [completion: Replace QList with std::vector in some places](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/624)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Simplify EditorPrivate docs/views storage](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/625)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [hash unification of states](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/568)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 3 days.

- **KSyntaxHighlighting - [Add more Qt macros](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/565)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after 13 days.

