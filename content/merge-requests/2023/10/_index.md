---
title: Merge Requests - October 2023
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2023/10/
---

#### Week 44

- **KTextEditor - [Fix selection shrink when indenting](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/623)**<br />
Request authored by [Rémi Peuchot](https://invent.kde.org/remipch) and merged after 4 days.

#### Week 43

- **Kate - [lspclient: replace QList by std::list if stable references needed](https://invent.kde.org/utilities/kate/-/merge_requests/1332)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **Kate - [Add LSP settings for SCSS, LESS, XML and PHP](https://invent.kde.org/utilities/kate/-/merge_requests/1331)**<br />
Request authored by [Henri K](https://invent.kde.org/henrik) and merged after 2 days.

- **Kate - [Fix run_shell_script default command argument](https://invent.kde.org/utilities/kate/-/merge_requests/1330)**<br />
Request authored by [Rémi Peuchot](https://invent.kde.org/remipch) and merged at creation day.

- **Kate - [GIT_SILENT: use KLocalizedString::setApplicationDomain(QByteArrayLiteral =&gt;...](https://invent.kde.org/utilities/kate/-/merge_requests/1329)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **KTextEditor - [Add missing includes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/622)**<br />
Request authored by [David Edmundson](https://invent.kde.org/davidedmundson) and merged at creation day.

- **kate-editor.org - [Remove reference to Markmail. Change http: protocol to https](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/58)**<br />
Request authored by [Josep M. Ferrer](https://invent.kde.org/jferrer) and merged at creation day.

- **Kate - [Adapt to new kactioncollection api](https://invent.kde.org/utilities/kate/-/merge_requests/1328)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **Kate - [Adapt to change of KStandardAction::name(id) return type](https://invent.kde.org/utilities/kate/-/merge_requests/1327)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after one day.

- **KTextEditor - [Adapt to new api](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/621)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **Kate - [snapcraft: add missig build dep gettext.](https://invent.kde.org/utilities/kate/-/merge_requests/1326)**<br />
Request authored by [Scarlett Moore](https://invent.kde.org/scarlettmoore) and merged at creation day.

#### Week 42

- **Kate - [Add Julia LSP setting](https://invent.kde.org/utilities/kate/-/merge_requests/1324)**<br />
Request authored by [Frederik Banning](https://invent.kde.org/laubblaeser) and merged after 2 days.

- **Kate - [Fix windows build](https://invent.kde.org/utilities/kate/-/merge_requests/1325)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **KSyntaxHighlighting - [best help message for --unbuffered](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/566)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [Adapt to KBookmarkManager API change](https://invent.kde.org/utilities/kate/-/merge_requests/1315)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after 13 days.

- **KTextEditor - [Allow building without rtti](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/620)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [doc: keep a single copy of views](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/619)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **kate-editor.org - [Fix minor issues](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/57)**<br />
Request authored by [Yuri Chornoivan](https://invent.kde.org/yurchor) and merged at creation day.

- **KTextEditor - [There&#x27;s no QVector anymore, QList is the QVector in Qt6](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/618)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **KTextEditor - [Allow enabling/disabling accessibility](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/617)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Small TextBlock Movingrange cache improvements](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/616)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Only use KWindowInfo when building with X11 support](https://invent.kde.org/utilities/kate/-/merge_requests/1323)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **Kate - [Replace QVector with QList](https://invent.kde.org/utilities/kate/-/merge_requests/1322)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Update script used for updating kate-editor-org, to enable i18n for /syntax and /themes pages](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/564)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged after 3 days.

- **kate-editor.org - [Enable i18n for pages](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/56)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged after 3 days.

- **Kate - [Do not popup snippets based on editor text](https://invent.kde.org/utilities/kate/-/merge_requests/1321)**<br />
Request authored by [Gary Li](https://invent.kde.org/li-gary) and merged after one day.

- **Kate - [remove the minimal activities integration](https://invent.kde.org/utilities/kate/-/merge_requests/1317)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [[flatpak] Use Qt5 branch for deps](https://invent.kde.org/utilities/kate/-/merge_requests/1320)**<br />
Request authored by [Julius Künzel](https://invent.kde.org/jlskuz) and merged at creation day.

- **Kate - [lsp: Avoid usb warning about casting half destroyed object](https://invent.kde.org/utilities/kate/-/merge_requests/1318)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Don&#x27;t install empty README.md](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/615)**<br />
Request authored by [Antonio Rojas](https://invent.kde.org/arojas) and merged at creation day.

#### Week 41

- **Kate - [Allow Copy in diagnostic context menu for DiagnosticFixItem](https://invent.kde.org/utilities/kate/-/merge_requests/1316)**<br />
Request authored by [Gary Li](https://invent.kde.org/li-gary) and merged after 3 days.

- **KTextEditor - [emmet/lib.js remove khtml commented out text](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/614)**<br />
Request authored by [Raresh Rus](https://invent.kde.org/nmariusp) and merged at creation day.

- **KSyntaxHighlighting - [autotests quanitfiers -&gt; quantifiers](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/563)**<br />
Request authored by [Raresh Rus](https://invent.kde.org/nmariusp) and merged at creation day.

- **KTextEditor - [add optional speech synthesis support](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/606)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 10 days.

- **KSyntaxHighlighting - [SyntaxHighlighter: pre-compute text format to speed up rendering](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/561)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

#### Week 40

- **KSyntaxHighlighting - [add -U / --unbuffered option for flush on each line with ANSI format](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/560)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KTextEditor - [Fixed KateBuffer::canEncode() never returning false](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/612)**<br />
Request authored by [Jaak Ristioja](https://invent.kde.org/jotik) and merged at creation day.

- **KSyntaxHighlighting - [Optimize HtmlHighlighter](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/557)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

- **KSyntaxHighlighting - [nginx: update for new directives and variables](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/558)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged at creation day.

- **KSyntaxHighlighting - [JSON: highlight invalid escaped characters and invalid numbers](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/542)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 27 days.

- **KTextEditor - [Improve word completion match collection performance](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/611)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Optimize minimap drawing](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/610)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Set mode for more config file endings](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/556)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **KSyntaxHighlighting - [Fix syntaxes that indefinitely stacks contexts instead of going back to a parent context](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/553)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **KSyntaxHighlighting - [Set mode for more config file endings](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/555)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after one day.

#### Week 39

- **KSyntaxHighlighting - [Optimize copy of State](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/551)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 5 days.

- **KSyntaxHighlighting - [avoid calling getSkipOffsetValue() when skipOffsets() is cleared (1.6% faster)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/554)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

