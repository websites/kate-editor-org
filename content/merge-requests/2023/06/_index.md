---
title: Merge Requests - June 2023
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2023/06/
---

#### Week 26

- **KTextEditor - [Add explicit moc includes to sources for moc-covered headers](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/573)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KSyntaxHighlighting - [Add explicit moc includes to sources for moc-covered headers](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/491)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **Kate - [Handle non local file in Diagnostics view](https://invent.kde.org/utilities/kate/-/merge_requests/1254)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after one day.

- **Kate - [Port away from deprecated KUrlRequester::setFilter](https://invent.kde.org/utilities/kate/-/merge_requests/1253)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after 3 days.

- **Kate - [Add DAP configuration for Dart and Flutter](https://invent.kde.org/utilities/kate/-/merge_requests/1243)**<br />
Request authored by [Bart Ribbers](https://invent.kde.org/bribbers) and merged after 12 days.

- **Kate - [Allow opening new tab in front of currently active tab optionally](https://invent.kde.org/utilities/kate/-/merge_requests/1249)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 6 days.

#### Week 25

- **Kate - [Improve code action invocation with no selection](https://invent.kde.org/utilities/kate/-/merge_requests/1252)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Fix project name not updated in combo after .kateproject change](https://invent.kde.org/utilities/kate/-/merge_requests/1251)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [cmake.xml: recognize sub-commands of `$&lt;LIST:…&gt;` and `$&lt;PATH:…&gt;` genex](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/490)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after 2 days.

- **Kate - [Fix LSP inlay hint deserialization](https://invent.kde.org/utilities/kate/-/merge_requests/1250)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [lsp: Use label as insertText as a last resort](https://invent.kde.org/utilities/kate/-/merge_requests/1248)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [cmake.xml: updates for CMake 3.27](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/489)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

#### Week 24

- **Kate - [reply to workspace/semanticTokens/refresh](https://invent.kde.org/utilities/kate/-/merge_requests/1246)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [use a different font config key to avoid config corruption qt5/6](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/572)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [Add heex to elixir extensions list](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/488)**<br />
Request authored by [Gabriel Morell-Pacheco](https://invent.kde.org/gmorellp) and merged after 2 days.

- **kate-editor.org - [Update link in old plugin tutorial](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/55)**<br />
Request authored by [Jake Leahy](https://invent.kde.org/amadan) and merged at creation day.

- **Kate - [DiagnosticView: remove gui client from KXMLGuiFactory in the dtor](https://invent.kde.org/utilities/kate/-/merge_requests/1245)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **Kate - [Pause the new message animation when not visible](https://invent.kde.org/utilities/kate/-/merge_requests/1244)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged after one day.

- **Kate - [kateviewmanager: Show welcome page as for new windows when all tabs closed](https://invent.kde.org/utilities/kate/-/merge_requests/1239)**<br />
Request authored by [Oliver Beard](https://invent.kde.org/olib) and merged after 2 days.

- **Kate - [Add clear button to input fields in config dialog and output view](https://invent.kde.org/utilities/kate/-/merge_requests/1238)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after one day.

- **Kate - [Fix qt6 build with pch](https://invent.kde.org/utilities/kate/-/merge_requests/1240)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Rename variables to fix two cppcheck warnings](https://invent.kde.org/utilities/kate/-/merge_requests/1241)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged at creation day.

- **Kate - [Fix various framework checks for KF6](https://invent.kde.org/utilities/kate/-/merge_requests/1242)**<br />
Request authored by [Aaron Dewes](https://invent.kde.org/aarondewes) and merged at creation day.

#### Week 23

- **KSyntaxHighlighting - [Highlight QML pragma keyword](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/487)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **KSyntaxHighlighting - [Highlight QML pragma keyword](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/486)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **Kate - [Fix dependency on KF5NewStuff for project addon](https://invent.kde.org/utilities/kate/-/merge_requests/1237)**<br />
Request authored by [Aaron Dewes](https://invent.kde.org/aarondewes) and merged at creation day.

- **Kate - [ensure KTextEditor plugins separation between kf5/6](https://invent.kde.org/utilities/kate/-/merge_requests/1236)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 5 days.

- **Kate - [Fix loading konsolepart in Qt6](https://invent.kde.org/utilities/kate/-/merge_requests/1235)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after 5 days.

- **KTextEditor - [emulatedcommandbar QList::first() on temporary fix](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/569)**<br />
Request authored by [Shreevathsa V M](https://invent.kde.org/shreevathsavm) and merged at creation day.

#### Week 22

- **Kate - [Do not change diagnostics filter after building with errors](https://invent.kde.org/utilities/kate/-/merge_requests/1233)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after 4 days.

- **Kate - [diagnostics: add action and shortcut to clear diagnostics filtering](https://invent.kde.org/utilities/kate/-/merge_requests/1234)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 2 days.

- **KTextEditor - [Introduce: Kate::TextBlock::blockSize() and offsetToCursor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/565)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Remove blockForIndex](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/568)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Remove some useless alias functions](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/567)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Reuse Kate::TextLine instance when inserting/removing](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/566)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

