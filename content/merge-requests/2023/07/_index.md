---
title: Merge Requests - July 2023
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2023/07/
---

#### Week 31

- **Kate - [Improve diagnostics adding performance](https://invent.kde.org/utilities/kate/-/merge_requests/1271)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [systemd unit: fix option group assignments](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/517)**<br />
Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr) and merged at creation day.

#### Week 30

- **Kate - [handle tab deletion/creation](https://invent.kde.org/utilities/kate/-/merge_requests/1270)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [systemd unit: update to systemd v254](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/516)**<br />
Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr) and merged at creation day.

- **Kate - [drop more tools](https://invent.kde.org/utilities/kate/-/merge_requests/1269)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [UrlInfo: Pass current working directory to QUrl::fromUserInput](https://invent.kde.org/utilities/kate/-/merge_requests/1268)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged after one day.

- **Kate - [Diagnostics output optional for the build-plugin](https://invent.kde.org/utilities/kate/-/merge_requests/1267)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

- **KTextEditor - [It was renamed as Q_RELOCATABLE_TYPE](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/584)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged after 2 days.

- **KSyntaxHighlighting - [Q_MOVABLE_TYPE was renamed to Q_RELOCATABLE_TYPE](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/515)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged after one day.

- **KTextEditor - [Drop unneeded Qt metatype declarations](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/583)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after one day.

- **KSyntaxHighlighting - [Hare language syntax highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/514)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after one day.

- **Kate - [Added feature to synchronise scrolling between split views](https://invent.kde.org/utilities/kate/-/merge_requests/1203)**<br />
Request authored by [Ulterno Ω*](https://invent.kde.org/ulterno) and merged after 93 days.

- **Kate - [diagnostics view imporvements](https://invent.kde.org/utilities/kate/-/merge_requests/1266)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Markdown: add Table, Emoji and Highlight Text](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/511)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 7 days.

- **KSyntaxHighlighting - [Fix #25: kateschema to theme converter](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/504)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 11 days.

- **KSyntaxHighlighting - [Add syntax highlighting for Twig](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/512)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

#### Week 29

- **Kate - [diagnostics: adjust line number display](https://invent.kde.org/utilities/kate/-/merge_requests/1265)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **Kate - [Remove Designer&#x27;s &quot;.&quot; normaloff file data from icon properties in .ui files](https://invent.kde.org/utilities/kate/-/merge_requests/1264)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged after 2 days.

- **KTextEditor - [Drop icon properties in UI files with Designer&#x27;s broken normaloff injection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/582)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after one day.

- **Kate - [lsp: Dont request diagnostic fixes for full doc range as fallback](https://invent.kde.org/utilities/kate/-/merge_requests/1262)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [diagnostics: Remove items in bulk instead of one at a time](https://invent.kde.org/utilities/kate/-/merge_requests/1263)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [lsp: always honor server when using Completion.textEdit.newText](https://invent.kde.org/utilities/kate/-/merge_requests/1261)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Use TextBlock::blockSize to preallocate space in text()](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/581)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Document: Add cursorToOffset/offsetToCursor api](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/578)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 5 days.

- **KSyntaxHighlighting - [add Klipper Config and Klipper G-Code](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/510)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [HTML: add Element Symbols and Attribute Separator (as xml)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/509)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

#### Week 28

- **KTextEditor - [Cleanup unused qOverload statements, port to KIO::stat job](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/580)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **Kate - [Add open action to tabbar context menu](https://invent.kde.org/utilities/kate/-/merge_requests/1260)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Java Module must be higher priority than Java](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/507)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Log File: add *.log.*, syslog and syslog.* extensions](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/508)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [R: add numeric suffix (i and L)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/506)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Java: add keywords, text block and java module language (#24)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/505)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [ADA: fix folding on &quot;null record&quot; ; add digit separator, based numeral and keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/503)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KTextEditor - [Document: Remove setMarkPixmap api](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/579)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [lsp: Adjust completion range to replace based on server data](https://invent.kde.org/utilities/kate/-/merge_requests/1258)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [QML: fix comments in property](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/502)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Makefile: add multi-line target, target variable value, target separator and...](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/500)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [Kotlin: fix Comment and Annotation ; add keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/501)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KTextEditor - [completion: Fix only start chars of items are matched](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/577)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [LSP InlayHint fixes](https://invent.kde.org/utilities/kate/-/merge_requests/1257)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KTextEditor - [Add scrollbar getter api to KTextEditor::View](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/575)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 6 days.

- **KSyntaxHighlighting - [Bash, Zsh: fix arguments after a heredoc](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/498)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [Makefile: &quot;if&quot; is NOT &quot;gmake_if_keywords&quot;](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/499)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

#### Week 27

- **KSyntaxHighlighting - [JS, TS: add Private Member Operator style and some optimizations](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/497)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [CSS: add values, units and pseudo-classes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/496)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [C++: new C++23 escape sequence, fix comments and # in &quot;namespace identifier&quot; and attribute](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/495)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [PHP: add functions and constants of 8.2](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/493)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Bash: add U, u, L, K for parameter transformation](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/494)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Python: upgrade f-string format, add special functions and fix \ preceded by a string literal](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/492)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [Fix unused variable warnings](https://invent.kde.org/utilities/kate/-/merge_requests/1256)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [Add explicit moc includes to sources for moc-covered headers](https://invent.kde.org/utilities/kate/-/merge_requests/1255)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KTextEditor - [Merge unregisterVariableMatch and unregisterVariablePrefix](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/576)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [avoid config syncs and updates on initial construction](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/574)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

