---
title: Merge Requests - September 2019
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2019/09/
---

#### Week 39

- **Kate - [fix hi-dpi rendering of tab buttons](https://invent.kde.org/utilities/kate/-/merge_requests/32)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Compile with -DQT_DISABLE_DEPRECATED_BEFORE=0x050d00](https://invent.kde.org/utilities/kate/-/merge_requests/30)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [Disalow Qt&#x27;s Q_FOREACH/foreach macros](https://invent.kde.org/utilities/kate/-/merge_requests/29)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [LSP Client config page: Use ui file](https://invent.kde.org/utilities/kate/-/merge_requests/28)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [External Tools: Support adding actions to toolbar](https://invent.kde.org/utilities/kate/-/merge_requests/23)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [External Tools: Extend output to include &quot;Copy to Clipboard&quot;](https://invent.kde.org/utilities/kate/-/merge_requests/27)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [Properly translate the external tools menu + config widget](https://invent.kde.org/utilities/kate/-/merge_requests/26)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [Add Tools &gt; External Tools &gt; Configure...](https://invent.kde.org/utilities/kate/-/merge_requests/25)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [Implement KTextEditor::MainWindow::showPluginConfigPage()](https://invent.kde.org/utilities/kate/-/merge_requests/24)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

#### Week 38

- **Kate - [Remove deprecated plugins](https://invent.kde.org/utilities/kate/-/merge_requests/16)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after 10 days.

- **Kate - [build-plugin: handle and process ninja stdout](https://invent.kde.org/utilities/kate/-/merge_requests/22)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **kate-editor.org - [Add a script to extract/merge translations](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/6)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged after 2 days.

- **Kate - [reformat: also format source files](https://invent.kde.org/utilities/kate/-/merge_requests/20)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

#### Week 37

- **Kate - [tabswitcher: switch to next-in-line doc when top document is closed](https://invent.kde.org/utilities/kate/-/merge_requests/18)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **Kate - [Added a line to set default sort order for the Symbolviewer.](https://invent.kde.org/utilities/kate/-/merge_requests/17)**<br />
Request authored by [Tore Havn](https://invent.kde.org/torehavn) and merged after one day.

- **Kate - [Revive externaltools plugin](https://invent.kde.org/utilities/kate/-/merge_requests/15)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

#### Week 36

- **Kate - [search: make item margin configurable](https://invent.kde.org/utilities/kate/-/merge_requests/12)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 2 days.

- **Kate - [Avoid using `size` for checking emptiness](https://invent.kde.org/utilities/kate/-/merge_requests/13)**<br />
Request authored by [Filip Gawin](https://invent.kde.org/gawin) and merged at creation day.

- **Kate - [Cleanup inheritance specifiers](https://invent.kde.org/utilities/kate/-/merge_requests/11)**<br />
Request authored by [Filip Gawin](https://invent.kde.org/gawin) and merged after one day.

- **kate-editor.org - [Syntax Page: add generated PHP syntax](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/5)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 7 days.

