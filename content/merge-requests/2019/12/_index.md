---
title: Merge Requests - December 2019
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2019/12/
---

#### Week 52

- **Kate - [Add icons to metadata of plugins supporting KDevelop/Plugin](https://invent.kde.org/utilities/kate/-/merge_requests/53)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after 10 days.

#### Week 51

- **Kate - [LSP: add support for semantic highlighting](https://invent.kde.org/utilities/kate/-/merge_requests/47)**<br />
Request authored by [Milian Wolff](https://invent.kde.org/mwolff) and merged after 23 days.

#### Week 50

- **Kate - [doc: extend lspclient documentation](https://invent.kde.org/utilities/kate/-/merge_requests/52)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 7 days.

#### Week 49

- **Kate - [[lspclient addon] Replace KRecursiveFilterProxyModel with QSortFilterProxyModel](https://invent.kde.org/utilities/kate/-/merge_requests/49)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 2 days.

- **Kate - [Fix: Do not translate KActionCollection identifier](https://invent.kde.org/utilities/kate/-/merge_requests/50)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [Fix translation of external tools](https://invent.kde.org/utilities/kate/-/merge_requests/51)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [[project addon] Don&#x27;t use deprecated KRecursiveFilterProxyModel](https://invent.kde.org/utilities/kate/-/merge_requests/48)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after one day.

