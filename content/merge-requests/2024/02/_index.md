---
title: Merge Requests - February 2024
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2024/02/
---

#### Week 09

- **Kate - [Fix the saving of the run commands for project targets](https://invent.kde.org/utilities/kate/-/merge_requests/1415)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 4 days.

- **Kate - [[CI/CD] Add macOS jobs](https://invent.kde.org/utilities/kate/-/merge_requests/1393)**<br />
Request authored by [Julius Künzel](https://invent.kde.org/jlskuz) and merged after 30 days.

- **KTextEditor - [Optimize a mark-restoring condition after document reload](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/671)**<br />
Request authored by [Igor Kushnir](https://invent.kde.org/igorkushnir) and merged at creation day.

- **KSyntaxHighlighting - [Fix implicit size -&gt; int conversion](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/601)**<br />
Request authored by [Orgad Shaneh](https://invent.kde.org/orgads) and merged after one day.

#### Week 08

- **Kate - [lsp client: close tabs with mouse middle button](https://invent.kde.org/utilities/kate/-/merge_requests/1411)**<br />
Request authored by [Leia uwu](https://invent.kde.org/leia) and merged after 3 days.

- **Kate - [tabswitcher: Do not emit if we&#x27;re adding no documents](https://invent.kde.org/utilities/kate/-/merge_requests/1414)**<br />
Request authored by [Aleix Pol Gonzalez](https://invent.kde.org/apol) and merged after one day.

- **Kate - [snapcraft: Fix stage files, add ibus, fcitx5 support, add konsole stage snap.](https://invent.kde.org/utilities/kate/-/merge_requests/1412)**<br />
Request authored by [Scarlett Moore](https://invent.kde.org/scarlettmoore) and merged after 2 days.

- **Kate - [Add a new tab if Ctrl is pressed when search is invoked](https://invent.kde.org/utilities/kate/-/merge_requests/1409)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 2 days.

- **Kate - [Port to stable MainWindow widget api](https://invent.kde.org/utilities/kate/-/merge_requests/1410)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix tabswitcher performance when large of docs are opened](https://invent.kde.org/utilities/kate/-/merge_requests/1408)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Avoid many proxy invalidations on close](https://invent.kde.org/utilities/kate/-/merge_requests/1407)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [build Qt6 based Appx](https://invent.kde.org/utilities/kate/-/merge_requests/1405)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 3 days.

- **KTextEditor - [add widgetAdded and widgetRemoved](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/670)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [add setRangeWithAttribute](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/668)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KTextEditor - [add widgets api](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/669)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [better handling of ctags process starting](https://invent.kde.org/utilities/kate/-/merge_requests/1406)**<br />
Request authored by [Victor Wollesen](https://invent.kde.org/altrus) and merged at creation day.

#### Week 07

- **Kate - [Added action to restore previously closed tab(s)](https://invent.kde.org/utilities/kate/-/merge_requests/1398)**<br />
Request authored by [Ben Gooding](https://invent.kde.org/bengooding) and merged after 13 days.

- **KTextEditor - [split lines early](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/663)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 13 days.

- **KSyntaxHighlighting - [OCaml: update to 5.x](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/600)**<br />
Request authored by [Edwin Török](https://invent.kde.org/edwintorok) and merged after one day.

- **Kate - [quickopen: Handle reslectFirst with filtering](https://invent.kde.org/utilities/kate/-/merge_requests/1404)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Documents: Fix row numbers not updated after dnd](https://invent.kde.org/utilities/kate/-/merge_requests/1403)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix selection rendering with custom line height](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/667)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

#### Week 06

- **Kate - [build plugin: make sure the selected target stays visible when changing the filter](https://invent.kde.org/utilities/kate/-/merge_requests/1401)**<br />
Request authored by [Alexander Neundorf](https://invent.kde.org/neundorf) and merged after one day.

- **Kate - [snapcraft: Add qtwayland5 platform plugin.](https://invent.kde.org/utilities/kate/-/merge_requests/1400)**<br />
Request authored by [Scarlett Moore](https://invent.kde.org/scarlettmoore) and merged after 3 days.

- **KTextEditor - [Plugin template: prepare for KF 6.0 release](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/666)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **Kate - [Documents: Allow closing docs with middle click optionally](https://invent.kde.org/utilities/kate/-/merge_requests/1399)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Allow configuring diagnostics limit](https://invent.kde.org/utilities/kate/-/merge_requests/1397)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Fix dyn word wrap indicator validation check](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/664)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [util-scripts: Use setText when working with whole document](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/665)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 05

- **Kate - [Remove not-really-optional dependencies from plugins](https://invent.kde.org/utilities/kate/-/merge_requests/1395)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

