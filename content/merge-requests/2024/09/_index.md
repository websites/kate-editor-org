---
title: Merge Requests - September 2024
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2024/09/
---

#### Week 40

- **Kate - [dap: Add support for flutter hot reload/restart](https://invent.kde.org/utilities/kate/-/merge_requests/1606)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [dap: Pass all fields from launch.json to the server](https://invent.kde.org/utilities/kate/-/merge_requests/1607)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 39

- **Kate - [Openlink: detect local file paths](https://invent.kde.org/utilities/kate/-/merge_requests/1604)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [lsp-inlay-hints improvements](https://invent.kde.org/utilities/kate/-/merge_requests/1605)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Read dir kateconfig on view creation](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/733)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KTextEditor - [Fix pressing &lt;ENTER&gt; on } inserts two lines instead of one](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/735)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [project: Emit error if the relevant vcs wasn&#x27;t found when loading](https://invent.kde.org/utilities/kate/-/merge_requests/1603)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add &#x27;reopen latest closed documents&#x27; to tab context menu](https://invent.kde.org/utilities/kate/-/merge_requests/1600)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Show relevant external tools in context menu](https://invent.kde.org/utilities/kate/-/merge_requests/1602)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Load MDI ToolView shortcuts as custom to avoid unintentional reset](https://invent.kde.org/utilities/kate/-/merge_requests/1596)**<br />
Request authored by [John Kizer](https://invent.kde.org/johnandmegh) and merged after one day.

- **Kate - [dap: When stop button is pressed, stop the debugging](https://invent.kde.org/utilities/kate/-/merge_requests/1599)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Dont remove trailing spaces in markdown by default](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/734)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Multicursor: Fix indent with multiple cursors](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/724)**<br />
Request authored by [Theresa Brauch](https://invent.kde.org/tgier) and merged after 18 days.

- **Kate - [diag: Add action to show hover info](https://invent.kde.org/utilities/kate/-/merge_requests/1601)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Allow for null ID in LSP response messages](https://invent.kde.org/utilities/kate/-/merge_requests/1598)**<br />
Request authored by [Jake Leahy](https://invent.kde.org/amadan) and merged at creation day.

- **Kate - [dap: Add support for reading targets from launch.json](https://invent.kde.org/utilities/kate/-/merge_requests/1594)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [tooltip: Dont hide if hovering over the same word](https://invent.kde.org/utilities/kate/-/merge_requests/1595)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Update dart &amp; rust lsp settings](https://invent.kde.org/utilities/kate/-/merge_requests/1592)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix expected separator for kate_mdi shortcuts](https://invent.kde.org/utilities/kate/-/merge_requests/1593)**<br />
Request authored by [John Kizer](https://invent.kde.org/johnandmegh) and merged at creation day.

- **Kate - [Dont hide the tooltip if the user is moving the mouse cursor towards the tooltip](https://invent.kde.org/utilities/kate/-/merge_requests/1563)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 34 days.

- **Kate - [dap improvements and fixes](https://invent.kde.org/utilities/kate/-/merge_requests/1590)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Make debug plugin compile on windows](https://invent.kde.org/utilities/kate/-/merge_requests/1591)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [snapcraft: Add paths for libs in core22 to ld_library_path](https://invent.kde.org/utilities/kate/-/merge_requests/1588)**<br />
Request authored by [Scarlett Moore](https://invent.kde.org/scarlettmoore) and merged after 2 days.

#### Week 38

- **Kate - [Add copyright headers to new files from #1557](https://invent.kde.org/utilities/kate/-/merge_requests/1587)**<br />
Request authored by [Karthik Nishanth](https://invent.kde.org/nishanthkarthik) and merged after one day.

- **KSyntaxHighlighting - [Swift: fix detection of end of protocol method declaration](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/663)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [More check with indexer](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/662)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

- **Kate - [Text hint improvements](https://invent.kde.org/utilities/kate/-/merge_requests/1557)**<br />
Request authored by [Karthik Nishanth](https://invent.kde.org/nishanthkarthik) and merged after 46 days.

- **Kate - [snapcraft: Ensure $SNAP path is first in LD_LIBRARY_PATH](https://invent.kde.org/utilities/kate/-/merge_requests/1586)**<br />
Request authored by [Scarlett Moore](https://invent.kde.org/scarlettmoore) and merged at creation day.

- **Kate - [Try to detect if untracked document belongs to the project](https://invent.kde.org/utilities/kate/-/merge_requests/1584)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [snapcraft: Patchelf dri files so we have graphics.](https://invent.kde.org/utilities/kate/-/merge_requests/1585)**<br />
Request authored by [Scarlett Moore](https://invent.kde.org/scarlettmoore) and merged at creation day.

- **KSyntaxHighlighting - [simplify xml syntax files before integrating them into the executable to speed up reading](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/643)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 32 days.

- **KSyntaxHighlighting - [orgmode.xml: Fix orgmode syntax highlighting not ending properly](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/661)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after one day.

- **KTextEditor - [avoid double signal emission](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/732)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [Ruby: fix %W, dot member, some parenthesis ; add ?c, escape char, etc](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/657)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 10 days.

- **Kate - [Fix recursive directory traversal and remove file skipping](https://invent.kde.org/utilities/kate/-/merge_requests/1583)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [lsp: Use the resolved completion item](https://invent.kde.org/utilities/kate/-/merge_requests/1582)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Remove MovingRange caching in TextBlock](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/730)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

#### Week 37

- **Kate - [Use documentation from completionItem/resolve](https://invent.kde.org/utilities/kate/-/merge_requests/1460)**<br />
Request authored by [Quinten Kock](https://invent.kde.org/colonelphantom) and merged after 153 days.

- **Kate - [lspclient: check for capability before using documentSymbol](https://invent.kde.org/utilities/kate/-/merge_requests/1581)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **KSyntaxHighlighting - [Gleam: Minor modifications to syntax and example file](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/660)**<br />
Request authored by [Louis Guichard](https://invent.kde.org/glpda) and merged at creation day.

- **KTextEditor - [Optimize killLine for multiple cursors](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/728)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [completion: Allow async population of documentation](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/727)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Fix text insertion with multiple cursors at same position.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/722)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **KTextEditor - [Add command names for &quot;Remove Spaces&quot; and &quot;Keep Extra Spaces&quot;](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/723)**<br />
Request authored by [Joshua Goins](https://invent.kde.org/redstrate) and merged after 3 days.

- **KTextEditor - [Minimap now follows the theme also for search matches](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/726)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

- **Kate - [fix strange behavior on document close after restore](https://invent.kde.org/utilities/kate/-/merge_requests/1580)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [fix session restore of tabs/views of untitled documents](https://invent.kde.org/utilities/kate/-/merge_requests/1578)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [Remove truncase from Common Lisp](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/659)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [snapcraft: Fix typo..](https://invent.kde.org/utilities/kate/-/merge_requests/1579)**<br />
Request authored by [Scarlett Moore](https://invent.kde.org/scarlettmoore) and merged at creation day.

- **Kate - [snapcraft: Actually add the neon packages.](https://invent.kde.org/utilities/kate/-/merge_requests/1577)**<br />
Request authored by [Scarlett Moore](https://invent.kde.org/scarlettmoore) and merged at creation day.

#### Week 36

- **Kate - [Persist viewstate across project reloads](https://invent.kde.org/utilities/kate/-/merge_requests/1576)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Make the code analysis tools header only](https://invent.kde.org/utilities/kate/-/merge_requests/1575)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [snapcraft: Use neon packages to simplify build and runtime.](https://invent.kde.org/utilities/kate/-/merge_requests/1571)**<br />
Request authored by [Scarlett Moore](https://invent.kde.org/scarlettmoore) and merged after one day.

- **Kate - [Fix compilation error with Qt 6.8.0 and g++ 14.2.1_p20240817](https://invent.kde.org/utilities/kate/-/merge_requests/1574)**<br />
Request authored by [Aidan Harris](https://invent.kde.org/aidanharris) and merged at creation day.

- **Kate - [SemanticHighlighting: Only highlight visible range](https://invent.kde.org/utilities/kate/-/merge_requests/1572)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [libkateprivate: Cleanup headers to improve compile times](https://invent.kde.org/utilities/kate/-/merge_requests/1573)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [snapcraft: New upstream release. Refresh prime cleanup.](https://invent.kde.org/utilities/kate/-/merge_requests/1570)**<br />
Request authored by [Scarlett Moore](https://invent.kde.org/scarlettmoore) and merged at creation day.

- **KTextEditor - [Make tests a bit faster](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/721)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

