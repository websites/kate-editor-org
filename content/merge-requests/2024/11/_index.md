---
title: Merge Requests - November 2024
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2024/11/
---

#### Week 48

- **KTextEditor - [Redesign stuff related to Kate::TextBuffer::m_multilineRanges](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/761)**<br />
Request authored by [BZZZZ DZZZZ](https://invent.kde.org/bzzzz) and merged after 3 days.

- **Kate - [Add back distance check to hide tooltip](https://invent.kde.org/utilities/kate/-/merge_requests/1661)**<br />
Request authored by [Leia uwu](https://invent.kde.org/leia) and merged after one day.

- **Kate - [Improve tooltip positioning logic](https://invent.kde.org/utilities/kate/-/merge_requests/1660)**<br />
Request authored by [Leia uwu](https://invent.kde.org/leia) and merged after one day.

- **Kate - [Add back distance check to hide tooltip](https://invent.kde.org/utilities/kate/-/merge_requests/1658)**<br />
Request authored by [Leia uwu](https://invent.kde.org/leia) and merged after one day.

- **Kate - [Fix #496460: Match export not working with lookarounds](https://invent.kde.org/utilities/kate/-/merge_requests/1657)**<br />
Request authored by [Markus Ebner](https://invent.kde.org/mebner) and merged after 3 days.

- **Kate - [don&#x27;t die if the arrays not only contain integers](https://invent.kde.org/utilities/kate/-/merge_requests/1659)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [Improve odin lang highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/671)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 47

- **Kate - [Add multi project support to build plugin](https://invent.kde.org/utilities/kate/-/merge_requests/1653)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **KTextEditor - [avoid closeUrl() call](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/760)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Advertise ourselves a bit better](https://invent.kde.org/utilities/kate/-/merge_requests/1656)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Clear all references/uses of aboutToDeleteMovingInterfaceContent](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/759)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Avoid ctag indexing the home/root dir](https://invent.kde.org/utilities/kate/-/merge_requests/1654)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix deprecation warnings](https://invent.kde.org/utilities/kate/-/merge_requests/1655)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 46

- **Kate - [Try to have a PATH on macos when not launched from terminal](https://invent.kde.org/utilities/kate/-/merge_requests/1621)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 33 days.

- **KSyntaxHighlighting - [Bump KF and QT versions in ecm_set_disabled_deprecation_versions](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/670)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **KTextEditor - [Align completion with the word being completed](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/757)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Allow to disable auto lsp completions](https://invent.kde.org/utilities/kate/-/merge_requests/1652)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Use a QLabel for scrollbar linenumbers tooltip](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/755)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Add functions for jumping to next/prev blank line](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/756)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Make compile without deprecated methods](https://invent.kde.org/utilities/kate/-/merge_requests/1651)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **Kate - [[24.12] Add QIcon include](https://invent.kde.org/utilities/kate/-/merge_requests/1650)**<br />
Request authored by [Sam James](https://invent.kde.org/thesamesam) and merged at creation day.

- **Kate - [Not necessary to add Qt6 suffix now](https://invent.kde.org/utilities/kate/-/merge_requests/1649)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged after one day.

- **KTextEditor - [Disable ENABLE_KAUTH_DEFAULT on Haiku also](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/754)**<br />
Request authored by [Luc Schrijvers](https://invent.kde.org/begasus) and merged at creation day.

- **Kate - [Build output improvements](https://invent.kde.org/utilities/kate/-/merge_requests/1646)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

#### Week 45

- **Kate - [Rename as KF6 :)](https://invent.kde.org/utilities/kate/-/merge_requests/1647)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **Kate - [Remove warning about not existing directory](https://invent.kde.org/utilities/kate/-/merge_requests/1648)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **KSyntaxHighlighting - [cmake.xml: updates for the recently released CMake 3.31](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/669)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after 2 days.

- **Kate - [Fix snippets not working unless completion is hard invoked](https://invent.kde.org/utilities/kate/-/merge_requests/1644)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [replace all map/unordered_map with vector in katemdi](https://invent.kde.org/utilities/kate/-/merge_requests/1643)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [Build: Append html output](https://invent.kde.org/utilities/kate/-/merge_requests/1645)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Fix crash if feedback or dyn attr is cleared before deletion](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/753)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Remove misleading dead code](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/752)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Add config option for tab cycling](https://invent.kde.org/utilities/kate/-/merge_requests/1642)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Store toolview data in one place in a vector](https://invent.kde.org/utilities/kate/-/merge_requests/1641)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [buffer: Remove m_invalidCursors](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/737)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 37 days.

#### Week 44

- **Kate - [Fix build with Qt 6.9 (dev)](https://invent.kde.org/utilities/kate/-/merge_requests/1638)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged after 2 days.

- **KTextEditor - [Vi mode: Don&#x27;t infinite loop in searcher](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/751)**<br />
Request authored by [Sune Vuorela](https://invent.kde.org/sune) and merged after 4 days.

- **Kate - [Use pch for tests](https://invent.kde.org/utilities/kate/-/merge_requests/1640)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

