---
title: Merge Requests - April 2024
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2024/04/
---

#### Week 18

- **Kate - [a11y: Set buddies and improve tab order for config pages](https://invent.kde.org/utilities/kate/-/merge_requests/1476)**<br />
Request authored by [Michael Weghorn](https://invent.kde.org/michaelweghorn) and merged after one day.

- **Kate - [keep track of recent used files on saveAs &amp; close](https://invent.kde.org/utilities/kate/-/merge_requests/1475)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [start to make dbus optional](https://invent.kde.org/utilities/kate/-/merge_requests/1432)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 36 days.

#### Week 17

- **KTextEditor - [Fix performance with many cursors in a large line](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/682)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [a11y: Set &quot;Line Height Multiplier&quot; buddy in &quot;Appearance&quot; -&gt; &quot;General&quot; tab, improve tab order in &quot;Appearance&quot; -&gt; &quot;Borders&quot; tab page](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/680)**<br />
Request authored by [Michael Weghorn](https://invent.kde.org/michaelweghorn) and merged after 5 days.

- **KTextEditor - [Fix broken navigation in completion widget with multiple views](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/681)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KSyntaxHighlighting - [Support single-quoted strings in MapCSS](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/609)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **Kate - [use kate from the right virtual desktop](https://invent.kde.org/utilities/kate/-/merge_requests/1474)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Fix build under Windows](https://invent.kde.org/utilities/kate/-/merge_requests/1473)**<br />
Request authored by [Gary Wang](https://invent.kde.org/garywang) and merged at creation day.

- **Kate - [Terminal plugin feature: keep one tab per directory we have a document for](https://invent.kde.org/utilities/kate/-/merge_requests/1463)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 7 days.

- **Kate - [Reduce Q_OBJECT usage in some plugins](https://invent.kde.org/utilities/kate/-/merge_requests/1471)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [These calls are officially available in KTextEditor::MainWindow](https://invent.kde.org/utilities/kate/-/merge_requests/1470)**<br />
Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry) and merged after one day.

#### Week 16

- **Kate - [S&amp;R: Improve tab handling](https://invent.kde.org/utilities/kate/-/merge_requests/1465)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 2 days.

- **Kate - [Fix external tools get lost on modification](https://invent.kde.org/utilities/kate/-/merge_requests/1468)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Store path directly in KateProjectItem instead of using setData](https://invent.kde.org/utilities/kate/-/merge_requests/1464)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [kateconfigdialog: Change new tab placement text](https://invent.kde.org/utilities/kate/-/merge_requests/1467)**<br />
Request authored by [Oliver Beard](https://invent.kde.org/olib) and merged at creation day.

- **Kate - [Add support for ruff formatter](https://invent.kde.org/utilities/kate/-/merge_requests/1461)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [project: Add ruff code analysis tool](https://invent.kde.org/utilities/kate/-/merge_requests/1462)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Port deprecated QCheckBox Qt6.7 method (QCheckBox::stateChanged-&gt;QCheckBox::checkStateChanged)](https://invent.kde.org/utilities/kate/-/merge_requests/1458)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **Kate - [project: Add html tidy analysis tool](https://invent.kde.org/utilities/kate/-/merge_requests/1459)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 15

- **Kate - [Add default Vue LSP config](https://invent.kde.org/utilities/kate/-/merge_requests/1457)**<br />
Request authored by [James Zuccon](https://invent.kde.org/jamesz) and merged at creation day.

- **Kate - [add more lsp server settings](https://invent.kde.org/utilities/kate/-/merge_requests/1456)**<br />
Request authored by [Carl nobody](https://invent.kde.org/blitter) and merged at creation day.

- **KSyntaxHighlighting - [Add Syntax Highlighting for Vue Template Files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/608)**<br />
Request authored by [James Zuccon](https://invent.kde.org/jamesz) and merged after one day.

- **Kate - [kateprojectinfoviewindex: Restore Q_OBJECT macro](https://invent.kde.org/utilities/kate/-/merge_requests/1453)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged at creation day.

- **Kate - [Reduce moc usage in project plugin](https://invent.kde.org/utilities/kate/-/merge_requests/1451)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Sessions: Update jump action list on shutdown](https://invent.kde.org/utilities/kate/-/merge_requests/1450)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

#### Week 14

- **Kate - [KWrite: Add more hamburger actions](https://invent.kde.org/utilities/kate/-/merge_requests/1445)**<br />
Request authored by [Nathan Garside](https://invent.kde.org/ngarside) and merged after one day.

- **Kate - [Reduce moc features usage in libkateprivate](https://invent.kde.org/utilities/kate/-/merge_requests/1449)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Remove unnecessary usage of old style connect](https://invent.kde.org/utilities/kate/-/merge_requests/1448)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [show all toplevel menu entries below the curated stuff](https://invent.kde.org/utilities/kate/-/merge_requests/1446)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Fix tooltip hides on trying to scroll using scrollbar](https://invent.kde.org/utilities/kate/-/merge_requests/1447)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [fix textInsertedRange signal for insertText behind last line](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/678)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [project plugin: when creating a project from a directory, skip some files](https://invent.kde.org/utilities/kate/-/merge_requests/1416)**<br />
Request authored by [Alexander Neundorf](https://invent.kde.org/neundorf) and merged after 38 days.

- **Kate - [Port some old style signal/slot connections](https://invent.kde.org/utilities/kate/-/merge_requests/1443)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Use qEnvironmentVariable instead of qgetenv](https://invent.kde.org/utilities/kate/-/merge_requests/1444)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Add syntax highlighting support for CashScript](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/607)**<br />
Request authored by [James Zuccon](https://invent.kde.org/jamesz) and merged after 2 days.

- **Kate - [man page: refer to Qt6 &amp; KF6 version of commandline options now](https://invent.kde.org/utilities/kate/-/merge_requests/1442)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KTextEditor - [Fix argument hint placement](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/677)**<br />
Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry) and merged after one day.

- **Kate - [Mark risky KNS content](https://invent.kde.org/utilities/kate/-/merge_requests/1440)**<br />
Request authored by [Nate Graham](https://invent.kde.org/ngraham) and merged after one day.

- **Kate - [Documents: Fix sorting for directories](https://invent.kde.org/utilities/kate/-/merge_requests/1441)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [ColorPicker plugin fixes](https://invent.kde.org/utilities/kate/-/merge_requests/1439)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Add job to publish on Microsoft Store (ane build appx)](https://invent.kde.org/utilities/kate/-/merge_requests/1435)**<br />
Request authored by [Julius Künzel](https://invent.kde.org/jlskuz) and merged after 4 days.

- **KSyntaxHighlighting - [cmake.xml: updates for the recently released CMake 3.29](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/606)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after 3 days.

