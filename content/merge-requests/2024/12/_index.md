---
title: Merge Requests - December 2024
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2024/12/
---

#### Week 01

- **Kate - [Allow the user to add paths to PATH var](https://invent.kde.org/utilities/kate/-/merge_requests/1686)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Adjust margins in build and gdb plugins](https://invent.kde.org/utilities/kate/-/merge_requests/1671)**<br />
Request authored by [Leo Ruggeri](https://invent.kde.org/mrpink) and merged after 19 days.

- **Kate - [Use path_helper utility on macos for setting PATH](https://invent.kde.org/utilities/kate/-/merge_requests/1684)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Remove unimplemented methods](https://invent.kde.org/utilities/kate/-/merge_requests/1682)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged after 2 days.

- **Kate - [snapcraft: Bring in fixes from release/24.08](https://invent.kde.org/utilities/kate/-/merge_requests/1685)**<br />
Request authored by [Scarlett Moore](https://invent.kde.org/scarlettmoore) and merged at creation day.

- **Kate - [don&#x27;t cut :x:y cursor info from remote urls](https://invent.kde.org/utilities/kate/-/merge_requests/1683)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

#### Week 52

- **Kate - [Add basic ui test](https://invent.kde.org/utilities/kate/-/merge_requests/1681)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [use the same font size as the ui](https://invent.kde.org/utilities/kate/-/merge_requests/1666)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 21 days.

- **KTextEditor - [&quot;Paste From File&quot; feature for Kate to select a file and insert it&#x27;s contents into the current file](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/766)**<br />
Request authored by [ambar chakravartty](https://invent.kde.org/amch) and merged after 4 days.

- **KTextEditor - [Try to split clipboard text across multiple cursors when pasting](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/768)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [add &#x27;Close this window&#x27; action](https://invent.kde.org/utilities/kate/-/merge_requests/1680)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

#### Week 51

- **Kate - [Add a File Template plugin](https://invent.kde.org/utilities/kate/-/merge_requests/1676)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 6 days.

- **Kate - [Introduce KTextEditor::Command to libkateprivate for scripting](https://invent.kde.org/utilities/kate/-/merge_requests/1672)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 8 days.

- **KTextEditor - [Optimize kill lines for large number of cursors](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/765)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **kate-editor.org - [docs: Add link to flathub.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/62)**<br />
Request authored by [Maximilian Kolb](https://invent.kde.org/maximiliankolb) and merged after 66 days.

- **Kate - [Make the project target updates less jumpy](https://invent.kde.org/utilities/kate/-/merge_requests/1675)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 2 days.

- **Kate - [Bunch of clang tidy fixes](https://invent.kde.org/utilities/kate/-/merge_requests/1678)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Run clang-tidy modernize-use-auto over the code](https://invent.kde.org/utilities/kate/-/merge_requests/1677)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 50

- **KTextEditor - [Fix full line selection behaviour of the C++ versions of the editing commands](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/764)**<br />
Request authored by [Paul Wise](https://invent.kde.org/pabs) and merged at creation day.

- **Kate - [If the configured build directory does not exist, ask.](https://invent.kde.org/utilities/kate/-/merge_requests/1674)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

- **Kate - [Fix build on macos](https://invent.kde.org/utilities/kate/-/merge_requests/1673)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Error popup when clicking on deleted files in Recent Files menus](https://invent.kde.org/utilities/kate/-/merge_requests/1670)**<br />
Request authored by [ambar chakravartty](https://invent.kde.org/amch) and merged after one day.

- **KSyntaxHighlighting - [systemd unit: update to systemd v257](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/674)**<br />
Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr) and merged after one day.

- **Kate - [Avoid a useless allocation when creating project](https://invent.kde.org/utilities/kate/-/merge_requests/1669)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [store QTextLayout inline](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/763)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

#### Week 49

- **Kate - [GIT_SILENT: it compiles fine without deprecated kf6.9](https://invent.kde.org/utilities/kate/-/merge_requests/1668)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **Kate - [Update README.md - Add Installation info, add screenshot header, make app name...](https://invent.kde.org/utilities/kate/-/merge_requests/1667)**<br />
Request authored by [Justin Zobel](https://invent.kde.org/justinzobel) and merged after one day.

- **Kate - [Use Filter… instead of Filter...](https://invent.kde.org/utilities/kate/-/merge_requests/1665)**<br />
Request authored by [Albert Astals Cid](https://invent.kde.org/aacid) and merged at creation day.

- **KSyntaxHighlighting - [Update odin highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/673)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Make placeholder text translatable](https://invent.kde.org/utilities/kate/-/merge_requests/1664)**<br />
Request authored by [Albert Astals Cid](https://invent.kde.org/aacid) and merged at creation day.

- **Kate - [Add Kate branding color](https://invent.kde.org/utilities/kate/-/merge_requests/1663)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged at creation day.

- **KSyntaxHighlighting - [The lua indenter was removed long ago in ktexteditor](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/672)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 48

- **Kate - [Don&#x27;t use title case for the appstream summary](https://invent.kde.org/utilities/kate/-/merge_requests/1662)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged at creation day.

