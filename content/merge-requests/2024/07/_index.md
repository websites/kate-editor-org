---
title: Merge Requests - July 2024
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2024/07/
---

#### Week 31

- **Kate - [Tabs: Wrap around to first on nextTab if current is last tab](https://invent.kde.org/utilities/kate/-/merge_requests/1552)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Diff fixes](https://invent.kde.org/utilities/kate/-/merge_requests/1551)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [earthfile.xml: modernize highlighter to support Earthly 0.8 features](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/631)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after 2 days.

- **Kate - [lsp: Dont discard ranges which meant to encompass the whole document](https://invent.kde.org/utilities/kate/-/merge_requests/1549)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 30

- **Kate - [snapcraft: Fix XDG_CONFIG_HOME to point to a writable dir.](https://invent.kde.org/utilities/kate/-/merge_requests/1548)**<br />
Request authored by [Scarlett Moore](https://invent.kde.org/scarlettmoore) and merged at creation day.

- **KTextEditor - [use ktexteditor-script-tester6 for javascript tests](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/703)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 14 days.

- **KTextEditor - [templates: Remove one level of nesting](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/704)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged after 2 days.

- **KTextEditor - [Ignore buffer signals in completion on undo/redo](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/705)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [C++: add floating-point literal suffixes of C++23](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/629)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 4 days.

- **KSyntaxHighlighting - [Python: add some special variables and fix &#x27;\&#x27; line continuation after a string](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/630)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

- **Kate - [Improve open commit dialog](https://invent.kde.org/utilities/kate/-/merge_requests/1547)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [cursorToOffset: remove unnecessary line variable](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/706)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Snapcraft: Initial qt6 port](https://invent.kde.org/utilities/kate/-/merge_requests/1545)**<br />
Request authored by [Scarlett Moore](https://invent.kde.org/scarlettmoore) and merged after 2 days.

- **Kate - [Add rpmspec to recognized language servers](https://invent.kde.org/utilities/kate/-/merge_requests/1546)**<br />
Request authored by [Bruno Pitrus](https://invent.kde.org/brjsp) and merged at creation day.

- **KSyntaxHighlighting - [add .clang-format file with YAML](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/628)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [allow the user to manage the allowed/blocked commands](https://invent.kde.org/utilities/kate/-/merge_requests/1544)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

#### Week 29

- **KSyntaxHighlighting - [Zig: fix range operator preceded by a number: [0..]](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/627)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [Build: Load CMake targets as project targets](https://invent.kde.org/utilities/kate/-/merge_requests/1535)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 15 days.

- **Kate - [Use .contains() member fn with std::map/unordered_map](https://invent.kde.org/utilities/kate/-/merge_requests/1543)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Debugger: Re-enable pausing execution without setting a break-point](https://invent.kde.org/utilities/kate/-/merge_requests/1541)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 2 days.

- **Kate - [Allow removing multiple items from welcome page](https://invent.kde.org/utilities/kate/-/merge_requests/1542)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Odin: add raw string color](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/625)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after one day.

- **KSyntaxHighlighting - [Update syntax highlighting for kdesrc-buildrc](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/624)**<br />
Request authored by [Dario Cambié](https://invent.kde.org/dcambie) and merged after 6 days.

#### Week 28

- **Kate - [plugin_kategdp: When breakpoint mark changes, toggle breakpoint for it](https://invent.kde.org/utilities/kate/-/merge_requests/1540)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after 2 days.

- **Kate - [Add formatter for opsi-script language](https://invent.kde.org/utilities/kate/-/merge_requests/1539)**<br />
Request authored by [Stefan Staeglich](https://invent.kde.org/staeglich) and merged at creation day.

- **Kate - [Port to QtKeychain](https://invent.kde.org/utilities/kate/-/merge_requests/1538)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [use session file name as stash folder name &amp; fix encoding](https://invent.kde.org/utilities/kate/-/merge_requests/1534)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 4 days.

- **Kate - [Fix katesql plugin build](https://invent.kde.org/utilities/kate/-/merge_requests/1537)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 27

- **KTextEditor - [Treat 0x0000 to 0x001F as non-printable](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/701)**<br />
Request authored by [Gary Wang](https://invent.kde.org/garywang) and merged after 3 days.

- **KTextEditor - [add action to quick toggle space visibility](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/698)**<br />
Request authored by [Gary Wang](https://invent.kde.org/garywang) and merged after 16 days.

- **KSyntaxHighlighting - [cmake.xml: update syntax for CMake 3.30](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/622)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KSyntaxHighlighting - [hare: add done keyword](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/623)**<br />
Request authored by [wackbyte](https://invent.kde.org/wackbyte) and merged at creation day.

- **Kate - [Improve CMake based loading](https://invent.kde.org/utilities/kate/-/merge_requests/1532)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

- **Kate - [Fix name of OmniSharp LSP binary](https://invent.kde.org/utilities/kate/-/merge_requests/1533)**<br />
Request authored by [James Groom](https://invent.kde.org/yoshirulz) and merged at creation day.

- **Kate - [Move target actions to TargetUI + fix target cloning](https://invent.kde.org/utilities/kate/-/merge_requests/1531)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

- **KTextEditor - [Tool to test javascript scripts](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/696)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 30 days.

- **KTextEditor - [kateregexpsearch: fix FAST_DEBUG](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/700)**<br />
Request authored by [Lassi Väätämöinen](https://invent.kde.org/lvaatamoinen) and merged at creation day.

