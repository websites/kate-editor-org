---
title: Merge Requests - June 2024
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2024/06/
---

#### Week 26

- **Kate - [Kate: tab switch consts to camelCase](https://invent.kde.org/utilities/kate/-/merge_requests/1530)**<br />
Request authored by [Lassi Väätämöinen](https://invent.kde.org/lvaatamoinen) and merged at creation day.

- **Kate - [Add settings for Gleam LSP](https://invent.kde.org/utilities/kate/-/merge_requests/1529)**<br />
Request authored by [Louis Guichard](https://invent.kde.org/glpda) and merged after one day.

- **Kate - [Add Copy,Cut and Paste of build targets and sets](https://invent.kde.org/utilities/kate/-/merge_requests/1519)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 17 days.

- **kate-editor.org - [build-it: reword config line details](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/61)**<br />
Request authored by [Lassi Väätämöinen](https://invent.kde.org/lvaatamoinen) and merged at creation day.

- **Kate - [Kate: direct tab switching](https://invent.kde.org/utilities/kate/-/merge_requests/1527)**<br />
Request authored by [Lassi Väätämöinen](https://invent.kde.org/lvaatamoinen) and merged after 2 days.

- **Kate - [Output: include date](https://invent.kde.org/utilities/kate/-/merge_requests/1528)**<br />
Request authored by [Lassi Väätämöinen](https://invent.kde.org/lvaatamoinen) and merged at creation day.

- **Kate - [project: add pid and directory to project ctags filename](https://invent.kde.org/utilities/kate/-/merge_requests/1526)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 4 days.

- **KSyntaxHighlighting - [Add Gleam syntax](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/621)**<br />
Request authored by [Louis Guichard](https://invent.kde.org/glpda) and merged after one day.

- **Kate - [kwrite: Add developer name and launchable to appdata](https://invent.kde.org/utilities/kate/-/merge_requests/1524)**<br />
Request authored by [bbhtt](https://invent.kde.org/bbhtt) and merged after 3 days.

- **Kate - [lspclient: remove reference to absent rapidjson include dir](https://invent.kde.org/utilities/kate/-/merge_requests/1525)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **KSyntaxHighlighting - [OrgMode: add header-item folding, properties folding, more todo keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/620)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after 2 days.

#### Week 25

- **KTextEditor - [API documentation: fix typo on kte_design page](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/699)**<br />
Request authored by [Stefan Kaspar](https://invent.kde.org/skaspar) and merged at creation day.

- **KSyntaxHighlighting - [[dracula.theme] Fix &quot;ISO/Delphi Extended&quot; for Pascal.](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/619)**<br />
Request authored by [Kirill Zhumarin](https://invent.kde.org/zhumarin) and merged after one day.

- **Kate - [Add GUI tests to Kwrite](https://invent.kde.org/utilities/kate/-/merge_requests/1515)**<br />
Request authored by [Antoine Herlicq](https://invent.kde.org/ahq) and merged after 10 days.

- **Kate - [Add LSP client settings for Java](https://invent.kde.org/utilities/kate/-/merge_requests/1523)**<br />
Request authored by [Jonah Brüchert](https://invent.kde.org/jbbgameich) and merged at creation day.

- **Kate - [Add Typst LSP config](https://invent.kde.org/utilities/kate/-/merge_requests/1522)**<br />
Request authored by [Frederik Banning](https://invent.kde.org/laubblaeser) and merged after one day.

- **Kate - [Fix appdata validation](https://invent.kde.org/utilities/kate/-/merge_requests/1521)**<br />
Request authored by [bbhtt](https://invent.kde.org/bbhtt) and merged at creation day.

- **KSyntaxHighlighting - [nginx: update for new directives and variables](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/618)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged at creation day.

#### Week 24

- **Kate - [Add LSP client settings for PureScript](https://invent.kde.org/utilities/kate/-/merge_requests/1520)**<br />
Request authored by [Jonah Brüchert](https://invent.kde.org/jbbgameich) and merged at creation day.

- **Kate - [[gdbplugin] fixed removing unresolved breakpoints](https://invent.kde.org/utilities/kate/-/merge_requests/1518)**<br />
Request authored by [Héctor Mesa Jiménez](https://invent.kde.org/hectorm) and merged after one day.

- **Kate - [bring back proper config dialog initial size](https://invent.kde.org/utilities/kate/-/merge_requests/1517)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Add an objectName to the centralWidget](https://invent.kde.org/utilities/kate/-/merge_requests/1516)**<br />
Request authored by [Kevin Ottens](https://invent.kde.org/ervin) and merged at creation day.

- **KSyntaxHighlighting - [systemd unit: update to systemd v256](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/617)**<br />
Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr) and merged at creation day.

- **KSyntaxHighlighting - [Add basic syntax highlighting rules for opsi-script](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/616)**<br />
Request authored by [Stefan Staeglich](https://invent.kde.org/staeglich) and merged after 5 days.

- **KSyntaxHighlighting - [[dracula.theme] Fix &quot;Special Variable&quot; for PHP.](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/615)**<br />
Request authored by [Kirill Zhumarin](https://invent.kde.org/zhumarin) and merged after 8 days.

#### Week 23

- **Kate - [Modernize confusing &#x27;continueCancel&#x27; message boxes](https://invent.kde.org/utilities/kate/-/merge_requests/1514)**<br />
Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry) and merged at creation day.

- **Kate - [Build Plugin: clean up logging output](https://invent.kde.org/utilities/kate/-/merge_requests/1512)**<br />
Request authored by [Alexander Neundorf](https://invent.kde.org/neundorf) and merged after 2 days.

- **KSyntaxHighlighting - [Add Typst highlighter](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/614)**<br />
Request authored by [Marco Rebhan](https://invent.kde.org/dblsaiko) and merged after 10 days.

- **Kate - [Be less strict about spaces in snippet names](https://invent.kde.org/utilities/kate/-/merge_requests/1511)**<br />
Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry) and merged after one day.

- **Kate - [build plugin: add &quot;Compile current file&quot;](https://invent.kde.org/utilities/kate/-/merge_requests/1508)**<br />
Request authored by [Alexander Neundorf](https://invent.kde.org/neundorf) and merged after 11 days.

