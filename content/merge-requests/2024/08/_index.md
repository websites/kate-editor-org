---
title: Merge Requests - August 2024
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2024/08/
---

#### Week 35

- **Kate - [Fix clashing and missing keyboard accelerators](https://invent.kde.org/utilities/kate/-/merge_requests/1569)**<br />
Request authored by [Karl Ove Hufthammer](https://invent.kde.org/huftis) and merged after 2 days.

- **Kate - [Direct tab switching defaults for non-Mac](https://invent.kde.org/utilities/kate/-/merge_requests/1568)**<br />
Request authored by [Lassi Väätämöinen](https://invent.kde.org/lvaatamoinen) and merged after one day.

- **KSyntaxHighlighting - [Update QFace IDL Definition to support single line comments](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/654)**<br />
Request authored by [Dominik Holland](https://invent.kde.org/gagi) and merged after 3 days.

#### Week 34

- **Kate - [Fix adding global suppression doesn&#x27;t suppress existing diagnostics](https://invent.kde.org/utilities/kate/-/merge_requests/1567)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [odin.xml: Multiple fixes to the syntax](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/656)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged at creation day.

- **KSyntaxHighlighting - [QFace: WordDetect without trailing space](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/655)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Bash/Zsh: add \E, \uHHHH and \UHHHHHHHH ; Bash: add @k parameter transformation and &amp; in string substitution](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/645)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 7 days.

- **Kate - [Change Microsoft Store URL to apps.microsoft.com](https://invent.kde.org/utilities/kate/-/merge_requests/1565)**<br />
Request authored by [Ingo Klöcker](https://invent.kde.org/kloecker) and merged after one day.

- **Kate - [Fix resizing logic of the LSP plugin tooltips](https://invent.kde.org/utilities/kate/-/merge_requests/1566)**<br />
Request authored by [Adam Fontenot](https://invent.kde.org/adamfontenot) and merged at creation day.

- **KTextEditor - [Move the sortuniq, uniq implementation to C++](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/719)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **KTextEditor - [read and write font features to config](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/720)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Tooltip: Only highlight blocks that are marked BlockCodeLanguage](https://invent.kde.org/utilities/kate/-/merge_requests/1564)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Build release branch.](https://invent.kde.org/utilities/kate/-/merge_requests/1562)**<br />
Request authored by [Scarlett Moore](https://invent.kde.org/scarlettmoore) and merged at creation day.

- **KSyntaxHighlighting - [Add definition for the QFace IDL](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/648)**<br />
Request authored by [Dominik Holland](https://invent.kde.org/gagi) and merged after 4 days.

- **KSyntaxHighlighting - [Modelines: fix indent-mode value and remove deprecated variables](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/626)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 32 days.

- **KSyntaxHighlighting - [detect-identical-context.py: add -p to show duplicate content](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/646)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 4 days.

- **KSyntaxHighlighting - [XML: add parameter entity declaration symbol (% in &lt;!ENTITY % name &quot;...&quot;&gt;)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/647)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

- **KSyntaxHighlighting - [Indexer: suggest removing .* and .*$ from RegExpr with lookAhead=1](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/649)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

- **KSyntaxHighlighting - [PHP: add \{ in double quote string as a Backslash Code](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/653)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [Bash: fix escaped line in brace condition](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/652)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [Nix: fix string in attribute access](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/651)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [B&amp;R: Add QDir::homePath() to newly created Tartget-Sets](https://invent.kde.org/utilities/kate/-/merge_requests/1559)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 3 days.

- **KTextEditor - [Fix doc.text() when first block is empty](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/718)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 5 days.

#### Week 33

- **Kate - [Formatter: add formatting for Odin language](https://invent.kde.org/utilities/kate/-/merge_requests/1561)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after one day.

- **KSyntaxHighlighting - [optimize AbstractHighlighterPrivate::ensureDefinitionLoaded (highlighter_benchmark is 1.1% faster)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/644)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [ci: add Alpine/musl job](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/642)**<br />
Request authored by [Bart Ribbers](https://invent.kde.org/bribbers) and merged at creation day.

- **Kate - [KateBuildView: Remove close button entirely](https://invent.kde.org/utilities/kate/-/merge_requests/1560)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged at creation day.

- **Kate - [Change the default LSP client for GLSL language to glsl_analyzer](https://invent.kde.org/utilities/kate/-/merge_requests/1558)**<br />
Request authored by [Marián Konček](https://invent.kde.org/mkoncek) and merged at creation day.

- **KTextEditor - [Fix block splitting](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/717)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Indexer: suggest replacing RegExpr with Int or Float when possible](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/641)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Indexer: check name and alternativeNames conflict ; update alternativeNames for generated files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/639)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [optimize Repository::addCustomSearchPath](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/638)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [Several independent changes which generally delete code](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/637)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [orgmode: add syntax highlighting to some languages](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/640)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged at creation day.

#### Week 32

- **KSyntaxHighlighting - [optimize Repository::definition[s]For*()](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/636)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **KTextEditor - [Restore previous indentation test mode based on individual files](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/715)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KTextEditor - [Store startlines in the buffer instead of block](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/716)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [doc: Fix code example for plugin hosting](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/714)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged at creation day.

- **KTextEditor - [Optimize cursorToOffset](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/713)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [ksyntaxhighlighter6: add --background-role parameter to display different possible theme backgrounds](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/633)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 7 days.

- **KSyntaxHighlighting - [added a link to all available syntaxes and how to test a file in an isolated environment](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/635)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 5 days.

- **Kate - [Add default icons for the &quot;Recent Sessions&quot; and &quot;All Sessions&quot; menus](https://invent.kde.org/utilities/kate/-/merge_requests/1556)**<br />
Request authored by [Nate Graham](https://invent.kde.org/ngraham) and merged after one day.

#### Week 31

- **KTextEditor - [Dont indent on tab when in block selection mode](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/711)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KSyntaxHighlighting - [Python: raw-string with lowercase r are highlighted as regex](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/632)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 5 days.

- **KSyntaxHighlighting - [JSON: fix float that start with 0 ; add *.jsonl and *.cast extensions](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/634)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [Fix Show sidebar shortcut not being restored properly](https://invent.kde.org/utilities/kate/-/merge_requests/1553)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [apps/lib/diff/difflinenumarea.cpp - include &lt;QApplication&gt;](https://invent.kde.org/utilities/kate/-/merge_requests/1554)**<br />
Request authored by [Allen Winter](https://invent.kde.org/winterz) and merged after one day.

- **Kate - [project: make getting the .git/index file path more robust](https://invent.kde.org/utilities/kate/-/merge_requests/1550)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 3 days.

- **KTextEditor - [Fix selection printing](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/712)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix drag pixmap with wrapped lines](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/709)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Add actions to convert spaces to tabs and vice versa](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/708)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

