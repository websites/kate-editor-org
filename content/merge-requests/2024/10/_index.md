---
title: Merge Requests - October 2024
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2024/10/
---

#### Week 44

- **Kate - [reduce qobject macros](https://invent.kde.org/utilities/kate/-/merge_requests/1639)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix appium UI test and reenable them](https://invent.kde.org/utilities/kate/-/merge_requests/1635)**<br />
Request authored by [Benjamin Port](https://invent.kde.org/bport) and merged after 3 days.

- **Kate - [Simplify KateDocManager signals](https://invent.kde.org/utilities/kate/-/merge_requests/1637)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [gitwidget: Update git widget with correct folder on project change](https://invent.kde.org/utilities/kate/-/merge_requests/1636)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after one day.

- **Kate - [addons/git-blame: hide the tooltip when appropriate](https://invent.kde.org/utilities/kate/-/merge_requests/1634)**<br />
Request authored by [Méven Car](https://invent.kde.org/meven) and merged at creation day.

- **Kate - [lsp: Use a custom header with menu button](https://invent.kde.org/utilities/kate/-/merge_requests/1633)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

#### Week 43

- **Kate - [ensure we keep the tab order on session restore](https://invent.kde.org/utilities/kate/-/merge_requests/1618)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 15 days.

- **KTextEditor - [Don&#x27;t temporarily clear document URL during openUrl()](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/750)**<br />
Request authored by [Igor Kushnir](https://invent.kde.org/igorkushnir) and merged after one day.

- **KTextEditor - [Only discard completion if the cursor was at the end of line](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/749)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [State: Fix inconsistent linkage warning on Windows](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/668)**<br />
Request authored by [Orgad Shaneh](https://invent.kde.org/orgads) and merged after one day.

- **Kate - [Remove extra shortcuts for tab next/prev](https://invent.kde.org/utilities/kate/-/merge_requests/1629)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Git improvements](https://invent.kde.org/utilities/kate/-/merge_requests/1632)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Fix implicit conversion of Qt::Key in Qt 6.9](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/748)**<br />
Request authored by [Fushan Wen](https://invent.kde.org/fusionfuture) and merged after 3 days.

- **Kate - [Remove per project gitwidget](https://invent.kde.org/utilities/kate/-/merge_requests/1631)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Show a better hint in quick open line edit](https://invent.kde.org/utilities/kate/-/merge_requests/1630)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [dap: Dont require 2 clicks to stop the debugger](https://invent.kde.org/utilities/kate/-/merge_requests/1625)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KTextEditor - [Try to avoid unwanted completions](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/747)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

#### Week 42

- **Kate - [Fix sql copy/export is randomly ordered](https://invent.kde.org/utilities/kate/-/merge_requests/1627)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Translating the empty string is impossible](https://invent.kde.org/utilities/kate/-/merge_requests/1628)**<br />
Request authored by [Albert Astals Cid](https://invent.kde.org/aacid) and merged at creation day.

- **Kate - [Fix infinite loop](https://invent.kde.org/utilities/kate/-/merge_requests/1626)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add libffi8 needed by opensuse.](https://invent.kde.org/utilities/kate/-/merge_requests/1623)**<br />
Request authored by [Scarlett Moore](https://invent.kde.org/scarlettmoore) and merged after 2 days.

- **KTextEditor - [Make ViewPrivate::displayRangeChanged public](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/746)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [better contrast for search highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/667)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [Adding TLA+ support](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/666)**<br />
Request authored by [Younes IO](https://invent.kde.org/younes-io) and merged after one day.

- **KSyntaxHighlighting - [odin: Add escape character to strings](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/665)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after 2 days.

- **Kate - [A couple of Session fixes](https://invent.kde.org/utilities/kate/-/merge_requests/1620)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [snapcraft: Fix ld-library-config.](https://invent.kde.org/utilities/kate/-/merge_requests/1622)**<br />
Request authored by [Scarlett Moore](https://invent.kde.org/scarlettmoore) and merged at creation day.

- **KTextEditor - [Set DocumentPrivate::m_reloading to false only if loading](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/744)**<br />
Request authored by [Igor Kushnir](https://invent.kde.org/igorkushnir) and merged after 3 days.

- **Kate - [Refactor format plugin for simplicity](https://invent.kde.org/utilities/kate/-/merge_requests/1619)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix line removal not handled properly in KateTemplateHandler](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/745)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 41

- **Kate - [KateViewSpace: Store session group name on save](https://invent.kde.org/utilities/kate/-/merge_requests/1617)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [QuickOpen: List open projects and allow switching to them from quick switcher](https://invent.kde.org/utilities/kate/-/merge_requests/1615)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after 2 days.

- **Kate - [kateprojectindex: Fix broken QStringLiteral argument parsing](https://invent.kde.org/utilities/kate/-/merge_requests/1616)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged at creation day.

- **Kate - [lsp: use char array for config keys](https://invent.kde.org/utilities/kate/-/merge_requests/1614)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Inline blocksize into buffer](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/740)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KTextEditor - [Improve TextRange::checkValidity performance and document the empty behaviour performance difference](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/743)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Improve external tools plugin load time](https://invent.kde.org/utilities/kate/-/merge_requests/1613)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add some timing prints to startup code](https://invent.kde.org/utilities/kate/-/merge_requests/1612)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Add a swap file test](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/742)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Use std::vector for cursor storage](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/741)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [improve debug plugin load performance](https://invent.kde.org/utilities/kate/-/merge_requests/1611)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

#### Week 40

- **Kate - [Lazy initialize project plugin info view](https://invent.kde.org/utilities/kate/-/merge_requests/1610)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Work around flakeyness of &#x27;save file&#x27; UI test](https://invent.kde.org/utilities/kate/-/merge_requests/1609)**<br />
Request authored by [Antoine Gonzalez](https://invent.kde.org/daspood) and merged at creation day.

- **KTextEditor - [Allow disabling editorconfig](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/738)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **Kate - [Port away from deprecated KPluralHandlingSpinBox](https://invent.kde.org/utilities/kate/-/merge_requests/1608)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged after one day.

- **KTextEditor - [Port away from deprecated KPluralHandlingSpinBox](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/739)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **KTextEditor - [Fix unexpected space indentation in Go var group](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/736)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KSyntaxHighlighting - [odin: add missing items, fix attribute, add directive](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/664)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after 2 days.

- **Kate - [debug: Improve threads handling](https://invent.kde.org/utilities/kate/-/merge_requests/1597)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

