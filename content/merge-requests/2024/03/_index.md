---
title: Merge Requests - March 2024
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2024/03/
---

#### Week 13

- **Kate - [don&#x27;t allow menu bar hide + hamburger menu on macOS](https://invent.kde.org/utilities/kate/-/merge_requests/1438)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Add Rainbow CSV plugin](https://invent.kde.org/utilities/kate/-/merge_requests/1437)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Hide menu bar in KWrite](https://invent.kde.org/utilities/kate/-/merge_requests/1422)**<br />
Request authored by [Nathan Garside](https://invent.kde.org/ngarside) and merged after 12 days.

- **Kate - [Show translated shortcut](https://invent.kde.org/utilities/kate/-/merge_requests/1434)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged after 2 days.

- **Kate - [Add screenshots for Windows to Appstream data](https://invent.kde.org/utilities/kate/-/merge_requests/1436)**<br />
Request authored by [Julius Künzel](https://invent.kde.org/jlskuz) and merged at creation day.

- **Kate - [Fix session autosave deletes view session config](https://invent.kde.org/utilities/kate/-/merge_requests/1433)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KTextEditor - [Dont use activeView as save dialog parent](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/676)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Dont write useless entries to session config](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/675)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

#### Week 12

- **Kate - [Fix typos, seperator-&gt;separator](https://invent.kde.org/utilities/kate/-/merge_requests/1431)**<br />
Request authored by [Daniels Umanovskis](https://invent.kde.org/umanovskis) and merged at creation day.

- **Kate - [add appimage ci](https://invent.kde.org/utilities/kate/-/merge_requests/1429)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 3 days.

- **Kate - [Scroll Synchronisation](https://invent.kde.org/utilities/kate/-/merge_requests/1430)**<br />
Request authored by [Ulterno Ω*](https://invent.kde.org/ulterno) and merged after one day.

- **Kate - [Add a button to the URL bar that copies the document&#x27;s URL to clipboard.](https://invent.kde.org/utilities/kate/-/merge_requests/1425)**<br />
Request authored by [Daniels Umanovskis](https://invent.kde.org/umanovskis) and merged after 5 days.

- **KTextEditor - [kateviewinternal: accept surrogate category character](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/674)**<br />
Request authored by [Yifan Zhu](https://invent.kde.org/fanzhuyifan) and merged at creation day.

- **Kate - [Restore last active toolview when closing file history](https://invent.kde.org/utilities/kate/-/merge_requests/1428)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix build, add missing QPointer includes](https://invent.kde.org/utilities/kate/-/merge_requests/1427)**<br />
Request authored by [Heiko Becker](https://invent.kde.org/heikobecker) and merged at creation day.

- **Kate - [Fixes for diagnostic view](https://invent.kde.org/utilities/kate/-/merge_requests/1426)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix tooltip not hiding after context menu was opened](https://invent.kde.org/utilities/kate/-/merge_requests/1424)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

#### Week 11

- **Kate - [Fix build, add missing QPointer includes](https://invent.kde.org/utilities/kate/-/merge_requests/1423)**<br />
Request authored by [Aleix Pol Gonzalez](https://invent.kde.org/apol) and merged at creation day.

- **Kate - [move lsp context menu actions to xmlgui](https://invent.kde.org/utilities/kate/-/merge_requests/1371)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 96 days.

- **Kate - [Remove doc about dead option](https://invent.kde.org/utilities/kate/-/merge_requests/1421)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged at creation day.

- **Kate - [add size limit config option](https://invent.kde.org/utilities/kate/-/merge_requests/1418)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 4 days.

- **KTextEditor - [Remove focus frame option](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/673)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged at creation day.

- **Kate - [Fix #483810: Use working directory placeholders when executing the run command](https://invent.kde.org/utilities/kate/-/merge_requests/1419)**<br />
Request authored by [André Althaus](https://invent.kde.org/nomanor) and merged after one day.

- **Kate - [Fix #483812: Show correct tooltip for working directory cell](https://invent.kde.org/utilities/kate/-/merge_requests/1420)**<br />
Request authored by [André Althaus](https://invent.kde.org/nomanor) and merged after one day.

- **KTextEditor - [Fix support for folding in vi normal mode](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/672)**<br />
Request authored by [Quinten Kock](https://invent.kde.org/colonelphantom) and merged after 4 days.

#### Week 10

- **KTextEditor - [try to save changes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/546)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 316 days.

- **KSyntaxHighlighting - [cpp: Update Qt classes for Qt 6](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/605)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged at creation day.

- **KSyntaxHighlighting - [Add gprbuild syntax](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/599)**<br />
Request authored by [Léo Germond](https://invent.kde.org/leogermond) and merged after 43 days.

- **KSyntaxHighlighting - [Add common alternative names used in various markdown parsers for languages](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/604)**<br />
Request authored by [James Graham](https://invent.kde.org/nvrwhere) and merged after one day.

#### Week 09

- **Kate - [addons/konsole: Add split horizontal, vertical and new tab to terminal tools](https://invent.kde.org/utilities/kate/-/merge_requests/1417)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged after one day.

- **KSyntaxHighlighting - [Alternate Names for Definitions](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/603)**<br />
Request authored by [James Graham](https://invent.kde.org/nvrwhere) and merged after one day.

- **KSyntaxHighlighting - [Add new type keywords to the Stan language definition](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/602)**<br />
Request authored by [Brian Ward](https://invent.kde.org/wardbrian) and merged after 4 days.

