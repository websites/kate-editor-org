---
title: Merge Requests - May 2024
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2024/05/
---

#### Week 22

- **Kate - [Remove KTextEditor/Plugin service type from JSON metadata, Replace KDevelop/Plugin servicetype with LoadInKDevelop boolean flag](https://invent.kde.org/utilities/kate/-/merge_requests/1509)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged after one day.

#### Week 21

- **KSyntaxHighlighting - [Couple fixes to Nix highlighter](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/612)**<br />
Request authored by [Marco Rebhan](https://invent.kde.org/dblsaiko) and merged after 2 days.

- **Kate - [Use std::span in a few places](https://invent.kde.org/utilities/kate/-/merge_requests/1507)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Port to KStandardActions](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/693)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **Kate - [lsp: Ignore invalid ranges when formatting](https://invent.kde.org/utilities/kate/-/merge_requests/1505)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [fmt: minor refactorings](https://invent.kde.org/utilities/kate/-/merge_requests/1506)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [fmt: small improvements](https://invent.kde.org/utilities/kate/-/merge_requests/1503)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Use Output view for error messages](https://invent.kde.org/utilities/kate/-/merge_requests/1504)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Build plugin: add functionality to load targets from an existing cmake build tree](https://invent.kde.org/utilities/kate/-/merge_requests/1413)**<br />
Request authored by [Alexander Neundorf](https://invent.kde.org/neundorf) and merged after 86 days.

- **KTextEditor - [Never remove trailing spaces on diff file type](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/692)**<br />
Request authored by [Robert-André Mauchin](https://invent.kde.org/rmauchin) and merged after 2 days.

#### Week 20

- **Kate - [allow to configure the style](https://invent.kde.org/utilities/kate/-/merge_requests/1495)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 4 days.

- **Kate - [Allow creating file/folder at project root](https://invent.kde.org/utilities/kate/-/merge_requests/1502)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Move formatting plugin actions to &quot;Edit&quot; menu](https://invent.kde.org/utilities/kate/-/merge_requests/1500)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix ruby method parsing and nested classes parsing](https://invent.kde.org/utilities/kate/-/merge_requests/1501)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add QML Formatter](https://invent.kde.org/utilities/kate/-/merge_requests/1499)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged at creation day.

- **Kate - [Scroll Synchronisation activation method reimplemented](https://invent.kde.org/utilities/kate/-/merge_requests/1469)**<br />
Request authored by [Ulterno Ω*](https://invent.kde.org/ulterno) and merged after 25 days.

- **Kate - [Formatting plugin improvements](https://invent.kde.org/utilities/kate/-/merge_requests/1498)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Gdb UI file: use notr=&quot;true&quot; instead of old comment=&quot;KDE::DoNotExtract&quot;](https://invent.kde.org/utilities/kate/-/merge_requests/1496)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after one day.

- **Kate - [Highlight links in diff commit view](https://invent.kde.org/utilities/kate/-/merge_requests/1497)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Document formatting plugin](https://invent.kde.org/utilities/kate/-/merge_requests/1494)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add docs for compiler explorer plugin](https://invent.kde.org/utilities/kate/-/merge_requests/1493)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **kate-editor.org - [Blog about the formatting plugin](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/60)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 19

- **Kate - [Remove unused _version.h](https://invent.kde.org/utilities/kate/-/merge_requests/1492)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **KTextEditor - [Screenshot dialog: support drag of image](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/686)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after 3 days.

- **Kate - [Modernize use designated init](https://invent.kde.org/utilities/kate/-/merge_requests/1490)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Buildplugin: Fix file-name-detection if there is no message](https://invent.kde.org/utilities/kate/-/merge_requests/1485)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 2 days.

- **Kate - [🍒KateSaveModifiedDialog: Use message box icon size from style](https://invent.kde.org/utilities/kate/-/merge_requests/1489)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged at creation day.

- **Kate - [KateSaveModifiedDialog: Use message box icon size from style](https://invent.kde.org/utilities/kate/-/merge_requests/1488)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged at creation day.

- **KTextEditor - [make tests independent of the user&#x27;s environment](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/690)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KTextEditor - [Modernize use designated init](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/691)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [do not display diff result in script tests when -silent is used](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/687)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KTextEditor - [fix raw string indentation with cstyle + C++](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/688)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [reduce moc usage in katemainwindow](https://invent.kde.org/utilities/kate/-/merge_requests/1487)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [includes cleanup](https://invent.kde.org/utilities/kate/-/merge_requests/1486)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [use new theme and style init functions](https://invent.kde.org/utilities/kate/-/merge_requests/1482)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

- **Kate - [Manually resizable target columns in build-plugin](https://invent.kde.org/utilities/kate/-/merge_requests/1402)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 87 days.

- **Kate - [Refer to qml language server binary by upstream name](https://invent.kde.org/utilities/kate/-/merge_requests/1484)**<br />
Request authored by [Nate Graham](https://invent.kde.org/ngraham) and merged after one day.

- **KTextEditor - [Tune some strings in screenshot dialog](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/685)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after one day.

- **KTextEditor - [Use ellipsis character instead of three dots in UI strings](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/684)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KSyntaxHighlighting - [Use ellipsis character instead of three dots in UI strings](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/611)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KSyntaxHighlighting - [xml: add fictionbook format](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/610)**<br />
Request authored by [Alexey Sakovets](https://invent.kde.org/sakovetsa) and merged after one day.

- **Kate - [Reduce moc usage in search plugin](https://invent.kde.org/utilities/kate/-/merge_requests/1472)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 13 days.

- **Kate - [Optimizations and fixes for urlbar](https://invent.kde.org/utilities/kate/-/merge_requests/1480)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

#### Week 18

- **Kate - [replicate KCommandBar style](https://invent.kde.org/utilities/kate/-/merge_requests/1481)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Some minor cleanups in katemainwindow](https://invent.kde.org/utilities/kate/-/merge_requests/1478)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [UrlBar: Fix symbol view](https://invent.kde.org/utilities/kate/-/merge_requests/1477)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

