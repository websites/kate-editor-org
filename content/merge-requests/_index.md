---
title: Merge Requests
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
menu:
  main:
    weight: 100
    parent: menu
---

This pages provides an overview of the merge requests we are current working on or did already accepted for the Kate, KTextEditor, KSyntaxHighlighting and kate-editor.org repositories.

## Currently Open Merge Requests
Our team is still working on the following requests. Feel free to take a look and help out, if any of them is interesting for you!
### Kate

- **[Draft: Allow loading the LSP Client plugin in KDevelop 6.2](https://invent.kde.org/utilities/kate/-/merge_requests/1712)**<br />
Request authored by [Igor Kushnir](https://invent.kde.org/igorkushnir).

- **[[debug] implement DAP reverse request runInTerminal](https://invent.kde.org/utilities/kate/-/merge_requests/1714)**<br />
Request authored by [Héctor Mesa Jiménez](https://invent.kde.org/hectorm).

### KSyntaxHighlighting

- **[Add syntax highlighting file for Hjson (syntax extension to JSON)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/679)**<br />
Request authored by [Marco Nelles](https://invent.kde.org/marcon).

- **[Draft: Consider application/x-ns-proxy-autoconfig to be JavaScript](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/675)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella).


## Overall Accepted Merge Requests
- 1546 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 709 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 641 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 57 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



## Accepted Merge Requests of 2025

- 24 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 14 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 4 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [February 2025](/merge-requests/2025/02/) (9 requests)
* [January 2025](/merge-requests/2025/01/) (33 requests)


## Accepted Merge Requests of 2024

- 289 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 101 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 77 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 3 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [December 2024](/merge-requests/2024/12/) (33 requests)
* [November 2024](/merge-requests/2024/11/) (38 requests)
* [October 2024](/merge-requests/2024/10/) (49 requests)
* [September 2024](/merge-requests/2024/09/) (58 requests)
* [August 2024](/merge-requests/2024/08/) (51 requests)
* [July 2024](/merge-requests/2024/07/) (37 requests)
* [June 2024](/merge-requests/2024/06/) (30 requests)
* [May 2024](/merge-requests/2024/05/) (46 requests)
* [April 2024](/merge-requests/2024/04/) (43 requests)
* [March 2024](/merge-requests/2024/03/) (32 requests)
* [February 2024](/merge-requests/2024/02/) (30 requests)
* [January 2024](/merge-requests/2024/01/) (23 requests)


## Accepted Merge Requests of 2023

- 308 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 174 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 173 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 13 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [December 2023](/merge-requests/2023/12/) (29 requests)
* [November 2023](/merge-requests/2023/11/) (66 requests)
* [October 2023](/merge-requests/2023/10/) (47 requests)
* [September 2023](/merge-requests/2023/09/) (62 requests)
* [August 2023](/merge-requests/2023/08/) (33 requests)
* [July 2023](/merge-requests/2023/07/) (53 requests)
* [June 2023](/merge-requests/2023/06/) (35 requests)
* [May 2023](/merge-requests/2023/05/) (42 requests)
* [April 2023](/merge-requests/2023/04/) (50 requests)
* [March 2023](/merge-requests/2023/03/) (91 requests)
* [February 2023](/merge-requests/2023/02/) (72 requests)
* [January 2023](/merge-requests/2023/01/) (88 requests)


## Accepted Merge Requests of 2022

- 445 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 194 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 111 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 9 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [December 2022](/merge-requests/2022/12/) (53 requests)
* [November 2022](/merge-requests/2022/11/) (55 requests)
* [October 2022](/merge-requests/2022/10/) (106 requests)
* [September 2022](/merge-requests/2022/09/) (53 requests)
* [August 2022](/merge-requests/2022/08/) (93 requests)
* [July 2022](/merge-requests/2022/07/) (43 requests)
* [June 2022](/merge-requests/2022/06/) (39 requests)
* [May 2022](/merge-requests/2022/05/) (37 requests)
* [April 2022](/merge-requests/2022/04/) (34 requests)
* [March 2022](/merge-requests/2022/03/) (91 requests)
* [February 2022](/merge-requests/2022/02/) (89 requests)
* [January 2022](/merge-requests/2022/01/) (66 requests)


## Accepted Merge Requests of 2021

- 348 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 177 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 144 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 24 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [December 2021](/merge-requests/2021/12/) (51 requests)
* [November 2021](/merge-requests/2021/11/) (32 requests)
* [October 2021](/merge-requests/2021/10/) (45 requests)
* [September 2021](/merge-requests/2021/09/) (15 requests)
* [August 2021](/merge-requests/2021/08/) (34 requests)
* [July 2021](/merge-requests/2021/07/) (45 requests)
* [June 2021](/merge-requests/2021/06/) (57 requests)
* [May 2021](/merge-requests/2021/05/) (57 requests)
* [April 2021](/merge-requests/2021/04/) (37 requests)
* [March 2021](/merge-requests/2021/03/) (91 requests)
* [February 2021](/merge-requests/2021/02/) (109 requests)
* [January 2021](/merge-requests/2021/01/) (120 requests)


## Accepted Merge Requests of 2020

- 87 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 49 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 132 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 2 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [December 2020](/merge-requests/2020/12/) (58 requests)
* [November 2020](/merge-requests/2020/11/) (28 requests)
* [October 2020](/merge-requests/2020/10/) (36 requests)
* [September 2020](/merge-requests/2020/09/) (42 requests)
* [August 2020](/merge-requests/2020/08/) (56 requests)
* [July 2020](/merge-requests/2020/07/) (10 requests)
* [June 2020](/merge-requests/2020/06/) (8 requests)
* [May 2020](/merge-requests/2020/05/) (14 requests)
* [April 2020](/merge-requests/2020/04/) (2 requests)
* [March 2020](/merge-requests/2020/03/) (4 requests)
* [February 2020](/merge-requests/2020/02/) (7 requests)
* [January 2020](/merge-requests/2020/01/) (5 requests)


## Accepted Merge Requests of 2019

- 45 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 6 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [December 2019](/merge-requests/2019/12/) (7 requests)
* [November 2019](/merge-requests/2019/11/) (3 requests)
* [October 2019](/merge-requests/2019/10/) (9 requests)
* [September 2019](/merge-requests/2019/09/) (20 requests)
* [August 2019](/merge-requests/2019/08/) (12 requests)
